#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
from dolfin import *
from dolfin_adjoint import *
from adjoint_contraction_args import logger
from copy import deepcopy

class Compressibility(object):
    def __init__(self, parameters):
        mesh = parameters["mesh"]
            
        V_str, Q_str = ("P_2", "P_1") if not parameters.has_key("state_space") \
          else parameters["state_space"].split(":")

          # Displacemet Space
        V = VectorFunctionSpace(mesh, V_str.split("_")[0], 
                                    int(V_str.split("_")[1]))

        # Lagrange Multiplier
        Q = FunctionSpace(mesh, Q_str.split("_")[0], 
                              int(Q_str.split("_")[1]))
        self.W = V*Q
        self.w = Function(self.W, name = "displacement-pressure")
        self.w_test = TestFunction(self.W)
        self.u_test, self.p_test = split(self.w_test)
        self.u, self.p = split(self.w)
    
    def __call__(self, J):
        return (J - 1)*self.p

    def get_displacement_space(self):
        return self.W.sub(0)
        
    def get_displacement_variable(self):
        return self.u
        
    def get_displacement(self, name, annotate = True):
        D = self.get_displacement_space()
        V = D.collapse()
        
        fa = FunctionAssigner(V, D)
        u = Function(V, name = name)
        fa.assign(u, self.w.split()[0], 
                      annotate = annotate)
        return u


class LVSolver(object):
    """
    A Cardiac Mechanics Solver
    """
    
    def __init__(self, params):        

        for k in ["mesh", "facet_function", "material", "bc"]:
            assert params.has_key(k), \
              "{} need to be in solver_parameters".format(k)

        
        self.parameters = params

        prm = self.default_solver_parameters()
            
        self.parameters["solve"] = prm
        
        
        self._init_spaces()
        self._init_forms()

    def default_solver_parameters(self):

        nsolver = "snes_solver" 
        prm = {"nonlinear_solver": "snes", "snes_solver":{}}

        prm[nsolver]['absolute_tolerance'] = 1E-5
        prm[nsolver]['relative_tolerance'] = 1E-5
        prm[nsolver]['maximum_iterations'] = 15
        prm[nsolver]['linear_solver'] = 'lu'
        prm[nsolver]['error_on_nonconvergence'] = True
        prm[nsolver]['report'] = True if logger.level < INFO else False
        
        return prm
           
        
    def get_displacement(self, name = "displacement", annotate = True):
        return self._compressibility.get_displacement(name, annotate)
    
    def get_u(self):
        return split(self._w)[0]

    def get_gamma(self):
        return self.parameters["material"].gamma

    def is_incompressible(self):
        return self._compressibility.is_incompressible()

    def get_state(self):
        return self._w

    def get_state_space(self):
        return self._W
    
    def reinit(self, w):
        """
        *Arguments*
          w (:py:class:`dolfin.GenericFunction`)
            The state you want to assign

        Assign given state, and reinitialize variaional form.
        """
        self.get_state().assign(w, annotate=False)
        self._init_forms()
    

    def solve(self):
        r"""
        Solve the variational problem

        .. math::

           \delta W = 0

        """
        # Get old state in case of non-convergence
        w_old = self.get_state().copy(True)
        try:
            # Try to solve the system
             solve(self._G == 0,
                   self._w,
                   self._bcs,
                   J = self._dG,
                   solver_parameters = self.parameters["solve"],
                   annotate = False)

        except RuntimeError as ex:
            logger.debug(ex)
            # Solver did not converge
            logger.warning("Solver did not converge")
            # Retrun the old state, and a flag crash = True
            self.reinit(w_old)
            return w_old, True

        else:
            # The solver converged
            
            # If we are annotating we need to annotate the solve as well
            if not parameters["adjoint"]["stop_annotating"]:

                # Assign the old state
                self._w.assign(w_old)
                # Solve the system with annotation
                solve(self._G == 0,
                      self._w,
                      self._bcs,
                      J = self._dG,
                      solver_parameters = self.parameters["solve"], 
                      annotate = True)

            # Return the new state, crash = False
            return self._w, False


    def _init_spaces(self):
        """
        Initialize function spaces
        """
        
        self._compressibility = Compressibility(self.parameters)
            
        self._W = self._compressibility.W
        self._w = self._compressibility.w
        self._w_test = self._compressibility.w_test


    def _init_forms(self):
        r"""
        Initialize variational form

        """
        material = self.parameters["material"]
        N =  self.parameters["facet_normal"]
        ds = Measure("exterior_facet", subdomain_data \
                     = self.parameters["facet_function"])

        # Displacement
        u = self._compressibility.get_displacement_variable()
        self._I = Identity(self.parameters["mesh"].topology().dim())
        # Deformation gradient
        F = grad(u) + self._I
        self._F = variable(F)
        J = det(self._F)
        self._C = F.T*F
        self._E = 0.5*(self._C - self._I)
        
        # Internal energy
        self._strain_energy = material.strain_energy(F)
        self._pi_int = self._strain_energy + self._compressibility(J)

        
        # Internal virtual work
        self._G = derivative(self._pi_int*dx, self._w, self._w_test)

        # External work
        
        v = self._compressibility.u_test

        # Neumann BC
        if self.parameters["bc"].has_key("neumann"):
            for neumann_bc in self.parameters["bc"]["neumann"]:
                p, marker = neumann_bc
                self._G += inner(J*p*dot(inv(F).T, N), v)*ds(marker)
        
        # Robin BC
        if self.parameters["bc"].has_key("robin"):
            for robin_bc in self.parameters["bc"]["robin"]:
                if robin_bc is not None:
                    val, marker = robin_bc
                    self._G += -inner(val*u, v)*ds(marker)
        
        
        # Dirichlet BC
        if self.parameters["bc"].has_key("dirichlet"):
            self._bcs = self.parameters["bc"]["dirichlet"](self._W)
            
        
        self._dG = derivative(self._G, self._w)




