import os, shutil
import numpy as np
try:
    import tabulate
    tabulate.LATEX_ESCAPE_RULES = {}
except:
    print "Warning tabulate module not found"
    print "To print tables please install tabulate"
    print "pip install tabulate"

__all__=["make_canvas_snap_shot", "tabalize", "make_canvas_strain"]

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
        
latex_head = r"""\documentclass[tightpage, 26pt]{{standalone}}
\usepackage{{subcaption}}
\usepackage{{graphicx}}
\usepackage{{float}}
\usepackage{{array}}
\usepackage[export]{{adjustbox}}
\newcolumntype{{C}}{{>{{\centering\arraybackslash}} m{{0.12\textwidth}}}}  

\newcommand{{\imgcasefront}}[1]{{\adjincludegraphics[scale=0.04,trim={{{{.2\width}} {{.01\height}} {{.2\width}} {{.01\height}}}}, clip]{{{0}_#1_front}}}}
\newcommand{{\imgcaseside}}[1]{{\adjincludegraphics[scale=0.04,trim={{{{.2\width}} {{.01\height}} {{.2\width}} {{.01\height}}}}, clip]{{{0}_#1_side}}}}

\begin{{document}}
\setlength\tabcolsep{{0.0pt}}
\renewcommand{{\arraystretch}}{{0.0}}
"""

tab4_head=r"""
\begin{tabular}{CCCC}
"""

tab_tail=r"""
\end{tabular}
\end{document}
"""

tab4_labels=r"""\multicolumn{{1}}{{l}}{{{0}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{1}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{2}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{3}}} \vspace{{0.01cm}}\\
"""
tab4_img=r"""\imgcasefront{{{0}}} & \imgcasefront{{{1}}} &
\imgcasefront{{{2}}} & \imgcasefront{{{3}}} \\ 
\imgcaseside{{{0}}} & \imgcaseside{{{1}}} & 
\imgcaseside{{{2}}} & \imgcaseside{{{3}}} \\
"""

tab6_head=r"""
\begin{tabular}{CCCCCC}
"""


tab6_heatmap=r"""
\multicolumn{{6}}{{c}}{{{1}}} \vspace{{0.1cm}}\\
\multicolumn{{6}}{{c}}{{\adjincludegraphics[scale=0.1,trim={{{{.0\width}} {{.0\height}} {{.0\width}} {{.785\height}}}}, clip]{{{0}}}}}\\
"""

tab5_heatmap=r"""
\multicolumn{{5}}{{c}}{{{1}}} \vspace{{0.1cm}}\\
\multicolumn{{5}}{{c}}{{\adjincludegraphics[scale=0.1,trim={{{{.0\width}} {{.0\height}} {{.0\width}} {{.785\height}}}}, clip]{{{0}}}}}\\
"""


tab6_labels=r"""\multicolumn{{1}}{{l}}{{{0}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{1}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{2}}} \vspace{{0.01cm}}&
\multicolumn{{1}}{{l}}{{{3}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{4}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{5}}} \vspace{{0.01cm}}\\
"""
tab6_img=r"""\imgcasefront{{{0}}} & \imgcasefront{{{1}}} &
\imgcasefront{{{2}}} & \imgcasefront{{{3}}} &
\imgcasefront{{{4}}} & \imgcasefront{{{5}}} \\ 
\imgcaseside{{{0}}} & \imgcaseside{{{1}}} & 
\imgcaseside{{{2}}} & \imgcaseside{{{3}}} & 
\imgcaseside{{{4}}} & \imgcaseside{{{5}}} \\
"""

tab5_head=r"""
\begin{tabular}{CCCCC}
"""
tab5_labels=r"""\multicolumn{{1}}{{l}}{{{0}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{1}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{2}}} \vspace{{0.01cm}}&
\multicolumn{{1}}{{l}}{{{3}}} \vspace{{0.01cm}}& 
\multicolumn{{1}}{{l}}{{{4}}} \vspace{{0.01cm}}\\
"""
tab5_img=r"""\imgcasefront{{{0}}} & \imgcasefront{{{1}}} &
\imgcasefront{{{2}}} & \imgcasefront{{{3}}} &
\imgcasefront{{{4}}}  \\ 
\imgcaseside{{{0}}} & \imgcaseside{{{1}}} & 
\imgcaseside{{{2}}} & \imgcaseside{{{3}}} & 
\imgcaseside{{{4}}}\\
"""

strain_tab=r"""\documentclass[tightpage, 26pt]{{standalone}}
\usepackage{{subcaption}}
\usepackage{{graphicx}}
\usepackage{{float}}
\usepackage{{array}}
\usepackage[export]{{adjustbox}}
\setlength{{\tabcolsep}}{{0pt}}%
\renewcommand{{\arraystretch}}{{0}}
\begin{{document}}
\begin{{tabular}}{{c}}
 \includegraphics[scale =0.25]{{{0}}} \\
\hline
  \includegraphics[scale = 0.25]{{{1}}} \\
\hline
  \includegraphics[scale = 0.25]{{{2}}} \\
\end{{tabular}}
\end{{document}}
"""


def tabalize(caption, header, table, label, floatfmt=".2e"):

    tabular =  tabulate.tabulate(table, header,
                                 tablefmt="latex", floatfmt=floatfmt)
    T = \
        r"""
\begin{{table}}
\caption{{{}}}
{}
\label{{{}}}
\end{{table}}
""".format(*[caption, tabular, label])
        
    return T

def make_canvas_strain(paths):

    outdir = os.path.dirname(paths[0])
   
    latex_full = strain_tab.format(*paths)

    fname = "simulated_strains"
    
    fnametex = ".".join([fname, "tex"])
    with open(fnametex, "w") as f:
        f.write(latex_full)

    os.system("pdflatex {} >/dev/null".format(fnametex))
   
    for ext in [".aux", ".log", ".tex"]:
        os.remove(fname+ext)

    src= ".".join([fname, "pdf"])
    dst = "/".join([outdir, src])
  
    shutil.move(src, dst)
    print "moved from {} to {}".format(src, dst)
    

def make_canvas_snap_shot(lst, times, name, heatmap_name = "", heatmap_label = r"$\gamma$"):

    assert len(lst) == len(times), \
        "Not equal length"

    r6 = len(lst) % 6
    r5 = len(lst) % 5
    assert r6 == 0 or r5 == 0, \
        "list must be diviable by six or five, rest5 = {}, rest6 = {}".format(r5, r6)

    # Check that the files exist and add extension
    for l in lst:
        v = "_{}".format(l)
        for s in ["_front", "_side"]:
            # if not os.path.isfile(name + v + s + ".png"):
            if os.path.isfile(name + v + s):
                shutil.move(name + v + s,
                            name + v + s +".png")
            else:
                if not os.path.isfile(name + v + s + ".png"):
                    raise IOError("File {} not found".format(name + v + s))
                
    if heatmap_name != "":
        if not os.path.isfile(heatmap_name + ".png"):
            if os.path.isfile(heatmap_name):
                shutil.move(heatmap_name, heatmap_name+".png")
            else:
                print "Not heatmap figure named ", heatmap_name
                print "Make figure without heatmap"
                tab_heatmap = ""

        if r6 == 0:
            tab_heatmap = tab6_heatmap.format(heatmap_name, heatmap_label)
        elif r5 == 0:
            tab_heatmap = tab5_heatmap.format(heatmap_name, heatmap_label)
    else:
        tab_heatmap = ""
            
            
            
        
    if r6 == 0:
        lst_chunks = [lst[i:i+6] for i in range(0, len(lst), 6)]
        times_chunks = [times[i:i+6] for i in range(0, len(times), 6)]


        latex_full = latex_head.format(name)+\
                     tab6_head + tab_heatmap

        for t,l in zip(times_chunks,lst_chunks):
            latex_full += tab6_labels.format(*t)
            latex_full += tab6_img.format(*l)

    elif r5 == 0:
        lst_chunks = [lst[i:i+5] for i in range(0, len(lst), 5)]
        times_chunks = [times[i:i+5] for i in range(0, len(times), 5)]


        latex_full = latex_head.format(name)+\
                     tab5_head + tab_heatmap

        for t,l in zip(times_chunks,lst_chunks):
            latex_full += tab5_labels.format(*t)
            latex_full += tab5_img.format(*l)

    latex_full += tab_tail

    
    fname = "_".join(["snap_shots", os.path.basename(name)])
    fnametex = ".".join([fname, "tex"])
    with open(fnametex, "w") as f:
        f.write(latex_full)

    exit()
    os.system("pdflatex {} >/dev/null".format(fnametex))
   
    for ext in [".aux", ".log", ".tex"]:
        os.remove(fname+ext)

    src= ".".join([fname, "pdf"])
    dst = "/".join([os.path.abspath(os.path.dirname(name)), src])
  
    shutil.move(src, dst)
    print "moved from {} to {}".format(src, dst)
    


if __name__ == "__main__":

    lst = range(3,26, 2)
    lst2 = [ r"{:.0f} $\%$".format(i) for i in np.linspace(0,100, len(lst))]
    for i in lst2: print i 
  
    
    make_canvas_snap_shot(lst, lst2, "gamma", "heatmap.png")



