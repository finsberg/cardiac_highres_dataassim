#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
from dolfin import *
from dolfin_adjoint import *
import numpy as np

from run_optimization import run_active_optimization, run_passive_optimization
from setup_optimization import make_solver_params, RegionalGamma
from heart_problem import SyntheticHeartProblem
from adjoint_contraction_args import *
from store_opt_results import write_opt_results_to_h5
import h5py, os, yaml
from numpy_mpi import *
from utils import contract_point_exists, passive_inflation_exists, Object, get_spaces, Text, pformat




def generate_percentage_vol_noise():
    """Generate random noise for passive
    synthetic case
    """
   
    vols = [225.22831244,  230.0104206 ,  234.25063661]
    dvols = np.diff(vols)

     
    # Do 30 tests each with two points (first i reference)
    for r in range(60):
        data = {}
        vol_noise = [0.0]
        for d in dvols:
            vol_noise.append(d*(r-30)/100)
        print vol_noise

        data["vol_noise_list"] = vol_noise
        data["strain_cov_crl"] = np.eye(3).tolist()
        data["strain_mean_crl"] = np.zeros(3).tolist()
        data["strain_noise_list"] = [[0.,0.,0.] for i in range(SYNTH_PASSIVE_FILLING)]
                             
        with open("data/vol_noise_synth_percent_{}.yml".format(r), "wb" ) as f:
            yaml.dump(data, f,  default_flow_style=False)

def generate_random_vol_noise():
    """Generate random noise for passive
    synthetic case
    """
    std = 4.3 #ml

    # Do 30 tests each with two points (first i reference)
    for r in range(30):
        data = {}
        vol_noise = [0.0]
        eps = np.random.normal(0, std)
        for t in range(SYNTH_PASSIVE_FILLING-1):
            vol_noise.append(eps)

        data["vol_noise_list"] = vol_noise
        data["strain_cov_crl"] = np.eye(3).tolist()
        data["strain_mean_crl"] = np.zeros(3).tolist()
        data["strain_noise_list"] = [[0.,0.,0.] for i in range(SYNTH_PASSIVE_FILLING)]
                             
        with open("data/vol_noise_synth_const_{}.yml".format(r), "wb" ) as f:
            yaml.dump(data, f,  default_flow_style=False)
        
    
def generate_noise():
    """
    This function shows how noise is generated 
    from the measurements
    """
    import yaml
    from demo import load_patient_data
    patient = load_patient_data(synth_data = True)

    with open("data/noise_synth.yml", "rb" ) as f:
            noise = yaml.load(f)

    mean = np.array(noise["strain_mean_crl"])
    cov = np.array(noise["strain_cov_crl"])

    num_points = patient.num_points
    del_mean = mean/float(num_points)
    del_cov = cov/float(num_points)
    L = np.linalg.cholesky(del_cov)
    

    # Generate 10 sets of noise in addition to the original one.
    for k in range(1,10):
        Y_points = [[0,0,0]]
        Y = np.array([0.0, 0.0, 0.0])
        for i in range(num_points-1):
            Y += np.dot(L, np.random.random(3) + del_mean)
            Y_points.append(Y.tolist())

        d = noise
        d["strain_noise_list"] = Y_points
        with open("data/noise_synth_{}.yml".format(k), "wb" ) as f:
            yaml.dump(d, f,  default_flow_style=False)
        
    
    
    

    

def store_passive_inflation(phm, states, strains,  params):

    for_result_opt, ini_for_res = make_synthetic_results(phm)
    for_result_opt.states = states
    for_result_opt.strains = strains
    
    h5group =  PASSIVE_INFLATION_GROUP.format(params["alpha_matparams"])
    mesh = phm.solver.parameters["mesh"]
    opt_matparams = interpolate(Constant(params["Material_parameters"].values()), VectorFunctionSpace(mesh, "R", 0, dim=4))
    write_opt_results_to_h5(h5group, params, ini_for_res, for_result_opt, opt_matparams = opt_matparams)


def make_synthetic_results(phm):
    # Just make a lot of dummy variables for the writing to work
    for_result_opt = Object()
    for_result_opt.phm = phm
    for_result_opt.func_value_strain = 0.0
    for_result_opt.func_value_volume = 0.0
    for_result_opt.weighted_func_value_strain = 0.0
    for_result_opt.weighted_func_value_volume = 0.0
    for_result_opt.gamma_gradient = 0.0
    for_result_opt.reg_par = 0.0
    for_result_opt.strain_weights = np.ones((17, 3))
    for_result_opt.volumes = [0.0]
    for_result_opt.lv_pressures = [0.0]
    for_result_opt.rv_pressures = [0.0]
   

    ini_for_res =  Object()
    ini_for_res.func_value_strain = 0.0
    ini_for_res.func_value_volume = 0.0
    ini_for_res.weighted_func_value_strain = 0.0
    ini_for_res.weighted_func_value_volume = 0.0

    return for_result_opt, ini_for_res

def correct_for_drift(phm, synth_output, noise_arr, synth_group):

    strains_original_lst, strains_noise_lst = add_noise_to_original_strains(phm, synth_output, noise_arr, synth_group)

    mpi_print("Correct strains for drift")
    strains_corrected_lst = []
    for i in range(17):
        region_strain_noise = strains_noise_lst[i]
        c_noise = [s[0] for s in region_strain_noise]
        r_noise = [s[1] for s in region_strain_noise]
        l_noise = [s[2] for s in region_strain_noise]

        c_corr = subtract_line(c_noise)
        r_corr = subtract_line(r_noise)
        l_corr = subtract_line(l_noise)

        regions_strain_corr_all = np.asarray(zip(c_corr, r_corr, l_corr))
        strains_corrected_lst.append(regions_strain_corr_all)

    return strains_corrected_lst, strains_noise_lst, strains_original_lst

def add_noise_to_original_strains(phm, synth_output, noise_arr, synth_group):
    
    strain_fun = Function(phm.strainspace)

    mpi_print("Add noise to original strains")
    strains_noise_lst  = [[] for i in range(17)]
    strains_original_lst  =[[] for i in range(17)]
    with h5py.File(synth_output, "r") as h5file:
        for point in range(len(noise_arr)):
            for i in STRAIN_REGION_NUMS:
                strain_arr = np.array(h5file[synth_group + "/point_{}/original_strain/region_{}".format(point, i)])
                assign_to_vector(strain_fun.vector(), strain_arr)
                strain_arr_orig = gather_broadcast(strain_fun.vector().array())
                strains_original_lst[i-1].append(strain_arr_orig)

                strain_arr_noise = np.add(strain_arr_orig, noise_arr[point])
                strains_noise_lst[i-1].append(strain_arr_noise)

    return strains_original_lst, strains_noise_lst

def add_noise_to_original_volumes(phm, synth_output, noise_arr, synth_group):
    
    vol_fun = Function(FunctionSpace(phm.mesh, "R", 0))

    mpi_print("Add noise to original strains")
    vols_noise_lst  = []
    vols_original_lst = []
    
    with h5py.File(synth_output, "r") as h5file:
        for point in range(len(noise_arr)):
            vol_arr = np.array(h5file[synth_group + "/point_{}/original_volume".format(point)])
            assign_to_vector(vol_fun.vector(), vol_arr)
            vol_arr_orig = gather_broadcast(vol_fun.vector().array())[0]
            vols_original_lst.append(vol_arr_orig)
           

            vol_arr_noise = vol_arr_orig + noise_arr[point]
            vols_noise_lst.append(vol_arr_noise)

    return vols_original_lst, vols_noise_lst

def subtract_line(Y):    
    X = range(len(Y))
    # Create a linear interpolant between the first and the new point
    line = [ i*(Y[-1] - Y[0])/(len(X)-1) for i in X]

    # Subtract the line from the original points
    Y_sub = np.subtract(Y, line)
    return Y_sub

def store_synth_data(phm, synth_output, point, noise, synth_group):

    scalar_func = Function(FunctionSpace(phm.solver.parameters["mesh"], "R", 0))
    h5group = synth_group + "/point_{}".format(point)

    file_mode = "a" if os.path.isfile(synth_output) else "w"

    with HDF5File(mpi_comm_world(), synth_output, file_mode) as storage:
        # Store strains
        for i in STRAIN_REGION_NUMS:
            if noise:
                storage.write(phm.strains[i-1].vector(), h5group + "/original_strain/region_{}".format(i))
            else:
                storage.write(phm.strains[i-1].vector(), h5group + "/strain/region_{}".format(i))

        # Store strain field
        storage.write(phm.strainfield, h5group + "/strainfield")

        # Store volume
        volume = phm.get_inner_cavity_volume()
        scalar_func.assign(Constant(volume))
        if noise:
            storage.write(scalar_func.vector(), h5group + "/original_volume")
        else:
            storage.write(scalar_func.vector(), h5group + "/volume")

        # Store pressure
        pressure = phm.p_lv.t
        scalar_func.assign(Constant(pressure))
        storage.write(scalar_func.vector(), h5group + "/pressure")

        # Store state
        storage.write(phm.solver.get_state(), h5group + "/state")   

        # Store gamma
        storage.write(phm.gamma, h5group + "/activation_parameter") 

def make_synthetic_pressure(pressure, passive_only = False):

    measurements = Object()
    pressure = np.multiply(0.1, pressure)
    pressure = np.subtract(pressure, pressure[0])
    num_points = SYNTH_PASSIVE_FILLING + NSYNTH_POINTS + 1
    p1 = pressure[:(num_points/2+1)]
    p2 = np.linspace(p1[-1], 0, num_points/2)

    if passive_only:
        measurements.pressure = np.concatenate((p1,p2))[:SYNTH_PASSIVE_FILLING]
    else:
        measurements.pressure = np.concatenate((p1,p2))
    return measurements


def generate_active_synth_data(params, gamma_expr, X0, patient):

    
    synth_output = params["sim_file"]
    synth_group = "synth_data"
    
    if mpi_comm_world().rank == 0:
        if not os.path.isdir("results"):
            os.makedirs("results")

    
    measurements = make_synthetic_pressure(patient.pressure, params["passive_synth"])
    solver_parameters, p_lv = make_solver_params(params, patient, measurements)

    mesh = patient.mesh
    crl_basis = (patient.e_circ, patient.e_rad, patient.e_long)
    
    spaces = get_spaces(mesh)

    if params["gamma_space"] == "regional":
        gamma = RegionalGamma(patient.strain_markers)
        gamma_list = [RegionalGamma(patient.strain_markers)]*(SYNTH_PASSIVE_FILLING-1)
        gamma_space = gamma.function_space()
        
    else:
        gamma_family, gamma_degree = params["gamma_space"].split("_")
        gamma_space = FunctionSpace(patient.mesh, gamma_family, int(gamma_degree))

        gamma = Function(gamma_space, name = 'activation parameter')
        gamma_list = [Function(gamma_space, name = 'activation parameter_zero')]*(SYNTH_PASSIVE_FILLING-1)
        
   
    solver_parameters['material'].gamma = gamma
    
    for x0 in X0:
        
        gamma_exp = Expression(gamma_expr,x0 = x0)

        if params["gamma_space"] == "regional":
            gamma_fun = RegionalGamma(patient.strain_markers)
            
            gamma_tmp = project(gamma_exp, FunctionSpace(mesh, "CG", 1))
            from postprocess import get_regional
            dX = Measure("dx", domain = patient.mesh,
                         subdomain_data = patient.strain_markers)
            gamma_reg = get_regional(dX, gamma_tmp)
     
            assign_to_vector(gamma_fun.vector(), gamma_reg)
                        
            
        else:
            
            gamma_fun = project(gamma_exp, gamma_space)
            
        gamma_list.append(gamma_fun)
        
    # Add a final zero point
    if params["gamma_space"] == "regional":
        gamma_list.append(RegionalGamma(patient.strain_markers))
    else:
        gamma_list.append(Function(gamma_space, name = 'activation parameter_zero'))
   
    num_points = len(gamma_list)
    phm = SyntheticHeartProblem(measurements.pressure, solver_parameters, p_lv,
                                    crl_basis, spaces, gamma_list)
   
    # Store the first point
    store_synth_data(phm, synth_output, 0, params["noise"], synth_group)
    
    it = 1
    # Run the forward model, and generate synthetic data
    logger.info("LV Pressure    LV Volume    Mean Strain Norms (c,r,l)")
    states = [Vector(phm.solver.get_state().vector())]
    strains = [[Vector(phm.strains[i].vector())] for i in range(17)]
   
    
    for sol, strain in phm:

        # Print output
        volume = phm.get_inner_cavity_volume()
        strain_list = [np.mean([gather_broadcast(phm.strains[s].vector().array())[0] 
                                for i in range(len(phm.strains))]) for s in range(3)]
        
        strain_str = ("{:.5f} "*len(strain_list)).format(*strain_list)
        logger.info(("{:.5f}" + " "*8 + "{:.5f}" + " "*5 + "{}").format(phm.p_lv.t, volume, strain_str))
        
        # Store the data
        store_synth_data(phm, synth_output, it, params["noise"], synth_group)
       

        if it < SYNTH_PASSIVE_FILLING:
            # Store the states. To be used for passive inflation phase
            states.append(Vector(phm.solver.get_state().vector()))
            
            for region in STRAIN_REGION_NUMS:
                # from IPython import embed; embed()
                strains[region-1].append(Vector(phm.strains[region-1].vector()))
            
        
        it += 1
    
  

    store_passive_inflation(phm, states, strains,  params)


    if params["noise"]:
        # Load the noise from generated file
        from yaml import load
        if params["passive_synth"]:
            noise_path = "/".join([params["noise_path"], "vol_noise_synth_percent_{}.yml".format(params["noise_realization"])])
        else:
            noise_path = "/".join([params["noise_path"], "noise_synth_{}.yml".format(params["noise_realization"])])
        with open(noise_path, "rb" ) as f:
            noise = load(f)

      
        # Volume
        vols_noise = np.array(noise["vol_noise_list"])
        
        vols_original_lst, vols_noisy_lst = add_noise_to_original_volumes(phm, synth_output, vols_noise, synth_group)
        
        scalar_func = Function(FunctionSpace(phm.solver.parameters["mesh"], "R", 0))
        mpi_print("Store the noisy volumes")
        with HDF5File(mpi_comm_world(), synth_output, "a") as h5file:
            for point in range(len(phm.pressure)):
                scalar_func.assign(Constant(vols_noisy_lst[point]))
                h5file.write(scalar_func.vector(), synth_group + "/point_{}/volume_w_noise".format(point))
                h5file.write(scalar_func.vector(), synth_group + "/point_{}/volume".format(point))

        #Strain
        strain_noise = np.array(noise["strain_noise_list"])
        
        # Correct noisy strain curves by subtracting a line so that first and last point have zero strain
        strains_corrected_lst, strains_noisy_lst, strains_original_lst = correct_for_drift(phm, synth_output, strain_noise, synth_group) 
        s = Function(phm.strainspace)
        mpi_print("Store the corrected strains")
        with HDF5File(mpi_comm_world(), synth_output, "a") as h5file:
            for point in range(len(phm.pressure)):
                for i in STRAIN_REGION_NUMS:
                    # Corrected
                    assign_to_vector(s.vector(),strains_corrected_lst[i-1][point])
                    h5file.write(s.vector(), synth_group + "/point_{}/corrected_strain/region_{}".format(point, i))
                    # Overwrite the originals. These are the ones we are trying to get
                    h5file.write(s.vector(), synth_group + "/point_{}/strain/region_{}".format(point, i))

                    # Strains with noise
                    assign_to_vector(s.vector(),strains_noisy_lst[i-1][point])
                    h5file.write(s.vector(), synth_group + "/point_{}/strain_w_noise/region_{}".format(point, i))


if __name__ == "__main__":
    generate_percentage_vol_noise()
