#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
import os, logging
import dolfin

log_level = logging.INFO

# Setup logger
def make_logger(name, level = logging.INFO):
    import logging

    mpi_filt = lambda: None
    def log_if_proc0(record):
        if dolfin.mpi_comm_world().rank == 0:
            return 1
        else:
            return 0
    mpi_filt.filter = log_if_proc0
    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter('%(message)s') 
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.addFilter(mpi_filt)
    dolfin.set_log_active(True)
    dolfin.set_log_level(dolfin.WARNING)

    return logger

logger = make_logger("Adjoint_Contraction", log_level)

PASSIVE_INFLATION_GROUP = "alpha_{}/passive_inflation" 
ACTIVE_CONTRACTION_GROUP = "alpha_{}/active_contraction/contract_point_{}" #.format(alpha, iteration number)
CONTRACTION_POINT = "contract_point_{}"
ALPHA_STR = "alpha_{}"
ACTIVE_CONTRACTION = "active_contraction"
PASSIVE_INFLATION = "passive_inflation"


STRAIN_REGIONS = {"LVBasalAnterior": 1,
		  "LVBasalAnteroseptal": 2,
		  "LVBasalSeptum": 3,
		  "LVBasalInferior": 4,
		  "LVBasalPosterior": 5,
		  "LVBasalLateral": 6,
		  "LVMidAnterior": 7,
		  "LVMidAnteroseptal": 8,
		  "LVMidSeptum": 9,
		  "LVMidInferior": 10,
		  "LVMidPosterior": 11,
		  "LVMidLateral": 12,
		  "LVApicalAnterior": 13,
		  "LVApicalSeptum": 14,
		  "LVApicalInferior": 15,
		  "LVApicalLateral": 16,
		  "LVApex": 17}

STRAIN_DIRECTIONS = ["RadialStrain", "LongitudinalStrain", 
                     "CircumferentialStrain", "AreaStrain"]
# Strain regions
STRAIN_REGION_NUMS = STRAIN_REGIONS.values()
STRAIN_REGION_NUMS.sort()
STRAIN_NUM_TO_KEY = {0:"circumferential",
                     1: "radial",
                     2: "longitudinal"}

PHASES = ['passive_inflation', 'active_contraction', "all"]

############### SYNTHETIC DATA  #####################
NSYNTH_POINTS = 7
SYNTH_PASSIVE_FILLING = 3
