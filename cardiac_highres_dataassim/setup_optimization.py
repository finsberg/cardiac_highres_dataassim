#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
from dolfin import *
from dolfin_adjoint import *
import numpy as np
from utils import Object, Text
from adjoint_contraction_args import *
from numpy_mpi import *
import h5py



def make_solver_params(params, patient, measurements, passive_group = None):
    
    # Material parameters

    # If we want to estimate material parameters, use the materal parameters
    # from the parameters
    if params["phase"] in [PHASES[0], PHASES[2]]:
        
        material_parameters = params["Material_parameters"]
        paramvec = Function(VectorFunctionSpace(patient.mesh, "R", 0, dim = 4), name = "matparam vector")
        assign_to_vector(paramvec.vector(), np.array(material_parameters.values()))
        

    # Otherwise load the parameters from the result file  
    else:

        # Open simulation file
        with HDF5File(mpi_comm_world(), params["sim_file"], 'r') as h5file:
        
            h5group = PASSIVE_INFLATION_GROUP.format(params["alpha_matparams"]) if passive_group is None else passive_group
            # Get material parameter from passive phase file
            paramvec = Function(VectorFunctionSpace(patient.mesh, "R", 0, dim = 4), name = "matparam vector")
            h5file.read(paramvec, h5group + "/parameters/optimal_material_parameters_function")

    a,a_f,b,b_f = split(paramvec)


    # Contraction parameter
    if params["gamma_space"] == "regional":
        gamma = RegionalGamma(patient.strain_markers)
    else:
        gamma_family, gamma_degree = params["gamma_space"].split("_")
        gamma_space = FunctionSpace(patient.mesh, gamma_family, int(gamma_degree))

        gamma = Function(gamma_space, name = 'activation parameter')



    p_lv = Expression("t", t = measurements.pressure[0], name = "LV_endo_pressure")
    N = FacetNormal(patient.mesh)


    if params["base_bc"] == "fixed":
        robin_bc = [None]
        def base_bc(W):
            '''Fix the basal plane.
            '''
            V = W if W.sub(0).num_sub_spaces() == 0 else W.sub(0)
            bc = [DirichletBC(V, Constant((0, 0, 0)), 10)]
            return bc
        
        
    else:
        if not params["base_bc"] == "dirichlet_bcs_fix_base_x":
            if mesh_verts is None:
                logger.warning("No mesh vertices found. This must be set in the patient class")
            else:
                logger.warning("Unknown Base BC")
            logger.warning("Fix base in x direction")
    
        def base_bc(W):
            '''Make Dirichlet boundary conditions where the base is allowed to slide
            in the x = 0 plane.
            '''
            V = W if W.sub(0).num_sub_spaces() == 0 else W.sub(0)
            bc = [DirichletBC(V.sub(0), 0, 10)]
            return bc

        
        # Apply a linear sprint robin type BC to limit motion
        robin_bc = [[-Constant(params["base_spring_k"], 
                               name ="base_spring_constant"), 10]]


    matparams = {"a":a, "a_f":a_f, "b":b, "b_f":b_f}
    material = HolzapfelOgden(patient.e_f, gamma, matparams)
    
    solver_parameters = {"mesh": patient.mesh,
                         "facet_function": patient.facets_markers,
                         "facet_normal": N,
                         "mesh_function": patient.strain_markers,
                         "material": material,
                         "bc":{"dirichlet": base_bc,
                               "neumann":[[p_lv, 30]],
                               "robin": robin_bc}}


    pararr = gather_broadcast(paramvec.vector().array())
    logger.info("\nParameters")
    logger.info("\ta     = {:.3f}".format(pararr[0]))
    logger.info("\ta_f   = {:.3f}".format(pararr[1]))
    logger.info("\tb     = {:.3f}".format(pararr[2]))
    logger.info("\tb_f   = {:.3f}".format(pararr[3]))
    logger.info('\talpha = {}'.format(params["alpha"]))
    logger.info('\talpha_matparams = {}'.format(params["alpha_matparams"]))
    logger.info('\treg_par = {}\n'.format(params["reg_par"]))


    if params["phase"] == PHASES[0]:
        return solver_parameters, p_lv, paramvec
    elif params["phase"] == PHASES[1]:
        return solver_parameters, p_lv, gamma
    else:
        return solver_parameters, p_lv

def get_measurements(params, patient):

 
    pressure = np.array(patient.pressure)
    # Compute offsets

    # Calculate difference bwtween calculated volume, and volume given from echo
    volume_offset = get_volume_offset(patient)
    logger.info("Volume offset = {} cm3".format(volume_offset))

    # Subtract this offset from the volume data
    volume = np.subtract(patient.volume,volume_offset)

    #Convert pressure to centipascal (the mesh is in cm)
    pressure = np.multiply(0.1, pressure)

    # Choose the pressure at the beginning as reference pressure
    reference_pressure = pressure[0] 
    logger.info("Pressure offset = {} cPa".format(reference_pressure))

    #Here the issue is that we do not have a stress free reference mesh. 
    #The reference mesh we use is already loaded with a certain amount of pressure, which we remove.    
    pressure = np.subtract(pressure,reference_pressure)

    # Strain
    strain = patient.strain



    if params["phase"] ==  PHASES[0]:
        # We need just the points from the passive phase
        start = 0
        end = patient.passive_filling_duration

    elif params["phase"] == PHASES[1]:
        # We need just the points from the active phase
        start = patient.passive_filling_duration -1
        end = len(pressure)
    else:
        start = 0
        end = len(pressure)
    
    measurements = Object()
    # Volume
    measurements.volume = volume[start:end]
    
    # Pressure
    measurements.pressure = pressure[start:end]


    strains = {}
    for region in STRAIN_REGION_NUMS:
        strains[region] = strain[region][start:end]
    measurements.strain = strains
    

    return measurements

def get_synthetic_measurements(params, patient):


    if params["passive_synth"]:
        num_points = patient.passive_filling_duration
        start = 0    

    else:
        num_points = patient.passive_filling_duration \
                     + patient.num_contract_points
        start = patient.passive_filling_duration-1
    
    pressure = []
    volume = []
    strain = {i:[] for i in STRAIN_REGION_NUMS}

    disp_space = VectorFunctionSpace(patient.mesh, "CG", 2)
    state_space = disp_space*FunctionSpace(patient.mesh, "CG", 1)
    state = Function(state_space)
    
    strainfieldspace = VectorFunctionSpace(patient.mesh, "CG", 1, dim = 3)
    strainfield = Function(strainfieldspace)
    strainfields = []
    disps = []

    synth_str = "synth_data/point_{}"            

    with HDF5File(mpi_comm_world(), params["sim_file"], "r") as h5file:
        for point in range(start, num_points):
            
            h5file.read(strainfield,
                        "/".join([synth_str.format(point),
                                  "strainfield"]))
            strainfields.append(Vector(strainfield.vector()))

            h5file.read(state, "/".join([synth_str.format(point),
                                  "state"]))
            u,p = state.split(deepcopy=True)
            disps.append(Vector(u.vector()))
            
            
    with h5py.File(params["sim_file"] , "r") as h5file:
        for point in range(start, num_points):
            h5group = synth_str.format(point)
            # Get strains
            for region in STRAIN_REGION_NUMS:
                strain_arr = np.array(h5file[h5group + "/strain/region_{}".format(region)])
                strain[region].append(tuple(strain_arr))

            # Get volume
            v = np.array(h5file[h5group + "/volume"])
            volume.append(v[0])
            
            # Get pressure
            p = np.array(h5file[h5group + "/pressure"])
            pressure.append(p[0]*10) # kPa
    

    measurements = Object()

    measurements.volume = volume
    measurements.pressure = np.multiply(0.1, pressure)
    measurements.strain = strain
    measurements.strainfield = strainfields
    measurements.disps = disps
    
    return measurements

def get_volume_offset(patient):
    N = FacetNormal(patient.mesh)
    ds = Measure("exterior_facet", subdomain_data = patient.facets_markers, domain = patient.mesh)(30)
    X = SpatialCoordinate(patient.mesh)
    vol = assemble((-1.0/3.0)*dot(X,N)*ds)
    return patient.volume[0] - vol

def setup_simulation(params, patient, passive_group = None):
    
    # Load measurements
    if params["synth_data"]:
        measurements = get_synthetic_measurements(params,patient)
    else:
        measurements = get_measurements(params, patient)
    solver_parameters, p_lv, controls = make_solver_params(params, patient, measurements, passive_group = passive_group)

    return measurements, solver_parameters, p_lv, controls


class MyReducedFunctional(ReducedFunctional):
    def __init__(self, for_run, paramvec, scale = 1.0):

        self.for_run = for_run
        self.paramvec = paramvec
        self.first_call = True
        self.scale = scale
        self.nr_crashes = 0
        self.iter = 0
        self.nr_der_calls = 0
        self.func_values_lst = []
        self.controls_lst = []
        self.forward_times = []
        self.backward_times = []
        self.initial_paramvec = gather_broadcast(paramvec.vector().array())

    def __call__(self, value):
        adj_reset()
        
        self.iter += 1

        paramvec_new = Function(self.paramvec.function_space(), name = "new control")

        if isinstance(value, Function) or isinstance(value, RegionalGamma):
            # val = gather_broadcast(value.vector().array())
            paramvec_new.assign(value)
        else:
            # val = value
            assign_to_vector(paramvec_new.vector(), value)
            

    
        logger.debug(Text.yellow("Start annotating"))
        parameters["adjoint"]["stop_annotating"] = False

        logger.setLevel(WARNING)
        t = Timer()
        t.start()
        self.for_res, crash= self.for_run(paramvec_new, True)
        for_time = t.stop()
        self.forward_times.append(for_time)
        logger.setLevel(INFO)
        logger.info("Forward time = {}".format(for_time))
        if self.first_call:
            # Store initial results 
            self.ini_for_res = self.for_res
            self.first_call = False
            logger.info("Iter\tI_tot\t\tI_vol\t\tI_strain\tI_reg")
        

        if crash:
            # This exection is thrown if the solver uses more than x steps.
            # The solver is stuck, return a large value so it does not get stuck again
            logger.warning(Text.red("Iteration limit exceeded. Return a large value of the functional"))
            # Return a big value, and make sure to increment the big value so the 
            # the next big value is different from the current one. 
            func_value = np.inf
            self.nr_crashes += 1
    
        else:
            func_value = self.for_res.func_value

        self.func_values_lst.append(func_value)
        self.controls_lst.append(Vector(paramvec_new.vector()))

        control = Control(self.paramvec)
        ReducedFunctional.__init__(self, Functional(self.for_res.total_functional), control)

        logger.debug(Text.yellow("Stop annotating"))
        parameters["adjoint"]["stop_annotating"] = True

        logger.info("{}\t{:.3e}\t{:.3e}\t{:.3e}\t{:.3e}".format(self.iter, 
                                                               func_value, 
                                                               self.for_res.func_value_volume, 
                                                               self.for_res.func_value_strain, 
                                                               self.for_res.gamma_gradient))

     

        

        return self.scale*func_value

    def derivative(self, *args, **kwargs):
        self.nr_der_calls += 1
        import math

        t = Timer()
        t.start()
        out = ReducedFunctional.derivative(self, forget = False)
        back_time = t.stop()
        logger.warning("Backward time = {}".format(back_time))
        self.backward_times.append(back_time)
        for num in out[0].vector().array():
            if math.isnan(num):
                raise Exception("NaN in adjoint gradient calculation.")

        gathered_out = gather_broadcast(out[0].vector().array())
        
        return self.scale*gathered_out



class RealValueProjector(object):
    """
    Projects onto a real valued function in order to force dolfin-adjoint to
    create a recording.
    """
    def __init__(self, u,v, mesh_vol):
        self.u_trial = u
        self.v_test = v
        self.mesh_vol = mesh_vol
    
        
    def project(self, expr, measure, real_function, mesh_vol_divide = True):

        if mesh_vol_divide:
            solve((self.u_trial*self.v_test/self.mesh_vol)*dx == \
                  self.v_test*expr*measure,real_function)
            
        else:
            solve((self.u_trial*self.v_test)*dx == \
              self.v_test*expr*measure,real_function)

        return real_function


def subplus(x):
    return conditional(ge(x, 0.0), x, 0.0)

def heaviside(x):
    return conditional(ge(x, 0.0), 1.0, 0.0)


class HolzapfelOgden(object):
    def __init__(self, f0 = None, gamma = None, params = None):

        
        self.f0 = f0
        self.gamma = Constant(0, name="gamma") if gamma is None else gamma        
        if params is None:
            params = self.default_parameters()

        for k,v in params.iteritems():
            setattr(self, k, v)


    def default_parameters(self):
        return {"a":0.291, "a_f":2.582, 
                "b":5.0, "b_f":5.0}


    def W_1(self, I_1):
        """
        Isotropic contribution.
        """
        a = self.a
        b = self.b

        return a/(2.0*b) * (exp(b*(I_1 - 3)) - 1)
        
    def W_4(self, I_4):
        """
        Anisotropic contribution.
        """
        a = self.a_f
        b = self.b_f

        if I_4 == 0:
            return 0
 
        return a/(2.0*b) * heaviside(I_4 - 1) * (exp(b*pow(I_4 - 1, 2)) - 1)
    
        
    def I1(self, F):
        """
        First Isotropic invariant
        """
        C = F.T * F
        J = det(F)
        Jm23 = pow(J, -float(2)/3)
        return  Jm23 * tr(C)

    def I4f(self, F):
        """
        Quasi invariant in fiber direction
        """
        if self.f0 is None:
            return Constant(0.0)

        C = F.T * F
        J = det(F)
        Jm23 = pow(J, -float(2)/3)
        return Jm23 * inner(C*self.f0, self.f0) 


    def strain_energy(self, F):
        """
        Strain-energy density function.
        """
        # Activation
        if self.gamma.value_size() == 17:
            # This means a regional gamma
            # Could probably make this a bit more clean
            gamma = self.gamma.get_function()
        else:
            gamma = self.gamma
            

        # Invariants
        I1  = self.I1(F)
        I4f =  self.I4f(F)
 
        mgamma = 1 - gamma
        I1e   = mgamma * I1 + (1/mgamma**2 - mgamma) * I4f
        I4fe  = 1/mgamma**2 * I4f
            
            
        W1   = self.W_1(I1e)
        W4f  = self.W_4(I4fe)
            
        W = W1 + W4f

        return W

class RegionalGamma(dolfin.Function):
    def __init__(self, meshfunction):
        
        mesh = meshfunction.mesh()
        V  = dolfin.VectorFunctionSpace(mesh, "R", 0, dim = 17)
        
        dolfin.Function.__init__(self, V)
        self._meshfunction = meshfunction

        # Functionspace for the indicator functions
        self._IndSpace = dolfin.FunctionSpace(mesh, "DG", 0)
       
        # Make indicator functions
        self._ind_functions = []
        for i in range(1,18):
            self._ind_functions.append(self._make_indicator_function(i))

    def get_function(self):
        """
        Return linear combination of coefficents
        and basis functions

        *Returns*
           fun (dolfin.Function)
             A function with gamma values at each segment
             
        """
        return self._sum()

    def _make_indicator_function(self, marker):
        dm = self._IndSpace.dofmap()
        cell_dofs = [dm.cell_dofs(i) for i in
                     np.where(self._meshfunction.array() == marker)[0]]
        dofs = np.unique(np.array(cell_dofs))
        
        f = dolfin.Function(self._IndSpace)
        f.vector()[dofs] = 1.0    
        return f  

    def _sum(self):
        coeffs = dolfin.split(self)
        fun = coeffs[0]*self._ind_functions[0]

        for c,f in zip(coeffs[1:], self._ind_functions[1:]):
            fun += c*f

        return fun
