#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
from dolfin import *
import numpy as np
import h5py, yaml, os
from adjoint_contraction_args import *
from numpy_mpi import *



def write_opt_results_to_h5(h5group, params, ini_for_res, for_result_opt, 
                            opt_matparams = None, opt_gamma = None, opt_result = None):

    filename = params["sim_file"]
    filedir = os.path.abspath(os.path.dirname(params["sim_file"]))
    if not os.path.exists(filedir) and mpi_comm_world().rank == 0:
        os.makedirs(filedir)
        write_append = "w"

    if os.path.isfile(filename):
        # Open the file in h5py
        h5file_h5py = h5py.File(filename, 'a')
        # Check if the group allready exists
        if h5group in h5file_h5py:
            # If it does, delete it
            if mpi_comm_world().rank == 0:
                del h5file_h5py[h5group]
        # Close the file
        h5file_h5py.close()
        # Open the file in HDF5File format
        open_file_format = "a"
        
    else:
        open_file_format = "w"
        
    
    
    with HDF5File(mpi_comm_world(), filename, open_file_format) as h5file:

        def save_data(data, name):
            # Need to do this in order to not get duplicates in parallell
            if hasattr(data, "__len__"):
                # Size of mesh needs to be big enough so that it can be distrbuted
                f = Function(VectorFunctionSpace(UnitSquareMesh(100,100), "R", 0, dim = len(data)))
            else:
                f = Function(FunctionSpace(UnitSquareMesh(100,100), "R", 0))

            f.assign(Constant(data))
            h5file.write(f.vector(), h5group + name)

        def dump_parameters_to_attributes(params, group):

            for k,v in params.iteritems():

                if isinstance(v, Parameters) or isinstance(v, dict):
                    for k_sub, v_sub in v.iteritems():
                        h5file.attributes(group)["{}/{}".format(k,k_sub)] = v_sub

                else:
                    if isinstance(v, np.bool_): v = bool(v)
                    if isinstance(v, int): v = abs(v)

                    h5file.attributes(group)[k] = v
                    

        # Parameters
        if opt_matparams:
            h5file.write(opt_matparams, h5group + "/parameters/optimal_material_parameters_function")
            h5file.write(opt_matparams.vector(), h5group + "/parameters/optimal_material_parameters")
            save_data(params["Material_parameters"].values(), "/parameters/initial_material_parameters")

            h5file.write(Function(FunctionSpace(for_result_opt.phm.mesh, 'R', 0)), 
                         h5group + "/parameters/activation_parameter_function")
            save_data(0.0, "/parameters/activation_parameter")
            

        if opt_gamma:
            h5file.write(opt_gamma, h5group  + "/parameters/activation_parameter_function")
            h5file.write(opt_gamma.vector(), h5group  + "/parameters/activation_parameter")
            save_data(for_result_opt.gamma_gradient, "/parameters/activation_parameter_gradient_size")
            save_data(for_result_opt.reg_par, "/input/regularization_parameter")
                



        # Input parameters
        h5file.attributes(h5group + "/parameters")["material parameters"] = \
          "a, b, a_f, b_f in transversely isotropic Holzapfel and Ogden model"
        h5file.attributes(h5group + "/parameters")["activation parameter"] = \
          "Active contraction in fiber direction. Value between 0 and 1 where 0 (starting value) is no contraction and 1 (infeasable) is full contraction"
        dump_parameters_to_attributes(params, h5group)
        # Dump parameters to yaml file as well
        with open(filedir+"/parameters.yml", 'w') as parfile:
            yaml.dump(params.to_dict(), parfile, default_flow_style=False)

        # Optimization results
        if opt_result is not None:
            controls = opt_result.pop("controls", [0])
            for it, c in enumerate(controls):
                h5file.write(c, h5group + "/controls/{}".format(it))

            func_vals= np.array(opt_result.pop("func_vals", [0]))
            save_data(func_vals, "/funtion_values")
            for_times= np.array(opt_result.pop("forward_time", [0]))
            save_data(for_times, "/forward_times")
            back_times= np.array(opt_result.pop("backward_time", [0]))
            save_data(back_times, "/backward_times")
            
            if opt_result and isinstance(opt_result, dict):
                dump_parameters_to_attributes(opt_result, h5group)
        

        # States
        for i, w in enumerate(for_result_opt.states):
            assign_to_vector(for_result_opt.phm.solver.get_state().vector(), gather_broadcast(w.array()))
            h5file.write(for_result_opt.phm.solver.get_state(), h5group + "/states/{}".format(i))


        # Strain traces
        
        for region in STRAIN_REGION_NUMS:
            for i, s in enumerate(for_result_opt.strains[region-1]):
                assign_to_vector(for_result_opt.phm.strains[region-1].vector(), gather_broadcast(s.array()))
                h5file.write(for_result_opt.phm.strains[region-1].vector(),  h5group + "/strains/{}/region_{}".format(i,region))

        # Functional, initial and optimal
        save_data(ini_for_res.func_value_strain,  "/misfit/misfit_functional/initial/strain")
        save_data(ini_for_res.func_value_volume,  "/misfit/misfit_functional/initial/volume")
        save_data(for_result_opt.func_value_strain, "/misfit/misfit_functional/optimal/strain")
        save_data(for_result_opt.func_value_volume, "/misfit/misfit_functional/optimal/volume")

        save_data(ini_for_res.weighted_func_value_strain, "/misfit/weighted_misfit_functional/initial/strain")
        save_data(ini_for_res.weighted_func_value_volume, "/misfit/weighted_misfit_functional/initial/volume")
        save_data(for_result_opt.weighted_func_value_strain, "/misfit/weighted_misfit_functional/optimal/strain")
        save_data(for_result_opt.weighted_func_value_volume, "/misfit/weighted_misfit_functional/optimal/volume")

        # Pressure and volume
        save_data(for_result_opt.lv_pressures, "/lv_pressures")
        save_data(for_result_opt.volumes, "/volume")
