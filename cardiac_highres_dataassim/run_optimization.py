#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
from dolfin import *
from dolfin_adjoint import *
from setup_optimization import setup_simulation, logger, MyReducedFunctional
from utils import Text, Object, pformat, print_optimization_report, contract_point_exists, get_spaces
from forward_runner import ActiveForwardRunner, PassiveForwardRunner

from numpy_mpi import *
from adjoint_contraction_args import *
from scipy.optimize import minimize as scipy_minimize
from store_opt_results import write_opt_results_to_h5


def run_passive_optimization(params, patient):    

    logger.info(Text.blue("\nRun Passive Optimization"))

    #Load patient data, and set up the simulation
    measurements, solver_parameters, p_lv, paramvec = setup_simulation(params, patient)

    rd, paramvec = run_passive_optimization_step(params, 
                                                 patient, 
                                                 solver_parameters, 
                                                 measurements, 
                                                 p_lv, paramvec)

    logger.info("\nSolve optimization problem.......")
    solve_oc_problem(params, rd, paramvec)


def run_passive_optimization_step(params, patient, solver_parameters, measurements, p_lv, paramvec):
    
    mesh = solver_parameters["mesh"]
    spaces = get_spaces(mesh)
    crl_basis = (patient.e_circ, patient.e_rad, patient.e_long)
     
    
    #Solve calls are not registred by libajoint
    logger.debug(Text.yellow("Stop annotating"))
    parameters["adjoint"]["stop_annotating"] = True
    
    # Load target data
    target_data = load_target_data(measurements, params, spaces)

    
    # Start recording for dolfin adjoint 
    logger.debug(Text.yellow("Start annotating"))
    parameters["adjoint"]["stop_annotating"] = False

       
    #Initialize the solver for the Forward problem
    for_run = PassiveForwardRunner(solver_parameters, 
                                   p_lv, 
                                   target_data,  
                                   crl_basis,
                                   params, 
                                   spaces, 
                                   paramvec)

    #Solve the forward problem with guess results (just for printing)
    logger.info(Text.blue("\nForward solution at guess parameters"))
    forward_result, _ = for_run(paramvec, False)
    

    # Stop recording
    logger.debug(Text.yellow("Stop annotating"))
    parameters["adjoint"]["stop_annotating"] = True

    # Initialize MyReducedFuctional
    rd = MyReducedFunctional(for_run, paramvec)
    
    return rd, paramvec


def run_active_optimization(params, patient):
    
    logger.info(Text.blue("\nRun Active Optimization"))

    #Load patient data, and set up the simulation
    measurements, solver_parameters, p_lv, gamma = setup_simulation(params, patient)
    
    # Loop over contract points
    for i in range(patient.num_contract_points):
        params["active_contraction_iteration_number"] = i
        if not contract_point_exists(params):
            
            rd, gamma = run_active_optimization_step(params, patient, 
                                                     solver_parameters, 
                                                     measurements, p_lv, 
                                                     gamma)


            logger.info("\nSolve optimization problem.......")
            solve_oc_problem(params, rd, gamma)
            adj_reset()

def run_active_optimization_step(params, patient, solver_parameters, measurements, p_lv, gamma, passive_group = None):

    
    # Circumferential, radial and logitudinal basis vectors
    crl_basis = (patient.e_circ, patient.e_rad, patient.e_long)
    mesh = solver_parameters["mesh"]

    # Initialize spaces
    spaces = get_spaces(mesh)
    

    #Get initial guess for gamma
    if  params["active_contraction_iteration_number"] == 0:
        zero = Constant(0.0) if gamma.value_size() == 1 \
          else Constant([0.0]*gamma.value_size())

        gamma.assign(zero)
       
    else:
        # Use gamma from the previous point as initial guess
        # Load gamma from previous point
        with HDF5File(mpi_comm_world(), params["sim_file"], "r") as h5file:
            h5file.read(gamma, "alpha_{}/active_contraction/contract_point_{}/parameters/activation_parameter_function/".format(params["alpha"], params["active_contraction_iteration_number"]-1))
  
        

    logger.debug(Text.yellow("Stop annotating"))
    parameters["adjoint"]["stop_annotating"] = True
    
    target_data = load_target_data(measurements, params, spaces)

    logger.debug(Text.yellow("Start annotating"))
    parameters["adjoint"]["stop_annotating"] = False
   
    for_run = ActiveForwardRunner(solver_parameters,
                                  p_lv,
                                  target_data,
                                  params,
                                  gamma, 
                                  patient, 
                                  spaces, 
                                  passive_group = passive_group)

    #Solve the forward problem with guess results (just for printing)
    logger.info(Text.blue("\nForward solution at guess parameters"))
    forward_result, _ = for_run(gamma, False)

    # Stop recording
    logger.debug(Text.yellow("Stop annotating"))
    parameters["adjoint"]["stop_annotating"] = True

    # Compute the functional as a pure function of gamma
    rd = MyReducedFunctional(for_run, gamma, params["Optimization_parmeteres"]["scale"])

    # Evaluate the reduced functional in case the solver chrashes at the first point.
    # If this is not done, and the solver crashes in the first point
    # then Dolfin adjoit has no recording and will raise an exception.
    rd(gamma)

            
    return rd, gamma


def store(params, rd, opt_controls, opt_result=None):

    logger.info("Store solution")
    if params["phase"] == PHASES[0]:

        h5group =  PASSIVE_INFLATION_GROUP.format(params["alpha_matparams"])
        write_opt_results_to_h5(h5group, params, rd.ini_for_res, rd.for_res, 
                                opt_matparams = opt_controls, 
                                opt_result = opt_result)
    else:
        
        h5group =  ACTIVE_CONTRACTION_GROUP.format(params["alpha"], params["active_contraction_iteration_number"])
        write_opt_results_to_h5(h5group, params, rd.ini_for_res, rd.for_res, 
                                opt_gamma = opt_controls, opt_result = opt_result)


def solve_oc_problem(params, rd, paramvec, store_results=True):

    paramvec_arr = gather_broadcast(paramvec.vector().array())
    opt_params = params["Optimization_parmeteres"]

    if params["phase"] == PHASES[0] and not params["optimize_matparams"]:
        rd(paramvec_arr)
        store(params, rd, paramvec)

    else:

        # Number of control variables
        nvar = len(paramvec_arr)

        if params["phase"] == PHASES[0]:

            lb = np.array([opt_params["matparams_min"]]*nvar)
            ub = np.array([opt_params["matparams_max"]]*nvar)

            # Set the upper and lower bounds equal for a_f, b, and b_f
            lb[1:] = ub[1:] = paramvec_arr[1:]

            tol = opt_params["passive_opt_tol"]
            max_iter = opt_params["passive_maxiter"]

        else:
                
            lb = np.array([0.0]*nvar)
            ub = np.array([opt_params["gamma_max"]]*nvar)

            tol= opt_params["active_opt_tol"]
            max_iter = opt_params["active_maxiter"]

        
    
        method = opt_params["method"]

        kwargs = {"method": method,
                  "bounds":zip(lb,ub),
                  "jac": rd.derivative,
                  "tol":tol,
                  "options": {"disp": opt_params["disp"],
                              "maxiter":max_iter}}


        # Start a timer to measure duration of the optimization
        t = Timer()
        t.start()
        # Solve the optimization problem
        opt_result = scipy_minimize(rd,paramvec_arr, **kwargs)
        run_time = t.stop()
        opt_result["ncrash"] = rd.nr_crashes
        opt_result["run_time"] = run_time
        opt_result["controls"] = rd.controls_lst
        opt_result["func_vals"] = rd.func_values_lst
        opt_result["forward_time"] = rd.forward_times
        opt_result["backward_time"] = rd.backward_times

        x = gather_broadcast(rd.controls_lst[np.argmin(rd.func_values_lst)].array())

        assign_to_vector(paramvec.vector(), x)

        val = rd.for_run(paramvec, False)
        
        logger.info(Text.blue("\nForward solution at optimal parameters"))
        print_optimization_report(params, rd.paramvec, rd.initial_paramvec,
                                  rd.ini_for_res, rd.for_res, opt_result)
        
        

        if store_results:
            store(params, rd, paramvec, opt_result)
        else:
            return params, rd, paramvec, opt_result



def load_target_data(measurements, params, spaces):
    logger.debug(Text.blue("Loading Target Data"))
        
    def get_strain(newfunc, i, it):
        assign_to_vector(newfunc.vector(), np.array(measurements.strain[i][it]))


    def get_volume(newvol, it):
        assign_to_vector(newvol.vector(), np.array([measurements.volume[it]]))

    # The target data is put into functions so that Dolfin-adjoint can properly record it.
 
    # Store target strains and volumes
    target_strains = []
    target_vols = []
    target_disps = []

    acin = params["active_contraction_iteration_number"]

    if params["phase"] == PHASES[0]:
        pressures = measurements.pressure
    else:
        pressures = measurements.pressure[acin: 2 + acin]
       

    logger.info(Text.blue("Load target data"))
    logger.info("\tLV Pressure (cPa) \tLV Volume (mL)")
   

    for it, p in enumerate(pressures):


        if params["optimize_displacements"]:
            newfunc = Function(spaces.displacementspace, name = "target_displacement_{}".format(acin+it))
            assign_to_vector(newfunc.vector(), \
                             gather_broadcast(measurements.disps[acin+it].array()))
            
            target_disps.append(newfunc)
        
        if params["use_deintegrated_strains"]:
            newfunc = Function(spaces.strainfieldspace, name = \
                               "strain_{}".format(acin+it))
          
            assign_to_vector(newfunc.vector(), \
                             gather_broadcast(measurements.strainfield[acin+it].array()))
            
            target_strains.append(newfunc)
            
        else:
            strains_at_pressure = []
            for i in STRAIN_REGION_NUMS:
                newfunc = Function(spaces.strainspace, name = "strain_{}_{}".format(acin+it, i))
                get_strain(newfunc, i, acin+it)
                strains_at_pressure.append(newfunc)

            target_strains.append(strains_at_pressure)

        newvol = Function(spaces.r_space, name = "newvol")
        get_volume(newvol, acin+it)
        target_vols.append(newvol)

        logger.info("\t{:.3f} \t\t\t{:.3f}".format(p,gather_broadcast(target_vols[-1].vector().array())[0]))

    target_data = Object()
    target_data.target_strains = target_strains
    target_data.target_vols = target_vols
    target_data.target_pressure = pressures
    target_data.target_disps = target_disps

    return target_data
