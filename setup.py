# System imports
from distutils.core import setup



# Version number
major = 1
minor = 0

setup(name = "cardiac_highres_dataassim",
      version = "{0}.{1}".format(major, minor),
      description = """
      An adjointable cardiac mechanics solver and data assimilator.
      """,
      author = "Henrik Finsberg",
      author_email = "henriknf@simula.no",
      packages = ["cardiac_highres_dataassim"],
      package_dir = {"cardiac_highres_dataassim": "cardiac_highres_dataassim"},
      )
