 #!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
import  h5py, pickle, sys, yaml, shutil
import numpy as np
from copy import deepcopy

try:
    import seaborn as sns
    has_seaborn = True
except:
    has_seaborn = False

try:
    import matplotlib as mpl
    from matplotlib import pyplot as plt, rcParams, cbook, ticker, cm
    from matplotlib.colors import LogNorm
    from matplotlib.ticker import MultipleLocator, FormatStrFormatter
    has_matplotlib = True
except:
    print "Warning: Matplotlib not found"
    has_matplotlib = False

from dolfin import *
from cardiac_highres_dataassim.utils import  AutoVivification, Text,  pformat, passive_inflation_exists, contract_point_exists, get_spaces as get_strain_spaces
from cardiac_highres_dataassim.setup_optimization import RegionalGamma
from cardiac_highres_dataassim.latex_utils import *
from cardiac_highres_dataassim.adjoint_contraction_args import *

from demo import setup_application_parameters, load_patient_data, setup_general_parameters

try:
    from dolfinplot import *
except:
    print "Warning: dolfinplot is missing"
    print "Snapshots cannot be plotted directly"
    print "Get repo: git clone git@bitbucket.org:finsberg/dolfinplot.git"


STRAIN_REGION_NAMES = {1:"Anterior",
                       2:"Anteroseptal",
                       3:"Septum",
                       4:"Inferior",
                       5:"Posterior",
                       6:"Lateral",
                       7:"Anterior",
                       8:"Anteroseptal",
                       9:"Septum",
                       10:"Inferior",
                       11:"Posterior",
                       12:"Lateral",
                       13:"Anterior",
                       14:"Septum",
                       15:"Inferior",
                       16:"Lateral",
                       17:"Apex"}

STRAIN_REGIONS = {1:"LVBasalAnterior",
                 2:"LVBasalAnteroseptal",
                 3:"LVBasalSeptum",
                 4:"LVBasalInferior",
                 5:"LVBasalPosterior",
                 6:"LVBasalLateral",
                 7:"LVMidAnterior",
                 8:"LVMidAnteroseptal",
                 9:"LVMidSeptum",
                 10:"LVMidInferior",
                 11:"LVMidPosterior",
                 12:"LVMidLateral",
                 13:"LVApicalAnterior",
                 14:"LVApicalSeptum",
                 15:"LVApicalInferior",
                 16:"LVApicalLateral",
                 17:"LVApex"}

parameters["allow_extrapolation"] = True

# Plotting options
if has_seaborn:
    sns.set_palette("husl")
    sns.set_style("white")
    sns.set_style("ticks")
    sns.set_context("poster")
    
if has_matplotlib:
    mpl.rcParams.update({'figure.autolayout': True})
    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 50} 

    mpl.rc('font', **font)
    mpl.pyplot.rc('text', usetex=True)
    mpl.rcParams['text.usetex']=True
    mpl.rcParams['xtick.labelsize'] = 40
    mpl.rcParams['ytick.labelsize'] = 40
    mpl.rcParams['legend.fontsize'] = 40
    mpl.rcParams['axes.titlesize'] = 50
    mpl.rcParams['axes.labelsize'] = 50
    
    # mpl.rcParams['figure.dpi'] = 30
    mpl.rcParams['savefig.dpi'] = 300
    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.unicode']=True
    # cmap = plt.get_cmap('jet')
    cmap = plt.get_cmap('gist_rainbow')
    # cmap = plt.cm.get_cmap('RdYlBu')

    from itertools import cycle
    linestyles = cycle(['-', '--', ':'])
    

####### Save stuff ################
def save_dict_to_h5(d, fname):
    """Create a HDF5 file and put the
    data in the dictionary in the 
    same hiearcy in the HDF5 file
    
    Assume leaf of dictionary is either
    float, numpy.ndrray, list or 
    dolfin.GenericVector.

    :param d: Dictionary to be saved
    :param fname: Name of the file where you want to save

    .. note:: 

        Works only in serial
    
    """
    if os.path.isfile(fname):
        os.remove(fname)

    with HDF5File(mpi_comm_world(), fname, "w") as h5file:

        def dict2h5(a, group):

            for key, val in a.iteritems():
                
                subgroup = "/".join([group, str(key)])

                if isinstance(val, dict):
                    dict2h5(val, subgroup)
                
                elif isinstance(val, list) or isinstance(val, np.ndarray):
                    
                    if isinstance(val[0], Vector):
                        for i, f in enumerate(val):
                            h5file.write(f, subgroup + "/{}".format(i))

                    elif isinstance(val[0], float) or isinstance(val[0], int):
                        h5file.write(np.array(val, dtype=float), subgroup)
                        
                    else:
                        raise ValueError("Unknown type {}".format(type(val[0])))
                    
                elif isinstance(val, float) or isinstance(val, int):
                    h5file.write(np.array([float(val)]), subgroup)

                else:
                    raise ValueError("Unknown type {}".format(type(val)))

        dict2h5(d, "")
        

    
####### Compute stuff ################
def compute_inner_cavity_volume(mesh, ffun):    
    """Compute cavity volume using the divergence theorem

    :param mesh: The mesh (:py:class:`dolfin.Mesh`)
    :param ffun: Facet function (:py:class:`dolfin.MeshFunction`)
        
    :returns: Volume of inner cavity
    :rtype: float
    """
    
    X = SpatialCoordinate(mesh)
    N = FacetNormal(mesh)
    ds = Measure("exterior_facet", subdomain_data = ffun, domain = mesh)(30)
    
    vol = assemble((-1.0/3.0)*dot(X, N)*ds)
    return vol
def get_global(dx, fun, fun_lst):
    """Get average value of function

    :param dx: Volume measure marked according of AHA segments
    :param fun: A dolfin function in the coorect spce
    :param fun_lst: A list of vectors that should be averaged
    :returns: list of average values (one for each element in fun_list)
    :rtype: list of floats

    """

    regions = range(17)
    meshvols = []

    for i in regions:
        meshvols.append(assemble(Constant(1.0)*dx(i+1)))

    meshvol = np.sum(meshvols)

    fun_mean = []
    for f in fun_lst:
        fun.vector()[:] = f

        if fun.value_size() == 17:
            fun_tot = np.sum(np.multiply(fun.vector().array(), meshvols))
            fun_mean.append(fun_tot/meshvol)
            
        else:
            fun_tot = 0
            for i in regions:            
                fun_tot += assemble((fun)*dx(i+1))

            fun_mean.append(fun_tot/meshvol)
 
    return fun_mean

def get_regional(dx, fun):
    """Return the average value of the function 
    in each segment

    :param dx: Volume measure marked according of AHA segments
    :param fun: The function that should be averaged
    :returns: The average value in each of the 17 AHA segment
    :rtype: list of floats

    """
    

    meshvols = []

    regions = range(17)
    
    for i in regions:
        meshvols.append(Constant(assemble(Constant(1.0)*dx(i+1))))

    lst = [] 
    for i in range(17):
        lst.append(assemble((fun/meshvols[i])*dx(i+1)))
    return np.array(lst)

####### Load stuff ################
def load_dict(fname):
    """
    Load the given h5file into
    a dictionary
    """
    
    assert os.path.isfile(fname), \
        "File {} does not exist".format(fname)

    with h5py.File(fname, "r") as h5file:

        def h52dict(hdf):
            if isinstance(hdf, h5py._hl.group.Group):
                t = {}
        
                for key in hdf.keys():
                    t[str(key)] = h52dict(hdf[key])
    
                
            elif isinstance(hdf, h5py._hl.dataset.Dataset):
                t = np.array(hdf)

            return t
        
        d = h52dict(h5file)

    return d

def get_synthetic_data(params, patient):

    if params["passive_synth"]:
        num_points = patient.passive_filling_duration
    else:
        num_points = patient.passive_filling_duration \
                     + patient.num_contract_points 

    noise = params["noise"]
    
    synth_data = {"volume":[], "volume_w_noise":[], "pressure":[],
                  "states":[], "gammas":[], "displacements":[]}
    
    strain_dict = {strain : {i:[] for i in STRAIN_REGION_NUMS}  \
                   for strain in STRAIN_NUM_TO_KEY.values()}
    
    synth_data["strain"] = deepcopy(strain_dict)
    synth_data["strain_original"] = deepcopy(strain_dict) if noise else None
    synth_data["strain_w_noise"] = deepcopy(strain_dict) if noise else None
    synth_data["strain_corrected"] = deepcopy(strain_dict) if noise else None
    
    synth_str = "synth_data/point_{}"            

    with h5py.File(params["sim_file"] , "r") as h5file:
        for point in range(num_points):
            
            h5group = synth_str.format(point)
            # Get strains
            for region in STRAIN_REGION_NUMS:
                strain_arr = np.array(h5file[h5group + "/strain/region_{}".format(region)])
                if noise:
                    strain_arr_noise = np.array(h5file[h5group + "/strain_w_noise/region_{}".format(region)])
                    strain_arr_orig = np.array(h5file[h5group + "/original_strain/region_{}".format(region)])
                    strain_arr_corr = np.array(h5file[h5group + "/corrected_strain/region_{}".format(region)])

                
                for direction in STRAIN_NUM_TO_KEY.keys():
                    synth_data["strain"][STRAIN_NUM_TO_KEY[direction]][region].append(strain_arr[direction])
                    
                    if noise:
                        synth_data["strain_w_noise"][STRAIN_NUM_TO_KEY[direction]][region].append(strain_arr_noise[direction])
                        synth_data["strain_original"][STRAIN_NUM_TO_KEY[direction]][region].append(strain_arr_orig[direction])
                        synth_data["strain_corrected"][STRAIN_NUM_TO_KEY[direction]][region].append(strain_arr_corr[direction])

           
            # Get volume
            if noise:
                v = np.array(h5file[h5group + "/original_volume"])
            else:
                v = np.array(h5file[h5group + "/volume"])
            synth_data["volume"].append(v[0])
            if noise:
                vn = np.array(h5file[h5group + "/volume_w_noise"])
                synth_data["volume_w_noise"].append(vn[0])
            
            # Get pressure
            p = np.array(h5file[h5group + "/pressure"])
            synth_data["pressure"].append(p[0]*10) # kPa
            
   
    ### DOLFIN DATA ###
    kwargs = init_spaces(patient.mesh, params["gamma_space"])
    
   
    gamma = Function(kwargs["gamma_space"], name = "Contraction Parameter")
    state = Function(kwargs["state_space"], name = "State")
   
    with HDF5File(mpi_comm_world(), params["sim_file"], "r") as h5file:

   
        for point in range(num_points):

            h5group = synth_str.format(point)
            
            h5file.read(state, h5group + "/state")
            h5file.read(gamma, h5group + "/activation_parameter")

            u,p = state.split(deepcopy=True)
            synth_data["displacements"].append(Vector(u.vector()))
            synth_data["states"].append(Vector(state.vector()))
            synth_data["gammas"].append(Vector(gamma.vector()))
            

    return synth_data
    
def get_data(params, patient, single = True):
    """
    Get data from result file
    """

    strain_dict = {strain : {i:[] for i in STRAIN_REGION_NUMS}  for strain in STRAIN_NUM_TO_KEY.values()}

    data = {"misfit":{"passive":{}, "active":{}}, "volume":[],
            "pressure":[], "strain": deepcopy(strain_dict),
            "states":[], "gammas":[], "displacements":[],
            "material_parameters": {}, "gamma_gradient":[],
            "controls":[], "function_values":[], "active_attrs":[]}
    for s in ["optimal", "initial"]:
        data["misfit"]["active"]["I_strain_{}".format(s)] = []
        data["misfit"]["active"]["I_volume_{}".format(s)] = []

    # Passive group names
    if single:
        passive_group = 'alpha_{}/passive_inflation'.format(params["alpha_matparams"])
    else:
        passive_group = 'alpha_{}/reg_par_0.0/passive_inflation'.format(params["alpha_matparams"])
        
    
    passive_misfit_str = "/".join([passive_group, "misfit/misfit_functional/{}/{}"])
    passive_volume_str = "/".join([passive_group, "volume"])
    passive_pressure_str = "/".join([passive_group, "lv_pressures"])
    passive_strain_str = "/".join([passive_group, "strains/{}/region_{}"])
    passive_matparams_str = "/".join([passive_group, "parameters/{}_material_parameters"])
    passive_controls_str = "/".join([passive_group, "controls/{}"])
    passive_funcvals_str = "/".join([passive_group+passive_group, "funtion_values"]) # some typos here

    if not os.path.isfile(params["sim_file"]):
        raise IOError("File {} does not exist".format(sim_file))

    with h5py.File(params["sim_file"] , "r") as h5file:
        
        
        ## Passive inflation
        if passive_group in h5file:

            # Volume
            data["volume"] = np.array(h5file[passive_volume_str]).tolist()

            # Pressure
            data["pressure"] = np.array(h5file[passive_pressure_str]).tolist()

            try:
                data["function_values"].append(np.array(h5file[passive_funcvals_str]).tolist())
                controls = {}
                it = 0
                while passive_controls_str.format(it) in h5file:
                    controls[str(it)] = np.array(h5file[passive_controls_str.format(it)]).tolist()
                    it+=1
                data["controls"].append(controls)
            except:
                # Not implement for all results
                pass
            

            # Strain
            for point in range(patient.passive_filling_duration):
                for region in STRAIN_REGION_NUMS:
                    strain_arr = np.array(h5file[passive_strain_str.format(point,region)])
                    for direction in STRAIN_NUM_TO_KEY.keys():
                        data["strain"][STRAIN_NUM_TO_KEY[direction]][region].append(strain_arr[direction])

            for s in ["optimal", "initial"]:

                # Material parameters
                data["material_parameters"][s] = np.array(h5file[passive_matparams_str.format(s)])

                # Misfit
                data["misfit"]["passive"]["I_strain_{}".format(s)] = \
                  np.array(h5file[passive_misfit_str.format(s, "strain")])[0]
                data["misfit"]["passive"]["I_volume_{}".format(s)] = \
                  np.array(h5file[passive_misfit_str.format(s, "volume")])[0]
                
        else:
            raise IOError("No data available")


           
        ## Active contraction
        if single:
            active_group = "alpha_{}/active_contraction/".format(params["alpha"])
        else:
            active_group = "alpha_{}/reg_par_{}/active_contraction/".format(params["alpha"], params["reg_par"])

        active_point = "/".join([active_group, "contract_point_{}"])
        
        p = 0
        if active_group in h5file:
            
            while "contract_point_{}".format(p) in h5file[active_group].keys():

                # Active group names
                active_misfit_str = "/".join([active_point.format(p), 
                                              "misfit/misfit_functional/{}/{}"])
                active_volume_str = "/".join([active_point.format(p), "volume"])
                active_pressure_str = "/".join([active_point.format(p), "lv_pressures"])
                active_strain_str = "/".join([active_point.format(p), 
                                              "strains/0/region_{}"])
                active_grad_str = "/".join([active_point.format(p),
                                            "parameters/activation_parameter_gradient_size"])
                active_funcvals_str = "/".join([active_point.format(p)+active_point.format(p), # some typos here
                                                "funtion_values"])
                active_controls_str = "/".join([active_point.format(p), "controls/{}"])

                # Volume
                data["volume"].append(np.array(h5file[active_volume_str])[0])

                # Pressure
                data["pressure"].append(np.array(h5file[active_pressure_str])[0])

                try:
                    # The functional values
                    data["function_values"].append(np.array(h5file[active_funcvals_str]).tolist())
                    controls = {}
                    it = 0
                    while active_controls_str.format(it) in h5file:
                        controls[str(it)] = np.array(h5file[active_controls_str.format(it)]).tolist()
                        it+=1
                    data["controls"].append(controls)
                except:
                    # Not implemented for all results
                    pass

                # Strain
                for region in STRAIN_REGION_NUMS:
                    strain_arr = np.array(h5file[active_strain_str.format(region)])

                
                    for direction in STRAIN_NUM_TO_KEY.keys():
                    
                        data["strain"][STRAIN_NUM_TO_KEY[direction]][region].append(strain_arr[direction])

                # Gamma gradient
                data["gamma_gradient"].append(np.array(h5file[active_grad_str])[0])

                # Misfit
                for s in ["optimal", "initial"]:
                    data["misfit"]["active"]["I_strain_{}".format(s)].append(
                        np.array(h5file[active_misfit_str.format(s, "strain")])[0])
                    data["misfit"]["active"]["I_volume_{}".format(s)].append(
                          np.array(h5file[active_misfit_str.format(s, "volume")])[0])



                p += 1

    
    num_contract_points = p
           
    # Get measured data
    measured_data = load_measured_strain_and_volume(patient)

    ### DOLFIN DATA ###
    kwargs = init_spaces(patient.mesh, params["gamma_space"])
    
    # Some indices
    passive_filling_duration = patient.passive_filling_duration
    # Total points 
    num_points = patient.num_points
    

    opt_keys = ["nfev", "njev", "nit", "run_time", "ncrash", "message"]
    gamma = Function(kwargs["gamma_space"], name = "Contraction Parameter")
    state = Function(kwargs["state_space"], name = "State")
    paramvec = Function(VectorFunctionSpace(patient.mesh, "R", 0, dim = 4))
    
   
    with HDF5File(mpi_comm_world(), params["sim_file"], "r") as h5file:

        print "Reading passive data"

        data["passive_attrs"] = {}
        # Load passive data

        for key in opt_keys:
            data["passive_attrs"][key] = h5file.attributes(passive_group)[key]
            

        for pv_num in range(passive_filling_duration):
            
            h5file.read(state, passive_group + "/states/{}".format(pv_num))

            u,p = state.split(deepcopy=True)
            data["displacements"].append(Vector(u.vector()))
            data["states"].append(Vector(state.vector()))
            data["gammas"].append(Vector(gamma.vector()))
            


        # Load active data
        print "Reading active data"
    
        for pv_num in range(num_contract_points):

            h5group = active_point.format(pv_num)

            active_attrs = {}
            for key in opt_keys:
                active_attrs[key] = h5file.attributes(h5group)[key]
            data["active_attrs"].append(active_attrs)
            
            
            h5file.read(state, h5group + "/states/0")
            h5file.read(gamma, h5group + "/parameters/activation_parameter_function")
            u,p = state.split(deepcopy=True)

            data["displacements"].append(Vector(u.vector()))
            data["states"].append(Vector(state.vector()))
            data["gammas"].append(Vector(gamma.vector()))
            
   

    # Add some extra stuff to arguments
    kwargs["mesh"] = patient.mesh 
    kwargs["num_points"] = num_points
    kwargs["passive_filling_duration"] = passive_filling_duration
    kwargs["num_contract_points"] = num_contract_points
    kwargs["dx"] = Measure("dx",
                           subdomain_data = patient.strain_markers,
                           domain = patient.mesh)

    return  data, kwargs


def load_measured_strain_and_volume(patient, num_points = None):

    num_points = patient.num_points if num_points is None else num_points

    # Volume
    cav_volume = compute_inner_cavity_volume(patient.mesh, patient.facets_markers)
    volume_offset = patient.volume[0] - cav_volume
    volumes = np.subtract(patient.volume[:num_points+1], volume_offset)

    # Strain
    strains = {strain : {i:[] for i in STRAIN_REGION_NUMS} for strain in STRAIN_NUM_TO_KEY.values()}
    for region in STRAIN_REGION_NUMS:
        for direction in range(3):
            regional_strain = patient.strain[region]
            strains[STRAIN_NUM_TO_KEY[direction]][region] = np.array([s[direction] for s in regional_strain[:(num_points+1)]])


    # Pressure
    pressure = np.array(patient.pressure[:num_points+1])
    # pressure *= KPA_TO_CPA
    pressure_offset = pressure[0]
    pressure = np.subtract(pressure, pressure_offset)
   
       

    data = {}
    data["volume"] = volumes
    data["strain"] = strains
    data["pressure"] = pressure
    return data




def init_spaces(mesh, gamma_space = "CG_1"):
    
    spaces = {}
    if gamma_space == "regional":
        spaces["gamma_space"] = VectorFunctionSpace(mesh, "R", 0, dim = 17)
    else:
        gamma_family, gamma_degree = gamma_space.split("_")
        spaces["gamma_space"] = FunctionSpace(mesh, gamma_family, int(gamma_degree))
        
    spaces["displacement_space"] = VectorFunctionSpace(mesh, "CG", 2)
    spaces["state_space"] = VectorFunctionSpace(mesh, "CG", 2)*FunctionSpace(mesh, "CG", 1)
    spaces["strain_space"] = VectorFunctionSpace(mesh, "R", 0, dim=3)
   
    return spaces



####### Other utils ##############    
def asint(s):
    try: return int(s), ''
    except ValueError: return sys.maxint, s
            

####### Plot stuff ################
def save_paraview():
    
    patient = load_patient_data()

    mesh = patient.mesh
    
    e_f = patient.e_f
   
    setup_general_parameters()
    ef1 = project(e_f, VectorFunctionSpace(mesh, "CG", 1))
   
    vec = VTK_DolfinVector(ef1)
    vec.Save2vtk("data/fiber.vtk")

    
def snap_shot(fun, path, name, wild = "", rnge = (0,0.1),
              colorbar = True, colorbar_align = "vertical", colorbar_only = False):
    """Plot snap shot using vtk
    
    Args:
      fun (:py:class`dolfin.Function`)
        The function you would like to plot
      path (string)
        Path to where to save the figure
      name (string)
        Name of the object you want to plot
      wild (string)
        Additional information about the object (optional)

    """

    if colorbar_only:
        vtkfun = VTK_Dolfin(rnge)
        height = 0.17
        width = 0.8
    else:
        vtkfun = VTK_DolfinScalar(fun)
        height = 0.8
        width = 0.17
        
    if name == "gamma":
        vtkfun.SetColorMap(hue=(0.0, 0.66),
                           saturation=(1.0, 1.0),
                           ncolors = 256,
                           values = (1.0, 1.0))
        vtkfun.SetRange(rnge)

    if colorbar:
        vtkfun.SetColorBar(title = " ".join([name, wild]), width = width, height=height,
                           orientation = colorbar_align,label_fmt = '%.1f')
    vtkfun.SetEdgeColor((0.6,0, 0))

    

    # Plot side
    vtkfun.camera.SetPosition(-18.0, 0.0, 0.0)
    vtkfun.camera.SetFocalPoint(4.5, 0.0, 0.0)
    vtkfun.camera.SetViewUp(0.0160, -0.68, 0.7242)
    vtkfun.Render(view="custom", size = (1200, 800))
    vtkfun.Save(fname = path + "_front")

    if not colorbar_only:
        # Plot front
        vtkfun.camera.SetPosition(-6.0, 15.0, -15.0)
        vtkfun.camera.SetFocalPoint(4.5, 0, 0)
        vtkfun.camera.SetViewUp(-0.8856, 0.3162, -0.3400)
        vtkfun.Clip(origin=(4.5, 0.0, 0.0), normal = (0,1,-1), overlay =True)
        vtkfun.clipmapper.SetScalarRange(rnge)
        vtkfun.clipmapper.SetLookupTable(vtkfun.colorLookupTable)
        vtkfun.Render(view="custom", size = (1200, 800))
        vtkfun.Save(fname = path + "_side")


def plot_patient_snap_shots():
    
    alpha = "0.9"
    lmbda = "0.01"

    gamma_space = "CG_1"

    patient = load_patient_data()
    
    
    

    if gamma_space == "CG_1":
        outdir = "figures/CG1/real/snapshots/alpha{}_lmbda{}".format(alpha, lmbda)
    
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        fname = "results/patient_simulation_dolfin.h5"

   
    res = load_dict(fname)


    data = res["alpha_{}".format(alpha)]["lambda_{}".format(lmbda)]
 
    gammas = data["gamma"]
    us = data["displacement"]
    times = sorted(gammas.keys(), key=asint)

    

    mesh = Mesh(patient.mesh)
    V_cg1 = FunctionSpace(mesh, "CG", 1)
    VV_cg1 = VectorFunctionSpace(mesh, "CG", 1)
    VV_cg2 = VectorFunctionSpace(mesh, "CG", 2)
    gamma = Function(V_cg1, name = "gamma")
    u = Function(VV_cg2, name = "displacement")
    u_prev = Function(VV_cg2, name = "displacement prev")
    u_diff = Function(VV_cg2, name = "displacement diff")
    
    path = "/".join([outdir, "gamma_{}"])

    
                
    for t in times:
        u.vector()[:] = us[t]
        u_diff.vector()[:] = u.vector() - u_prev.vector()
        d = interpolate(u_diff, VV_cg1)
        ALE.move(mesh, d)
        
        gamma.vector()[:] = gammas[t]
        snap_shot(gamma,  path.format(t), "gamma", rnge = (0,0.5), colorbar = False)
        u_prev.assign(u)

    # Put together a canvas
    keys = times[::3]
    labels = [r"{:.0f} $\%$".format(100*float(i)/float(times[-1])) for i in keys]
    make_canvas_snap_shot(keys,labels,path[:-3])
        
        

    

def plot_CG_1_results(case_id = 1,
                      plot_all_strains = False,
                      plot_pv_loop = False, 
                      plot_l_curve_fixed_alpha =False,
                      plot_l_curve_fixed_lambda = False,
                      plot_mean_gamma_varying_alpha = False,
                      plot_mean_gamma_varying_lambda =False,
                      outdir = ""):

    cases = ["patient",
             "synthetic_noisefree"]

    case = cases[case_id]
    
    plot_pv_loop_fixed_alpha = False
    plot_pv_loop_fixed_lambda = False
    plot_strain_trace_fixed_alpha = False
    plot_strain_trace_fixed_lambda = False
    plot_snapshots = False

    
    # Only for synthetic cases
    plot_gamma_error_fixed_alpha = False
    plot_gamma_error_fixed_lambda = False
    plot_displacement_error_fixed_alpha = False
    plot_displacement_error_fixed_lambda = False
    plot_synthetic_snapshots = False
    print_gamma_error = True
    print_disp_error = False

    

    fixed_alpha = "0.95"
    fixed_lambda = "0.0"
    if case == 1:
        snap_lambda = "1.0"
    else:
        snap_lambda = "0.01"

    # For the fixed strain trace plotting
    region = "1"
    component = "longitudinal"

    if case == cases[0]:
        
        outdir = "figures/CG1/real" if outdir == "" else outdir
        fname = "results/patient_simulation_CG1.h5"
        
    elif case == cases[1]:
        
        outdir = "figures/CG1/synthetic_noisefree" if outdir == "" else outdir
        fname = "results/synthetic_noisefree_CG1.h5"


    if not os.path.exists(outdir):
        os.makedirs(outdir)

    patient = load_patient_data(synth_data = case in cases[1:])
    alphas = [str(i/10.) for i in range(11)] + ["0.95", "0.99", "0.999", "0.9999"]
    lmbdas = ["0.0"] + [str(10**i/1000000.) for i in range(9)]

    res = load_dict(fname)

    alphas.sort()
    lmbdas.sort()

    if 0:
        meas_data = res["measured"]
        I_vol = []
       
            
        sim_data = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(fixed_lambda)]
        sim_vol = sim_data["volume"]
        meas_vol = meas_data["volume"]

 
        pfd = patient.passive_filling_duration
        I_vol = np.mean(np.divide(np.abs(np.subtract(sim_vol[:pfd],meas_vol[:pfd])), meas_vol[:pfd]))
        print I_vol
        exit()

    if plot_mean_gamma_varying_alpha:

        alphas_ = ["0.0", "0.5", "0.95", "1.0"]
        colors = [cmap(i) for i in np.linspace(0,1, len(alphas_))]
        dX = Measure("dx",subdomain_data = patient.strain_markers,
                     domain = patient.mesh)
        gamma_space = FunctionSpace(patient.mesh, "CG", 1)
        gamma = Function(gamma_space)
        fig = plt.figure()
        ax = fig.gca()

        for i, a in enumerate(alphas_):
            gs = res["simulated"]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]["gamma"]
            gamma_lst = [gs[k] for k in sorted(gs, key=asint)]
            gamma_mean = get_global(dX, gamma, gamma_lst)
            ax.plot(gamma_mean, label = r"$\alpha={}$".format(a), color = colors[i], linestyle = next(linestyles))
        # ax.legend(loc = "best")
        lgd = ax.legend(loc = "center left", bbox_to_anchor=(1, 0.5))
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\overline{\gamma}$")
        path = "/".join([outdir, "mean_gamma_varying_alpha.eps"])
        fig.savefig(path, bbox_extra_artists=(lgd,), bbox_inches='tight')
        

    if plot_mean_gamma_varying_lambda:

        lmbdas_ = ["0.0001", "0.001", "0.01", "0.1", "1.0"]
        colors = [cmap(i) for i in np.linspace(0,1, len(lmbdas_))]
        dX = Measure("dx",subdomain_data = patient.strain_markers,
                     domain = patient.mesh)
        gamma_space = FunctionSpace(patient.mesh, "CG", 1)
        gamma = Function(gamma_space)
        fig = plt.figure()
        ax = fig.gca()

        for i, l in enumerate(lmbdas_):
            gs = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(l)]["gamma"]
            gamma_lst = [gs[k] for k in sorted(gs, key=asint)]
            gamma_mean = get_global(dX, gamma, gamma_lst)
            ax.plot(gamma_mean, label = r"$\lambda={}$".format(l), color = colors[i], linestyle = next(linestyles))
            
        lgd = ax.legend(loc = "center left", bbox_to_anchor=(1, 0.5))
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\overline{\gamma}$")
        path = "/".join([outdir, "mean_gamma_varying_lambda.eps"])
        fig.savefig(path, bbox_extra_artists=(lgd,), bbox_inches='tight')

        
        
    # Plot PV loops for different lambdas
    if plot_pv_loop_fixed_alpha:
        
        fig = plt.figure()
        ax = fig.gca()
        pressure = res["measured"]["pressure"]
        volume = res["measured"]["volume"]
        ax.plot(volume, pressure, "k-*", label = "Measured")
        
        for l in lmbdas:
            v = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(l)]["volume"]
            n = len(v)
            ax.plot(v, pressure[:n], marker = "o", label = r"$\lambda = {}$".format(l))

        ax.set_xlabel("Volume (ml)")
        ax.set_ylabel("Pressure (cPa)")
        ax.legend(loc = "best")
        path = "/".join([outdir, "pv_loop_fixed_alpha_{}_{}.eps".format(fixed_alpha, case)])
        fig.savefig(path)



    # Plot PV loops for different alphas
    if plot_pv_loop_fixed_lambda:
        
        fig = plt.figure()
        ax = fig.gca()
        pressure = res["measured"]["pressure"]
        volume = res["measured"]["volume"]
        ax.plot(volume, pressure, "k-*", label = "Measured")
        
        for a in alphas:
            v = res["simulated"]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]["volume"]
            n = len(v)
            ax.plot(v, pressure[:n], marker = "o", label = r"$\alpha = {}$".format(a))

        ax.set_xlabel("Volume (ml)")
        ax.set_ylabel("Pressure (cPa)")
        ax.legend(loc = "best")
        path = "/".join([outdir, "pv_loop_fixed_lambda_{}_{}.eps".format(fixed_lambda, case)])
        fig.savefig(path)

          
    if plot_strain_trace_fixed_alpha:
        
        fig = plt.figure()
        ax = fig.gca()
        
        strain = res["measured"]["strain"][component][region]
        x = np.linspace(0,100, len(strain))
        
        for l in lmbdas:
            s = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(l)]["strain"][component][region]
            n = len(s)
            ax.plot(x[:n], s, marker = "o", label = r"$\lambda = {}$".format(l))

        ax.set_xlabel(r"$\%$ cardiac cycle")
        ax.set_ylabel("{} strain, region {}".format(component, region))
        ax.legend(loc = "best")
        path = "/".join([outdir, "{}_strain_trace_region_{}_fixed_alpha_{}.eps".format(component,
                                                                                        region,
                                                                                        fixed_alpha)])
        fig.savefig(path)

    if plot_strain_trace_fixed_lambda:
        
        fig = plt.figure()
        ax = fig.gca()
        strain = res["measured"]["strain"][component][region]
        x = np.linspace(0,100, len(strain))
        
        for a in alphas:
            s = res["simulated"]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]["strain"][component][region]
            n = len(s)
            ax.plot(x[:n], s, marker = "o", label = r"$\alpha = {}$".format(a))

        ax.set_xlabel(r"$\%$ cardiac cycle")
        ax.set_ylabel("{} strain, region {}".format(component, region))
        ax.legend(loc = "best")
        path = "/".join([outdir, "{}_strain_trace_region_{}_fixed_lambda_{}.eps".format(component,
                                                                                       region,
                                                                                       fixed_lambda)])
        fig.savefig(path)
        
    if plot_l_curve_fixed_lambda:
        # cmap = plt.cm.get_cmap('RdYlBu')
        fig = plt.figure()
        ax = fig.gca() 
        ax.set_yscale('log')
        ax.set_xscale('log')
        # mpl.rcParams['xtick.labelsize'] = 24
        # mpl.rcParams['ytick.labelsize'] = 24


        meas_data = res["measured"]
        I_vol = []
        I_strain = []
        for a in alphas:
            
            sim_data = res["simulated"]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]

            n = len(sim_data["volume"])
            
            if sim_data["volume"][0] == 0:
                sim_vol = sim_data["volume"][1:]
                meas_vol = meas_data["volume"][-(n-1):]
            else:
                sim_vol = sim_data["volume"]
                meas_vol = meas_data["volume"][-n:]

            I_vol.append(np.sum(np.abs(np.subtract(sim_vol,meas_vol))) /float(np.sum(meas_vol)))
            I_strain_rel, I_strain_max = get_Istrain(meas_data, sim_data, n)
            I_strain.append(I_strain_rel)

        ax.plot(I_vol, I_strain, "k-")
        s = ax.scatter(I_vol, I_strain, c = alphas, cmap = cmap, s = 180)

        if case == cases[0]:
            i = 10
        elif case == cases[1]:
            i = 10

        ax.annotate(alphas[i], (I_vol[i], I_strain[i]), size = 40)
            

        ax.set_ylabel(r"$\overline{I}_{\mathrm{strain}}$")
        ax.set_xlabel(r"$\overline{I}_{\mathrm{vol}}$")

        cbar = plt.colorbar(s)
        cbar.set_label(r"$\alpha$")
        fig.tight_layout()
        path = "/".join([outdir, "l_curve_fixed_lambda_{}_{}.eps".format(fixed_lambda, case)])
        fig.savefig(path)
        

    if plot_l_curve_fixed_alpha:

        fig = plt.figure()
        ax = fig.gca() 
        ax.set_yscale('log')
        ax.set_xscale('log')

        meas_data = res["measured"]
        I_data = []
        I_reg = []
        for l in lmbdas:
            
            sim_data = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(l)]

            n = len(sim_data["volume"])
            
            if sim_data["volume"][0] == 0:
                sim_vol = sim_data["volume"][1:]
                meas_vol = meas_data["volume"][-(n-1):]
            else:
                sim_vol = sim_data["volume"]
                meas_vol = meas_data["volume"][-n:]

         
            I_vol = np.sum(np.abs(np.subtract(sim_vol,meas_vol)))/float(np.sum(meas_vol))
            I_strain_rel, I_strain_max = get_Istrain(meas_data, sim_data, n)
            
            I_data.append(I_vol + I_strain_rel)
            I_reg.append(np.mean(sim_data["misfit"]["gamma_gradient"]))


        ax.plot(np.roll(I_reg, 2), np.roll(I_data, 2), "k-")
        s = ax.scatter(I_reg, I_data, c = lmbdas, cmap = cmap,
                       norm=mpl.colors.LogNorm(), s = 180)

        if case == cases[0]:
            i = 3
        elif case == cases[1]:
            i = 5

        ax.annotate(lmbdas[i], (I_reg[i], I_data[i]), size = 40)
        
        ax.set_ylabel(r"$\overline{I}_{\mathrm{data}}$")
        ax.set_xlabel(r"$\overline{I}_{\mathrm{smooth}}$")

        cbar = plt.colorbar(s)
        cbar.set_label(r"$\lambda$")
        fig.tight_layout()
        path = "/".join([outdir, "l_curve_fixed_alpha_{}_{}.eps".format(fixed_alpha, case)])
        fig.savefig(path)


    if plot_all_strains:
        # Plot all strains with fixed alpha and lambda
        measured_strains = res["measured"]["strain"]
        simulated_strains = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(fixed_lambda)]["strain"]
        plot_strains(simulated_strains, measured_strains, outdir)

    if plot_pv_loop:

        colors = [cmap(i) for i in [0.0, 0.75]]
        fig = plt.figure()
        ax = fig.gca()
        
        pressure = res["measured"]["pressure"]
        volume = res["measured"]["volume"]
        ax.plot(volume, pressure, color = colors[0], linestyle = "-", label = "Measured")
        vol = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(fixed_lambda)]["volume"]
        n = len(vol)
        ax.plot(vol, pressure[:n], color = colors[1], linestyle = "-", marker = "o", label = "Simulated")

        ax.set_xlabel("Volume (ml)")
        ax.set_ylabel("Pressure (kPa)")
        ax.legend(loc = "best")
        path = "/".join([outdir, "simulated_pv_loop.eps"])
        fig.savefig(path)

    if case in cases[1:]:

        if print_gamma_error:


            V_cg1 = FunctionSpace(patient.mesh, "CG", 1)
            f = XDMFFile(mpi_comm_world(), "gamma_err_noisefree.xdmf")
            gamma_diff = Function(V_cg1, name = "gamma error")
            gamma_sim = Function(V_cg1)
            gamma_synth = Function(V_cg1)
            dX = Measure("dx", subdomain_data = patient.strain_markers, domain = patient.mesh)
            synth_gamma = res["measured"]["gamma"]
            times = sorted(synth_gamma.keys(), key=asint)
            gamma_synth_l2 = []
            gamma_synth_regional = []
            for t in times:
                gamma_synth.vector()[:] = synth_gamma[t]
                gamma_synth_l2.append(norm(gamma_synth, "L2"))
                gamma_synth_regional.append(get_regional(dX, gamma_synth))

            max_g_l2 = np.max(gamma_synth_l2)
            max_g_reg = np.array(gamma_synth_regional).max(0)

            g_err_l2 = []
            gamma_err_max = []
            g_err_regional = []
            sim_gamma = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["gamma"]      
            for i, t in enumerate(times):

                
                gamma_synth.vector()[:] = synth_gamma[t]
                gamma_sim.vector()[:] = sim_gamma[t]
                gamma_err = project(gamma_synth-gamma_sim, V_cg1)
                gamma_diff.vector()[:] = gamma_err.vector()
                f.write(gamma_diff, float(t))
                gamma_err_max.append(gamma_err.vector().array().max())
                g_err_l2.append(np.divide(norm(gamma_err, "L2"),norm(gamma_synth, "L2")))###max_g_l2)
                g_sim_reg = get_regional(dX, gamma_sim)
                g_err_regional.append(np.mean(np.divide(np.abs(np.subtract(g_sim_reg,
                                                                           gamma_synth_regional[i])),
                                                        max_g_reg)))


            print "#"*10+"Synth Noisefree"+"#"*10
            print "max max = {}".format(np.max(gamma_err_max))
            print "max gamma error (l2) = {}".format(np.max(g_err_l2[3:-1]))
            print "mean gamma error (l2) = {}".format(np.mean(g_err_l2[3:-1]))
            print "max gamma error (regional) = {}".format(np.max(g_err_regional))
            print "mean gamma error (regional) = {}".format(np.mean(g_err_regional))

        if print_disp_error:

            synth_disp = res["measured"]["displacement"]
            sim_disp = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["displacement"]
            times = sorted(synth_disp.keys(), key=asint)
            
            u_diff = [np.abs(np.subtract(sim_disp[t], synth_disp[t])) for t in times]
            print "max u diff (mm) = {}".format(np.max(u_diff)*10.)
            print "mean u diff (mm) = {}".format(np.mean(u_diff)*10.)
        
        if plot_gamma_error_fixed_lambda:
            errkey = "err_linf"
            synth_gamma = res["measured"]["gamma"]
      
            gamma_error = {}
            # Maximum value on the dofs
            max_synth_gamma = np.max([np.max(s) for s in synth_gamma.values()])
            times = synth_gamma.keys()

            colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
            
            fig = plt.figure()
            ax = fig.gca()
            # Loop over alphas
            for i, a in enumerate(alphas):
                gamma_error[a] = {}
                
                # Loop over time steps
                for t in times:
                    gamma_error[a][t] = {}
                    
                    # Inf error
                    err_linf = np.linalg.norm(np.subtract(res["simulated"]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]["gamma"][t], synth_gamma[t]), np.inf)
                
                    gamma_error[a][t]["err_linf"]  = np.mean(err_linf)
               

        
                ax.plot(times, [gamma_error[a][t][errkey] for t in times],
                        "o", color = colors[i], label = r"$\alpha = {}$".format(a))
                     
            ax.legend(loc="best")
            ax.set_xlabel("Point")
            ax.set_ylabel(r"$\frac{ \| \gamma - \gamma^{ \mathrm{synth}} \|_{\infty} } { \| \gamma^{\mathrm{synth}} \|_{ \infty} }$")
            path = "/".join([outdir, "gamma_{}_fixed_lambda_{}.pdf".format(errkey, fixed_lambda)])
            fig.savefig(path)

        if plot_gamma_error_fixed_alpha:
            errkey = "err_linf"
            synth_gamma = res["measured"]["gamma"]
      
            gamma_error = {}
            # Maximum value on the dofs
            max_synth_gamma = np.max([np.max(s) for s in synth_gamma.values()])
            times = synth_gamma.keys()
            
            colors = [cmap(i) for i in np.linspace(0, 1, len(lmbdas))]
        
            fig = plt.figure()
            ax = fig.gca()
            # Loop over alphas
            for i, l in enumerate(lmbdas):
                gamma_error[l] = {}
            
                # Loop over time steps
                for t in times:
                    gamma_error[l][t] = {}
             
                    # Inf error
                    err_linf = np.linalg.norm(np.subtract(res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(l)]["gamma"][t], synth_gamma[t]), np.inf)
                    
                    gamma_error[l][t]["err_linf"]  = np.mean(err_linf)
        
                ax.plot(times, [gamma_error[l][t][errkey] for t in times],
                        "o", color = colors[i], label = r"$\lambda = {}$".format(l))
                     
            ax.legend(loc="best")
            ax.set_xlabel("Point")
            ax.set_ylabel(r"$\frac{ \| \gamma - \gamma^{ \mathrm{synth}} \|_{\infty} } { \| \gamma^{\mathrm{synth}} \|_{ \infty} }$")
            path = "/".join([outdir, "gamma_{}_fixed_alpha_{}.pdf".format(errkey, fixed_alpha)])
            fig.savefig(path)
        
        if plot_displacement_error_fixed_lambda:
            # Just do the max over the dofs
            synth_disp = res["measured"]["displacement"]

            times = synth_disp.keys()
            disp_error = {}
            
            colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
            
            fig = plt.figure()
            ax = fig.gca()
            # Loop over alphas
            for i, a in enumerate(alphas):
                disp_error[a] = {}
          
                # Loop over time steps
                for t in times:
                    disp_error[a][t] = {}
                    
                    err = np.linalg.norm(np.subtract(res["simulated"]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]["displacement"][t],synth_disp[t]), np.inf)
                    
                    disp_error[a][t]["err"] = np.mean(err)

                ax.plot(times, [disp_error[a][t]["err"] for t in times], "o", color = colors[i], label = r"$\alpha = {}$".format(a))
            ax.legend(loc="best")
            ax.set_xlabel("Point")
            ax.set_ylabel(r"$\| \mathbf{u} - \mathbf{u}^{\mathrm{synth}} \|_{ \infty}$")
            path = "/".join([outdir, "disp_err_fixed_lambda_{}.pdf".format(fixed_lambda)])
            fig.savefig(path)

        if plot_displacement_error_fixed_alpha:
            # Just do the max over the dofs

            synth_disp = res["measured"]["displacement"]
    
            times = synth_disp.keys()
            disp_error = {}
            
            colors = [cmap(i) for i in np.linspace(0, 1, len(lmbdas))]
            
            fig = plt.figure()
            ax = fig.gca()
            # Loop over alphas
            for i, l in enumerate(lmbdas):
                disp_error[l] = {}
                
                # Loop over time steps
                for t in times:
                    disp_error[l][t] = {}

                    err = np.linalg.norm(np.subtract(res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(l)]["displacement"][t],synth_disp[t]), np.inf)
        
                    disp_error[l][t]["err"] = np.mean(err)

                ax.plot(times, [disp_error[l][t]["err"] for t in times], "o", color = colors[i], label = r"$\lambda = {}$".format(l))
            ax.legend(loc="best")
            ax.set_xlabel("Point")
            ax.set_ylabel(r"$\| \mathbf{u} - \mathbf{u}^{\mathrm{synth}} \|_{ \infty}$")
                
            path = "/".join([outdir, "disp_err_fixed_alpha_{}.pdf".format(fixed_alpha)])
            fig.savefig(path)

        if plot_synthetic_snapshots:

            patient = load_patient_data(synth_data=True)
            mesh = Mesh(patient.mesh)
            V_cg1 = FunctionSpace(mesh, "CG", 1)
            VV_cg2 = VectorFunctionSpace(mesh, "CG", 2)
            VV_cg1 = VectorFunctionSpace(mesh, "CG", 1)
    
            gamma = Function(V_cg1)
       
            u = Function(VV_cg2)
            u_prev = Function(VV_cg2)
            u_diff = Function(VV_cg2)

            snap_outdir = "/".join([outdir, "snapshots"])
            if not os.path.exists(snap_outdir):
                os.makedirs(snap_outdir)
                
            path = "/".join([snap_outdir, "syntheic_gamma_{}"])
            times = sorted(res["measured"]["displacement"].keys(), key=asint)

            for t in times:
                u.vector()[:] = res["measured"]["displacement"][t]
                u_diff.vector()[:] = u.vector() - u_prev.vector()
                d = interpolate(u_diff, VV_cg1)
                ALE.move(mesh, d)
            
                
                gamma.vector()[:] = res["measured"]["gamma"][t]
                snap_shot(gamma, path.format(t), "gamma", "synthetic", rnge = (0,0.15), colorbar = False)
                u_prev.assign(u)

            keys = times[1:]
            labels = [r"Point {}".format(int(i)) for i in keys]
            # from IPython import embed; embed()
            make_canvas_snap_shot(keys,labels,path[:-3])
            make_video("/".join([snap_outdir, "syntheic_gamma_%d_side.png"]), times,
                       "/".join([snap_outdir, "syntheic_gamma_side.mp4"]))
            make_video("/".join([snap_outdir, "syntheic_gamma_%d_front.png"]), times,
                       "/".join([snap_outdir, "syntheic_gamma_front.mp4"]))

            
    if plot_snapshots:

        patient = load_patient_data()
        mesh = Mesh(patient.mesh)
        V_cg1 = FunctionSpace(mesh, "CG", 1)
        VV_cg2 = VectorFunctionSpace(mesh, "CG", 2)
        VV_cg1 = VectorFunctionSpace(mesh, "CG", 1)
    
        gamma = Function(V_cg1)
       
        u = Function(VV_cg2)
        u_prev = Function(VV_cg2)
        u_diff = Function(VV_cg2)
    
        snap_outdir = "/".join([outdir, "snapshots/alpha{}_lmbda{}".format(fixed_alpha, snap_lambda)])
        if not os.path.exists(snap_outdir):
            os.makedirs(snap_outdir)

        us = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["displacement"]
        gammas = res["simulated"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["gamma"]
        times = sorted(gammas.keys(), key=asint)

        rnge = (0,0.5) if case == cases[0] else (0,0.15)
        path = "/".join([snap_outdir, "gamma_{}"])
        for t in times:
            u.vector()[:] = us[t]
            u_diff.vector()[:] = u.vector() - u_prev.vector()
            d = interpolate(u_diff, VV_cg1)
            ALE.move(mesh, d)
            
            # Simulated
            gamma.vector()[:] = gammas[t]
            snap_shot(gamma, path.format(t), "gamma", "simulated", rnge = rnge, colorbar = False)
            u_prev.assign(u)
        


        # Put together a canvas
        if case == cases[0]:
            keys = times[::3]
            labels = [r"{:.0f} $\%$".format(100*float(i)/float(times[-1])) for i in keys]
        else:
            keys = times[1:]
            labels = [r"Point {}".format(int(i)) for i in keys]

        heatmappath = "/".join([snap_outdir, "heatmap"])
        snap_shot(gamma,  heatmappath, "gamma",
                  rnge = rnge, colorbar = True,
                  colorbar_align = "horizontal", colorbar_only=True)
        
        make_canvas_snap_shot(keys,labels,path[:-3], heatmap_name = heatmappath+"_front")
        make_video("/".join([snap_outdir, "gamma_%d_side.png"]), times,
                   "/".join([snap_outdir, "gamma_side.mp4"]))
        make_video("/".join([snap_outdir, "gamma_%d_front.png"]), times,
                   "/".join([snap_outdir, "gamma_front.mp4"]))
        
def make_video(imgpath, keys, moviepath):

    # Make sure that the images exists
    for k in keys:
        if not os.path.exists(imgpath % int(k)):
            if os.path.exists(os.path.splitext(imgpath % int(k))[0]):
                shutil.move(os.path.splitext(imgpath % int(k))[0],
                            imgpath % int(k))
            else:
                raise IOError("File {} does not exist".format(imgpath & int(k)))

    os.system("ffmpeg -y -loglevel panic -framerate 12 -i {0}  -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p {1}".format(imgpath, moviepath))

def plot_patient_pv_loop(outdir = ""):

    outdir = "figures/patient_data" if outdir=="" else outdir

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    colors = [cmap(i) for i in [0.0, 0.75]]
    patient = load_patient_data(synth_data = True)
    pfd = patient.passive_filling_duration
    
    fig = plt.figure()
    ax = fig.gca()

    ax.plot(patient.volume[:pfd], patient.pressure[:pfd], color = colors[0],
            linestyle = "--", marker = "o", label = "Atrial\nsystole")
    ax.plot(patient.volume[pfd-1:], patient.pressure[pfd-1:], color = colors[1],
            linestyle = "-", marker = "o", label = "Contraction/\nRelaxation")

    ax.set_ylabel("Pressure (kPa)")
    ax.set_xlabel("Volume (ml)")
    # ax.legend(loc="center")
    lgd = ax.legend(loc = "center left", bbox_to_anchor=(1, 0.5))
    fig.savefig("/".join([outdir, "patient_pv_loop.eps"]), bbox_extra_artists=(lgd,), bbox_inches='tight')
    
def passive_synth_test():


    cases = ["full", "constant_noise", "single", "percent"]
    case = "percent"
    
    outdir = "figures/passive_synth".format(case)
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    

    setup_general_parameters()
    patient = load_patient_data(synth_data = True)
    params = setup_application_parameters()
    
    params["noise"] = True
    params["reg_par"] = 0.0
    params["synth_data"] = True
    params["gamma_space"] = "CG_1"
    params["alpha"] = 0.95
    params["passive_synth"] = True


    if case == "constant_noise":
        fname = "results/passive_synth_const_noise.h5"
        realizations = [str(i) for i in range(30)]
      
    elif case == "single":
        fname = "results/passive_synth_single.h5"
        realizations = [str(i) for i in range(3)]
       
        re_to_label = [-4.3, 0, 4.3]
    elif case == "percent":
        fname = "results/passive_synth_percent.h5"
        realizations = [str(r) for r in range(5,56, 5)]
        
        re_to_label = [int(r)-30 for r in realizations]
        
    else:
        fname = "results/passive_synth.h5"
        realizations = [str(i) for i in range(60)]
        


        
    res = load_dict(fname)

        
    a = []
    vols_synth = []
    vols_sim = []
    I_vol = []

    for r in realizations:
        
        v = res["synthetic"]["volume_w_noise"][r]
        # Include only the results with monotonoically increasing volumes
    
        a.append(res["simulated"][r]["material_parameters_optimal"][0])
        vols_sim.append(res["simulated"][r]["volume"])
        I_vol.append(res["simulated"][r]["I_volume_optimal_passive"][0])
        vols_synth.append(v)



    vols_orig = vols_synth[5]
    dvols = np.array([v-vols_orig for v in vols_synth]).T[1:].T
    lst = np.array([re_to_label, dvols[:,0], dvols[:,1], I_vol, a]).T 
    caption = "Results from passive optimization"
    header = ["perturbation ($\%$)",
              "$\Delta V_1$ (ml)",
              "$\Delta V_2$ (ml)",
              "$\overline{I}_{\mathrm{vol}}$",
              "$a$"]
    table = lst
    label = "tab:passive_synth_opt"
    T = tabalize(caption, header, table, label, floatfmt=".3g")
    print T



    print vols_sim
    print vols_synth
    print a
    print "a = {} +/- {}".format(np.mean(a), np.std(a))
    print "median = {}".format(np.median(a))
    fig = plt.figure()
    ax = fig.gca()
    ax.set_title(r"Reproduction of $a$")
   
    if case in ["percent", "singel"]:
        x = re_to_label
    else:
        x = np.zeros(len(a))

    
    ax.scatter(x, a, color = "black", s = 175)
    ax.scatter([0.0], [2.28], color = "red", s = 180)
    
    y = [min(a), 2.28, max(a)]
    ax.set_yticks(y)
    ax.set_yticklabels(["{:.2f}".format(yi) for yi in y]) 
    


    xt = [min(x), 0, max(x)]
    ax.set_xticks(xt)
    ax.set_xticklabels([r"{:.0f}".format(xi) for xi in xt])
    if case == "percent":
        ax.set_xlabel(r"error ($\%$)")
    elif case == "single":
        ax.set_xlabel(r"error (ml)")
    else:
        ax.axes.get_xaxis().set_visible(False)
    ax.set_ylabel(r"$a$")
    
    fig.savefig("/".join([outdir, "passive_synth_test_a_{}.eps".format(case)]))
    fig.savefig("/".join([outdir, "passive_synth_test_a_{}.pdf".format(case)]))
    
    synth_mean = np.array(vols_synth).mean(0)
    synth_std = np.array(vols_synth).std(0)
    
    sim_mean = np.array(vols_sim).mean(0)
    sim_std = np.array(vols_sim).std(0)

    
    x = range(3)
    fig = plt.figure(figsize=(12.8, 8.8))
    ax = fig.gca()
    ax.plot(x, synth_mean, "k-", label = "Mean")
    ax.fill_between(x, synth_mean + synth_std, synth_mean - synth_std, facecolor = 'gray', alpha = 0.5)
    ax.set_title("Ground Truth")
    ax.plot(x, res["synthetic"]["volume"], "k--", label = "Original")
    ax.set_ylabel("Volume (ml)")
    ax.set_xlabel("Point")
    ax.set_xticks(x)
    ax.set_xticklabels(x)   
    ax.legend(loc="upper left")
    y = [min(synth_mean), np.mean(synth_mean), np.max(synth_mean)]
    ax.set_yticks(y)
    ax.set_yticklabels(["{:.0f}".format(yi) for yi in y]) 

    
    fig.savefig("/".join([outdir, "passive_synthetic_data_test_ground_{}.eps".format(case)]))

    fig = plt.figure(figsize=(10.8, 8.8))
    ax = fig.gca()
    ax.plot(x, sim_mean, "k-", label = "Mean")
    ax.fill_between(x, sim_mean + sim_std, sim_mean - sim_std, facecolor = 'gray', alpha = 0.5)
    ax.set_title("Reproduction")
    ax.plot(x, res["synthetic"]["volume"], "k--", label = "Original")

    ax.set_xlabel("Point")
    ax.set_xticks(x)
    ax.set_xticklabels(x) 
    ax.legend(loc="upper left")
    y = [min(synth_mean), np.mean(synth_mean), np.max(synth_mean)]
    ax.set_yticks(y)
    ax.set_yticklabels([])

 
    fig.savefig("/".join([outdir, "passive_synthetic_data_test_repr_{}.eps".format(case)]))


    


def plot_synthetic_results_CG1_fullstrain():
    
    plot_l_curve = False
    plot_gamma_error = False
    plot_displacement_error = False
    plot_strains = False
    plot_snapshots = False
    print_gamma_error = True
    print_disp_error = False

    outdir = "figures/CG1/synthetic_fullstrain"
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    fname = "results/synthetic_fullstrain_CG1.h5"

    setup_general_parameters()
    patient = load_patient_data(synth_data = True)
    params = setup_application_parameters()
    
    params["noise"] = False
    
    params["reg_par"] = 0.0
    params["synth_data"] = True
    params["gamma_space"] = "CG_1"
    
    alphas = [str(i/10.) for i in range(11)] + ["0.95", "0.99", "0.999", "0.9999"]

    V_cg1 = FunctionSpace(patient.mesh, "CG", 1)
    VV_cg2 = VectorFunctionSpace(patient.mesh, "CG", 2)
    
    gamma_sim = Function(V_cg1, name = "simulated")
    gamma_synth = Function(V_cg1, name = "synthetic")
    gamma_err = Function(V_cg1, name = "error")

    u_sim = Function(VV_cg2)
    u_synth = Function(VV_cg2)
    u_err = Function(VV_cg2)

    all_results = load_dict(fname)

    alphas.sort()

    if print_gamma_error:

        f = XDMFFile(mpi_comm_world(), "gamma_err_full_strain.xdmf")
        gamma_diff = Function(V_cg1, name = "gamma error")

        V_cg1 = FunctionSpace(patient.mesh, "CG", 1)
        gamma_sim = Function(V_cg1)
        gamma_synth = Function(V_cg1)
        dX = Measure("dx", subdomain_data = patient.strain_markers, domain = patient.mesh)
        synth_gamma = all_results["synthetic"]["gamma"]
        times = sorted(synth_gamma.keys(), key=asint)
        gamma_synth_l2 = []
        gamma_synth_regional = []
        for t in times:
            gamma_synth.vector()[:] = synth_gamma[t]
            gamma_synth_l2.append(norm(gamma_synth, "L2"))
            gamma_synth_regional.append(get_regional(dX, gamma_synth))

        max_g_l2 = np.max(gamma_synth_l2)
        max_g_reg = np.array(gamma_synth_regional).max(0)

        g_err_l2 = []
        g_err_regional = []
        sim_gamma = all_results["simulated"]["gamma"]
        gamma_err_max = []
        for i, t in enumerate(times):

            gamma_synth.vector()[:] = synth_gamma[t]
            gamma_sim.vector()[:] = sim_gamma[t]
            
            gamma_err = project(gamma_synth-gamma_sim, V_cg1)
            gamma_diff.vector()[:] = gamma_err.vector()
            f.write(gamma_diff, float(t))
            gamma_err_max.append(gamma_err.vector().array().max())
            g_err_l2.append(np.divide(norm(gamma_err, "L2"),norm(gamma_synth, "L2")))#max_g_l2)
            g_sim_reg = get_regional(dX, gamma_sim)
            g_err_regional.append(np.mean(np.divide(np.abs(np.subtract(g_sim_reg,
                                                                       gamma_synth_regional[i])),
                                                    max_g_reg)))

        print "#"*10+"Full strain"+"#"*10
        print "max max = {}".format(np.max(gamma_err_max))
        print "max gamma error (l2) = {}".format(np.max(g_err_l2[3:-1]))
        print "mean gamma error (l2) = {}".format(np.mean(g_err_l2[3:-1]))
        print "max gamma error (regional) = {}".format(np.max(g_err_regional))
        print "mean gamma error (regional) = {}".format(np.mean(g_err_regional))
        
    if print_disp_error:

        synth_disp = all_results["synthetic"]["displacement"]
        sim_disp = all_results["simulated"]["displacement"]
        times = sorted(synth_disp.keys(), key=asint)
            
        u_diff = [np.abs(np.subtract(sim_disp[t], synth_disp[t])) for t in times]
        print "max u diff (mm) = {}".format(np.max(u_diff)*10.)
        print "mean u diff (mm) = {}".format(np.mean(u_diff)*10.)
        

    if plot_gamma_error:
        errkey = "err_linf"
        synth_gamma = all_results["synthetic"]["gamma"]
      
        gamma_error = {}
        # Maximum value on the dofs
        max_synth_gamma = np.max([np.max(s) for s in synth_gamma.values()])
        times = synth_gamma.keys()
       
        colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
        
        fig = plt.figure()
        ax = fig.gca()
        # Loop over alphas
        gamma_error = {}
        # Loop over time steps
        for t in times:
            gamma_error[t] = {}
            
            # Inf error
            err_linf = np.linalg.norm(np.subtract(all_results["simulated"]["gamma"][t], synth_gamma[t]), np.inf)
                
            gamma_error[t]["err_linf"]  = np.mean(err_linf)
               

        
        ax.plot(times, [gamma_error[t][errkey] for t in times],
                "o", color = colors[0])
                     
        ax.legend(loc="best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\frac{ \| \gamma - \gamma^{ \mathrm{synth}} \|_{\infty} } { \| \gamma^{\mathrm{synth}} \|_{ \infty} }$")
        # plt.show()
        path = "/".join([outdir, "gamma_{}_new.pdf".format(errkey)])
        fig.savefig(path)

    if plot_displacement_error:
        # Just do the max over the dofs

        synth_disp = all_results["synthetic"]["displacement"]
    
        times = synth_disp.keys()
        disp_error = {}
       
        colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
        
        fig = plt.figure()
        ax = fig.gca()
        # Loop over alphas
        disp_error= {}
          
        # Loop over time steps
        for t in times:
            disp_error[t] = {}
            
            err = np.linalg.norm(np.subtract(all_results["simulated"]["displacement"][t],
                                             synth_disp[t]), np.inf)
            
            disp_error[t]["err"] = np.mean(err)

        ax.plot(times, [disp_error[t]["err"] for t in times], "o", color = colors[0])
        ax.legend(loc="best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\| \mathbf{u} - \mathbf{u}^{\mathrm{synth}} \|_{ \infty}$")
        
        path = "/".join([outdir, "disp_err_new.pdf"])
        fig.savefig(path)


    if plot_strains:
        region = "1"
        # Simulated
        s = all_results["synthetic"]["strain"]["longitudinal"][region]
        x = range(len(s))            
        
        for a in alphas:
            fig = plt.figure()
            ax = fig.gca()
            ax.plot(all_results["simulated"]["alpha_{}".format(a)]["strain"]["longitudinal"][region], label = "alpha = {}".format(a))

            ax.plot(x,s, "k-", linewidth = 2.0, label = "Original")
            ax.legend(loc="best")
            path = "/".join([outdir, "strain_simulated_region_{}_alpha_{}.pdf".format(region, a)])
            fig.savefig(path)

    if plot_snapshots:

        snap_outdir = "/".join([outdir, "snapshots"])
        if not os.path.exists(snap_outdir):
            os.makedirs(snap_outdir)

        synth_gamma = all_results["synthetic"]["gamma"]
        times = synth_gamma.keys()

        path_sim = "/".join([snap_outdir, "gamma_simulated_{}"])
        path_synth = "/".join([snap_outdir, "gamma_synthetic_{}"])
                
        for t in times:
            # Simulated
            gamma_sim.vector()[:] = \
                all_results["simulated"]["gamma"][t]
            
            snap_shot(gamma_sim, path_sim.format(t), "gamma", "simulated")

            gamma_synth.vector()[:] = synth_gamma[t]
            
            snap_shot(gamma_synth, path_synth.format(t), "gamma", "synthetic")

        
        # Put together a canvas
        make_canvas_snap_shot(sorted(times, key=asint)[1:],
                              ["Point {}".format(t) for t \
                               in sorted(times, key=asint)][1:],
                              path_sim[:-3])
        make_canvas_snap_shot(sorted(times, key=asint)[1:],
                              ["Point {}".format(t) for t \
                               in sorted(times, key=asint)][1:],
                              path_synth[:-3])

        
def material_parameter_sensitivity_fixed(outdir = ""):

    plot_pv_loop = False
    plot_mean_gamma = True
    
    outdir = "figures/sensitivity_analysis/material_parameters_fixed" \
             if outdir == "" else outdir
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    fname = "results/matparams_sensitivity_fixed.h5"
    
    setup_general_parameters()
    patient = load_patient_data()
    params = setup_application_parameters()
    params["alpha"] = 0.95
    

    matparams = ["0.384", "0.435", "0.494"]
 
    results = load_dict(fname)


 
    if plot_pv_loop:
        # cmap = plt.get_cmap('jet')
        colors = [cmap(i) for i in np.linspace(0, 1, len(matparams))]
        n = patient.passive_filling_duration+1
        
        fig = plt.figure()
        ax = fig.gca()
        pressure = patient.pressure
        volume = patient.volume
        ax.plot(volume, pressure, "k-*", label = "Measured")
        for i, a in enumerate(matparams):
            v = results[a]["volume"]
            p = pressure[:len(v)]
            ax.plot(v,p, marker = "o", label = r"$a={}$".format(a), color = colors[i])
        ax.legend(loc = "best")
        ax.set_xlabel("Volume(ml)")
        ax.set_ylabel(r"$Pressure(kPa)$")
        path = "/".join([outdir, "pv_loops.pdf"])
        fig.savefig(path)
   
    
            
    # Mean gamma
    if plot_mean_gamma:
        # colors = [cmap(i) for i in np.linspace(0, 1, len(matparams))]
        colors = [cmap(i) for i in [0.0, 0.4, 0.8]]
        dX = Measure("dx",subdomain_data = patient.strain_markers,
                     domain = patient.mesh)
        gamma_space = FunctionSpace(patient.mesh, "CG", 1)
        gamma = Function(gamma_space)
        fig = plt.figure()
        ax = fig.gca()
        for i, a in enumerate(matparams):
            gamma_lst = [results[a]["gammas"][k] for k in sorted(results[a]["gammas"], key=asint)]
            gamma_mean = get_global(dX, gamma, gamma_lst)
            ax.plot(gamma_mean, label = r"$a={}$".format(a), color = colors[i], linestyle = next(linestyles))
        ax.legend(loc = "best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\overline{\gamma}$")
        path = "/".join([outdir, "mean_gamma_material_parameters_fixed.eps"])
        fig.savefig(path)


def material_parameter_sensitivity():

    plot_matparam_convergence=False
    plot_pv_loop = True
    plot_mean_gamma = True
    
    outdir = "figures/sensitivity_analysis/material_parameters"
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    fname = "results/matparams_sensitivity.h5"
    
    setup_general_parameters()
    patient = load_patient_data()
    params = setup_application_parameters()
    params["alpha"] = 0.5
    
    matparams = ["0.561", "4.835", "12.21", "19.643",
                 "24.985", "31.635", "37.386", "44.894"]
    
    results = load_dict(fname)


    caption = "Multistart material parameters.  Showing intial and optimal " +\
              " material parameters as well as the value of the objective functional" +\
              "before and after optimzation."
    header = ["$a$", "I", 
              "$a$",  "I"]
    table = [[results[a]["material_parameters_initial"][0]] \
             + [results[a]["I_volume_initial_passive"]] \
             + [results[a]["material_parameters_optimal"][0]]\
             + [results[a]["I_volume_optimal_passive"]] for a in matparams]
    label = "tab:multistart_matparams"
    
    T = tabalize(caption, header, table, label, floatfmt="g")

    print T

    if plot_matparam_convergence:


        # Plot convergence of the value
       
        colors = [cmap(i) for i in np.linspace(0, 1, len(matparams))]
        fig = plt.figure()
        ax = fig.gca()
        for i, a in enumerate(matparams):

            d = results[a]["controls"]["0"]
            nit =int(results[a]["passive_nit"][0]) +1
            vals = [d[k][0] for k in sorted(d, key=asint)][-nit:]
            x = range(len(vals))
            ax.semilogy(x, vals , marker="o",
                        label = r"$a={}$".format(a), color = colors[i])

        ax.legend(loc="best")
        ax.set_xlabel("Number of iterations")
        ax.set_ylabel(r"$a$")
        path = "/".join([outdir, "matparam_convergence_value.pdf"])
        fig.savefig(path)

        # Plot convergence of the error
       
        colors = [cmap(i) for i in np.linspace(0, 1, len(matparams))]
        fig = plt.figure()
        ax = fig.gca()
        for i, a in enumerate(matparams):
            nit = int(results[a]["passive_nit"][0]) +1
            vals = results[a]["functional_values"]["0"][-nit:]
            x = range(len(vals))
            ax.semilogy(x, vals , marker="o",
                        label = r"$a={}$".format(a), color = colors[i])

        ax.legend(loc="best")
        ax.set_xlabel("Number of iterations")
        ax.set_ylabel(r"$I_{\mathrm{volume}}$")
        path = "/".join([outdir, "matparam_convergence_err.pdf"])
        fig.savefig(path)


        
    if plot_pv_loop:
   
        colors = [cmap(i) for i in np.linspace(0, 1, len(matparams))]
        n = patient.passive_filling_duration+1
        
        fig = plt.figure()
        ax = fig.gca()
        pressure = patient.pressure
        volume = patient.volume
        ax.plot(volume, pressure, "k-*", label = "Measured")
        for i, a in enumerate(matparams):
            v = results[a]["volume"]
            p = pressure[:len(v)]
            ax.plot(v,p, marker = "o", label = r"$a={}$".format(a), color = colors[i])
        ax.legend(loc = "best")
        ax.set_xlabel("Volume(ml)")
        ax.set_ylabel(r"$Pressure(kPa)$")
        path = "/".join([outdir, "pv_loops.pdf"])
        fig.savefig(path)
   
    
            
    # Mean gamma
    if plot_mean_gamma:
        dX = Measure("dx",subdomain_data = patient.strain_markers,
                     domain = patient.mesh)
        gamma_space = FunctionSpace(patient.mesh, "CG", 1)
        gamma = Function(gamma_space)
        fig = plt.figure()
        ax = fig.gca()
        for i, a in enumerate(matparams):
            gamma_lst = [results[a]["gammas"][k] for k in sorted(results[a]["gammas"], key=asint)]
            gamma_mean = get_global(dX, gamma, gamma_lst)
            ax.plot(gamma_mean, label = r"$a={}$".format(a), color = colors[i])
        ax.legend(loc = "best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\overline{\gamma}$")
        path = "/".join([outdir, "mean_gamma.pdf"])
        fig.savefig(path)

        
def compare_gamma_resolutions():

    plot_misfit = False
    print_opt_details = False
    print_misfit = True
    make_vtk_files = False
    plot_mean_gamma = True
    
    gamma_spaces = ["scalar", "regional", "CG_1"]
    g_spaces_dict = {"scalar":"R_0", "regional":"regional",
                     "CG_1":"CG_1"}

    alpha = 0.95
    reg_par = 0.01

    
    
    outdir = "figures/gamma_spaces"
    fname = "results/patient_simulation_gamma_spaces.h5"
    
    opt_keys = ["nfev", "njev", "nit", "run_time", "ncrash"]
    patient = load_patient_data()

    if not os.path.exists(outdir):
        os.makedirs(outdir)


    res = load_dict(fname)


    if plot_mean_gamma:

        colors = [cmap(i) for i in [0.0, 0.4, 0.8]]
        fig = plt.figure()
        ax = fig.gca()
        dX = Measure("dx",subdomain_data = patient.strain_markers,
                     domain = patient.mesh)
        
        for i, g in enumerate(gamma_spaces):

            if g == "regional":
                gamma_space = VectorFunctionSpace(patient.mesh, "R", 0, dim=17)
            else:
                gamma_family, gamma_degree  = g_spaces_dict[g].split("_")
                gamma_space = FunctionSpace(patient.mesh,
                                            gamma_family,
                                            int(gamma_degree))

            gamma = Function(gamma_space, name = "gamma")

            gs = res["simulated"][g]["gamma"]
            times = sorted(gs.keys(), key=asint)
            gamma_lst = [gs[k] for k in times]
            if g == "scalar":
                gamma_mean = gamma_lst
            else:
                gamma_mean = get_global(dX, gamma, gamma_lst)

            gprint = "P1" if g == "CG_1" else g
            ax.plot(gamma_mean, label = "{}".format(gprint), color = colors[i], linestyle = next(linestyles))
            
        ax.legend(loc = "best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\overline{\gamma}$")
        plt.plot()
        path = "/".join([outdir, "mean_gamma_resolutions.pdf"])
        fig.savefig(path)

    if make_vtk_files:

        for g in gamma_spaces:
            path = "/".join([outdir, g+".xdmf"]) 
            f = XDMFFile(mpi_comm_world(), path)
            mesh = Mesh(patient.mesh)

            if g == "regional":
                gamma_space = FunctionSpace(mesh, "DG", 0)
                rg = RegionalGamma(patient.strain_markers)
            else:
                gamma_family, gamma_degree  = g_spaces_dict[g].split("_")
                gamma_space = FunctionSpace(mesh,
                                            gamma_family,
                                            int(gamma_degree))

            gamma = Function(gamma_space, name = "gamma")
            
            disp_space = VectorFunctionSpace(mesh, "CG", 2)
            u = Function(disp_space)
            u_prev = Function(disp_space)
            u_diff = Function(disp_space)
            V = VectorFunctionSpace(mesh, "CG", 1)

            us = res["simulated"][g]["displacement"]
            gammas = res["simulated"][g]["gamma"]
            times = sorted(us.keys(), key=asint)
            for t in times:

                u.vector()[:] = us[t]
                u_diff.vector()[:] = u.vector() - u_prev.vector()
                d = interpolate(u_diff, V)
                ALE.move(mesh, d)
                
        
                if g == "regional":
                    rg.vector()[:] = gammas[t]
                    gamma_tmp = project(rg.get_function(), gamma_space)
                    gamma.vector()[:] = gamma_tmp.vector()
                else:
                    gamma.vector()[:] = gammas[t]
                    
                f.write(gamma, float(t))
                u_prev.assign(u)

                
    if print_opt_details:
        caption = "Optimization details (average over all time steps during active phase)"
        header = ["$\gamma$ space",
                  "$\#$ forward solves",
                  "$\#$ adjoint solves",
                  "run time",
                  "number of crashes"]
 
        table = [ [g,
                   np.mean(res["simulated"][g]["nfev"]),
                   np.mean(res["simulated"][g]["njev"]),
                   np.mean(res["simulated"][g]["run_time"]),
                   np.mean(res["simulated"][g]["ncrash"])] for g in gamma_spaces]
        label = "tab:patient_opt_details"
        
        T = tabalize(caption, header, table, label, floatfmt=".2g")
        print T

    if print_misfit:
        
        meas_data = res["measured"]
        lst = []
        for g in gamma_spaces:

            sim_vol = res["simulated"][g]["volume"]
            n = len(sim_vol)
            meas_vol = meas_data["volume"][-n:]
            I_vol = np.sum(np.abs(np.subtract(sim_vol,meas_vol)))/np.sum(meas_vol)

            I_strain_rel, I_strain_max = get_Istrain(meas_data, res["simulated"][g], n)
            g1 = "P1" if g == "CG_1" else g
            lst.append([g1, I_vol, I_strain_rel, I_strain_max])

        caption = "Relative misfit for different representation of $\gamma$"
        header = ["$\gamma$ space",
                  "$\overline{I}_{\mathrm{vol}}$",
                  "$\overline{I}_{\mathrm{strain}}^{\mathrm{rel}}$",
                  "$\overline{I}_{\mathrm{strain}}^{\mathrm{max}}$"]
        table = lst
        label = "tab:gamma_space_opt_misfit"
        T = tabalize(caption, header, table, label, floatfmt=".2g")
        print T
        
    if plot_misfit:
        fig, ax = plt.subplots(1,2, figsize=(20.0, 6.0))
        x = np.linspace(0,100, patient.num_contract_points)
        labels = {"scalar":"Scalar", "regional":"Regional","CG_1":"P1"}
        ys = []
        for g in gamma_spaces:

            y = res["simulated"][g]["misfit"]["I_strain_optimal_active"]
            ax[0].semilogy(x,y, label = labels[g])
            ys.append(y)
            
        ax[0].set_ylabel("$I_{\mathrm{strain}}$")
        ax[0].set_xlabel("$\%$ Cardiac Cycle")
        ax[0].set_xticks([0,50, 100])
        ax[0].set_xticklabels([0,50, 100])

        yt = [np.min(ys), np.min(ys), np.max(ys)]
        ax[0].set_yticks([1e-4, 1e-2, 1.])
       
        lines=[]
        for g in gamma_spaces:

            l = ax[1].semilogy(x,res["simulated"][g]["misfit"]["I_volume_optimal_active"], label = labels[g])
            lines.append(l[0])

        ax[1].set_ylabel("$I_{\mathrm{vol}}$")
        ax[1].set_xlabel("$\%$ Cardiac Cycle")
        ax[1].set_xticks([0,50, 100])
        ax[1].set_xticklabels([0,50, 100])
        ax[1].set_yticks([1e-9, 1e-6, 1e-3])
        fig.legend(lines, labels.values(), loc = "upper center", bbox_to_anchor=(0.5, 1.08),#bbox_to_anchor=(0.5, 1.035),
                   bbox_transform = plt.gcf().transFigure, ncol = 3)
        fig.tight_layout()
        
        path = "/".join([outdir, "misfit.eps"])
        fig.savefig(path)
        plt.show()
    
        
def plot_synthetic_results_CG1_w_noise(outdir = "",
                                       plot_mean_gamma = False,
                                       plot_strains = False,
                                       plot_l_curve_fixed_alpha =False,
                                       plot_l_curve_fixed_lambda = False):
    
    plot_gamma_error = False
    plot_displacement_error =False
    
    plot_snapshots = False
    plot_synthetic_snapshots = False
    print_disp_error= False
    print_gamma_error = True

    outdir = "figures/CG1/synthetic_w_noise" if outdir == "" else outdir
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    fname = "results/synthetic_w_noise_CG1.h5"

    setup_general_parameters()
    patient = load_patient_data(synth_data = True)
    params = setup_application_parameters()

    

    fixed_alpha = "0.95"
    fixed_lambda = "0.0"
    snap_lambda = "1.0"
    
    params["noise"] = True
    
    params["reg_par"] = 0.0
    params["synth_data"] = True
    params["gamma_space"] = "CG_1"
    
    realizations = [str(i) for i in range(5) + range(6,10)]
   
    alphas = [str(i/10.) for i in range(11)] + ["0.95", "0.99", "0.999", "0.9999"]
    lmbdas = ["0.0"] + [str(10**i/1000000.) for i in range(9)]
 

    V_cg1 = FunctionSpace(patient.mesh, "CG", 1)
    VV_cg2 = VectorFunctionSpace(patient.mesh, "CG", 2)
    
    gamma_sim = Function(V_cg1, name = "simulated")
    gamma_synth = Function(V_cg1, name = "synthetic")
    gamma_err = Function(V_cg1, name = "error")

    u_sim = Function(VV_cg2)
    u_synth = Function(VV_cg2)
    u_err = Function(VV_cg2)
    
    all_results = load_dict(fname)

    alphas.sort()

    if plot_mean_gamma:

        region = 1
        fig = plt.figure(figsize=(10.8, 8.8))
        ax = fig.gca()
        # Get average gamma in region 1
        gsim = np.array([all_results["simulated"][n]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["gamma"] \
                          for n in realizations])
        dX = Measure("dx", domain = patient.mesh, subdomain_data=patient.strain_markers)
        regional_gammas = []
        for gs in gsim:
            greg = []
            times = sorted(gs.keys(), key=asint)
            for t in times:
                g = gs[t]
                gamma_sim.vector()[:] = g
                greg.append(get_regional(dX, gamma_sim)[region-1])

            regional_gammas.append(greg)

        x = np.array(times, dtype=float)
        gmean = np.mean(regional_gammas, 0)
        ax.plot(x, gmean, "k-", label = "Mean")
        gstd = np.std(regional_gammas, 0)
        ax.fill_between(x, gmean + gstd, gmean - gstd, facecolor = 'gray', alpha = 0.5)

        ax.set_xlabel("Point")
        ax.set_ylabel(r"Average $\gamma$")

        majorLocator = MultipleLocator(0.03)
        majorFormatter = FormatStrFormatter('%.2f')
        minorLocator = MultipleLocator(0.01)
              
        ax.yaxis.set_major_locator(majorLocator)
        ax.yaxis.set_major_formatter(majorFormatter)

        # for the minor ticks, use no labels; default NullFormatter
        ax.yaxis.set_minor_locator(minorLocator)
        
        yt =  [0.00, 0.03, 0.06, 0.09]
        ax.set_yticks(yt)
        ax.set_yticklabels(["${}$".format(yti) for yti in yt])
        fig.tight_layout()
        path = "/".join([outdir, "mean_gamma_region1_synth_w_noise.eps"])
        fig.savefig(path)
        # from IPython import embed; embed()

        

    if plot_l_curve_fixed_lambda:
        
        fig = plt.figure(figsize=(10.8, 8.8))
        ax = fig.gca() 
        ax.set_yscale('log')
        ax.set_xscale('log')
        
        
        meas_data = all_results["synthetic"]
        I_vol = []
        I_strain = []
        I_vol_std = []
        I_strain_std = []
        for a in alphas:

            I_vol_realiz = []
            I_strain_realiz = []
            for realization in realizations:
                sim_data = all_results["simulated"][realization]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]
            
                n = len(sim_data["volume"])
            
                if sim_data["volume"][0] == 0:
                    sim_vol = sim_data["volume"][1:]
                    meas_vol = meas_data["volume_w_noise"][-(n-1):]
                else:
                    sim_vol = sim_data["volume"]
                    meas_vol = meas_data["volume_w_noise"][-n:]

             
                I_vol_realiz.append(np.sum(np.abs(np.subtract(sim_vol,meas_vol))[SYNTH_PASSIVE_FILLING:]) /float(np.sum(meas_vol[SYNTH_PASSIVE_FILLING:])))      
                I_strain_rel, I_strain_max = get_Istrain_noisy(meas_data, sim_data, n, realization)
                    
                I_strain_realiz.append(I_strain_rel)
                
            I_strain.append(np.mean(I_strain_realiz))
            I_strain_std.append(np.std(I_strain_realiz))
            I_vol.append(np.mean(I_vol_realiz))
            I_vol_std.append(np.std(I_vol_realiz))
            
       
        ax.plot(I_vol, I_strain, "k-")
        s = ax.scatter(I_vol, I_strain, c = alphas, cmap = cmap, s = 180)
        

        i = 10
        ax.annotate(alphas[i], (I_vol[i], I_strain[i]), size = 40)
            

        ax.set_ylabel(r"$\overline{I}_{\mathrm{strain}}$")
        ax.set_xlabel(r"$\overline{I}_{\mathrm{vol}}$")

        fig.tight_layout()
        path = "/".join([outdir, "l_curve_fixed_lambda_{}_synth_w_noise.eps".format(fixed_lambda)])
        fig.savefig(path)
        

    if plot_l_curve_fixed_alpha:

       
        fig = plt.figure(figsize=(10.8, 8.8))
        ax = fig.gca() 
        ax.set_yscale('log')
        ax.set_xscale('log')

        meas_data = all_results["synthetic"]
        I_data = []
        I_reg = []
        I_data_std = []
        I_reg_std = []
        for l in lmbdas:

            I_data_realiz = []
            I_reg_realiz = []
            for realization in realizations:
                sim_data = all_results["simulated"][realization]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(l)]
            
                n = len(sim_data["volume"])
            
                if sim_data["volume"][0] == 0:
                    sim_vol = sim_data["volume"][1:]
                    meas_vol = meas_data["volume_w_noise"][-(n-1):]
                else:
                    sim_vol = sim_data["volume"]
                    meas_vol = meas_data["volume_w_noise"][-n:]

                I_vol_realiz = np.sum(np.abs(np.subtract(sim_vol,meas_vol))[SYNTH_PASSIVE_FILLING:]) /float(np.sum(meas_vol[SYNTH_PASSIVE_FILLING:]))
                I_strain_rel, I_strain_max = get_Istrain_noisy(meas_data, sim_data, n, realization)
                    
              
                I_data_realiz.append(I_vol + I_strain_rel)
                I_reg_realiz.append(np.mean(sim_data["gamma_gradient"]))
                
            I_data.append(np.mean(I_data_realiz))
            I_data_std.append(np.std(I_data_realiz))
            I_reg.append(np.mean(I_reg_realiz))
            I_reg_std.append(np.std(I_reg_realiz))

        ax.plot(I_reg, I_data, "k-")
        s = ax.scatter(I_reg, I_data, c = lmbdas,
                       cmap = cmap, norm=mpl.colors.LogNorm(),
                       s = 180 )
        

        i = 7

        ax.annotate(lmbdas[i], (I_reg[i], I_data[i]), size = 40)
        ax.set_ylabel(r"$\overline{I}_{\mathrm{data}}$")
        ax.set_xlabel(r"$\overline{I}_{\mathrm{smooth}}$")

        fig.tight_layout()
        path = "/".join([outdir, "l_curve_fixed_alpha_{}_synth_w_noise.eps".format(fixed_alpha)])
        fig.savefig(path)

    if print_gamma_error:

        f = XDMFFile(mpi_comm_world(), "gamma_err_noise.xdmf")
        gamma_diff = Function(V_cg1, name = "gamma error")
        
        dX = Measure("dx", subdomain_data = patient.strain_markers, domain = patient.mesh)
        synth_gamma = all_results["synthetic"]["gamma"]
        times = sorted(synth_gamma.keys(), key=asint)
        gamma_synth_l2 = []
        gamma_synth_regional = []
        for t in times:
            gamma_synth.vector()[:] = synth_gamma[t]
            gamma_synth_l2.append(norm(gamma_synth, "L2"))
            gamma_synth_regional.append(get_regional(dX, gamma_synth))

        max_g_l2 = np.max(gamma_synth_l2)
        max_g_reg = np.array(gamma_synth_regional).max(0)

        gamma_err_l2 = []
        gamma_err_l2_mean = []
        gamma_err_regional = []
        gamma_err_regional_mean = []
        gamma_err_max = []
              
        for r in realizations:
            sim_gamma = all_results["simulated"][str(r)]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["gamma"]

            g_err_l2 = []
            g_err_regional = []
            g_err_max = []
      
            for i, t in enumerate(times):

                gamma_synth.vector()[:] = synth_gamma[t]
                gamma_sim.vector()[:] = sim_gamma[t]
                gamma_err = project(gamma_synth-gamma_sim, V_cg1)
                g_err_max.append(gamma_err.vector().array().max())
                g_err_l2.append(np.divide(norm(gamma_err, "L2"),norm(gamma_synth, "L2")))#max_g_l2)
                g_sim_reg = get_regional(dX, gamma_sim)
                g_err_regional.append(np.mean(np.divide(np.abs(np.subtract(g_sim_reg,
                                                                       gamma_synth_regional[i])),
                                                    max_g_reg)))
                gamma_diff.vector()[:] = gamma_err.vector()
                # f.write(gamma_diff, float(t))
            # exit()

            gamma_err_max.append(np.max(g_err_max))
            gamma_err_l2.append(np.max(g_err_l2[3:-1]))
            gamma_err_regional.append(np.max(g_err_regional))
            gamma_err_l2_mean.append(np.mean(g_err_l2[3:-1]))
            gamma_err_regional_mean.append(np.mean(g_err_regional))

            
        print "#"*10+"Noisy"+"#"*10
        print "max max = {}".format(np.max(gamma_err_max))
        print "max gamma error (l2) = {} +/- {}".format(np.mean(gamma_err_l2),
                                                        np.std(gamma_err_l2))
        print "mean gamma error (l2) = {} +/- {}".format(np.mean(gamma_err_l2_mean),
                                                         np.std(gamma_err_l2_mean))
        print "max gamma error (regional) = {} +/- {}".format(np.mean(gamma_err_regional),
                                                              np.std(gamma_err_regional))
        print "mean gamma error (regional) = {} +/- {}".format(np.mean(gamma_err_regional_mean),
                                                               np.std(gamma_err_regional_mean))


    if print_disp_error:

        synth_disp = all_results["synthetic"]["displacement"]
        times = sorted(synth_disp.keys(), key=asint)

        u_err_max = []
        u_err_mean = []  
        for r in realizations:
            sim_disp = all_results["simulated"][str(r)]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["displacement"]

            u_diff = [np.abs(np.subtract(sim_disp[t], synth_disp[t])) for t in times]
            u_err_max.append(np.max(u_diff))
            u_err_mean.append(np.mean(u_diff))

        
        print "max u diff (mm) = {} +/ {}".format(np.mean(u_err_max)*10., np.std(u_err_max*10))
        print "mean u diff (mm) = {} +/ {}".format(np.mean(u_err_mean)*10., np.std(u_err_mean*10))

    if plot_gamma_error:
        errkey = "err_linf"
        synth_gamma = all_results["synthetic"]["gamma"]
      
        gamma_error = {}
        # Maximum value on the dofs
        max_synth_gamma = np.max([np.max(s) for s in synth_gamma.values()])
        times = synth_gamma.keys()

        colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
        
        fig = plt.figure()
        ax = fig.gca()
        # Loop over alphas
        for i, a in enumerate(alphas):
            gamma_error[a] = {}
            
            # Loop over time steps
            for t in times:
                gamma_error[a][t] = {}
             
                err_l2 = []
  
                # Inf error
                err_linf = [np.linalg.norm(np.subtract(all_results["simulated"][str(r)]["alpha_{}".format(a)]["lambda_{}".format(fixed_lambda)]["gamma"][t],
                                                       synth_gamma[t]), np.inf) for r in realizations]
                
                gamma_error[a][t]["err_l2"]  = np.mean(err_l2)
                gamma_error[a][t]["err_l2_std"]  = np.std(err_l2)
                gamma_error[a][t]["err_linf"]  = np.mean(err_linf)
                gamma_error[a][t]["err_linf_std"]  = np.std(err_linf)

            ax.plot(times, [gamma_error[a][t][errkey] for t in times],
                    "o", color = colors[i], label = r"$\alpha = {}$".format(a))
                     
        ax.legend(loc="best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\frac{ \| \gamma - \gamma^{ \mathrm{synth}} \|_{\infty} } { \| \gamma^{\mathrm{synth}} \|_{ \infty} }$")
      
        path = "/".join([outdir, "gamma_{}.pdf".format(errkey)])
        fig.savefig(path)

    if plot_displacement_error:
        # Just do the max over the dofs

        synth_disp = all_results["synthetic"]["displacement"]
    
        times = synth_disp.keys()
        disp_error = {}
       
        colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
        
        fig = plt.figure()
        ax = fig.gca()
        # Loop over alphas
        for i, a in enumerate(alphas):
            disp_error[a] = {}
          
            # Loop over time steps
            for t in times:
                disp_error[a][t] = {}

                err = [np.linalg.norm(np.subtract(all_results["simulated"][str(r)]["alpha_{}".format(a)]["displacement"][t],
                                                  synth_disp[t]), np.inf) for r in realizations]
        
                disp_error[a][t]["err"] = np.mean(err)
                disp_error[a][t]["err_std"] = np.std(err)

            ax.plot(times, [disp_error[a][t]["err"] for t in times], "o", color = colors[i], label = r"$\alpha = {}$".format(a))
        ax.legend(loc="best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\| \mathbf{u} - \mathbf{u}^{\mathrm{synth}} \|_{ \infty}$")
       
        path = "/".join([outdir, "disp_err.pdf"])
        fig.savefig(path)


    if plot_strains:
        ## Plot strain traces
        region = "1"
 
        # Corrected
        s = all_results["synthetic"]["strain_original"]["longitudinal"][region]
        s_noise = np.array([all_results["synthetic"]["strain_corrected"][n]["longitudinal"][region] \
                            for n in realizations])
        x = range(len(s))            
        
        fig = plt.figure(figsize=(12.8, 8.8))
        
        ax = fig.gca()
        
        ax.fill_between(x, s.tolist()[:3] + s_noise.max(0).tolist()[3:],
                           s.tolist()[:3] + s_noise.min(0).tolist()[3:],
                           facecolor="gray", alpha = 0.5)
        ax.plot(x,s, "k-", linewidth = 2.0, label = "Original")    
        ax.set_xlabel("Point")
        ax.set_xticks(x)
        ax.set_xticklabels(["${}$".format(xti) for xti in x])

        majorLocator = MultipleLocator(0.02)
        majorFormatter = FormatStrFormatter('%.2f')
        minorLocator = MultipleLocator(0.01)

        ax.yaxis.set_major_locator(majorLocator)
        ax.yaxis.set_major_formatter(majorFormatter)

        # for the minor ticks, use no labels; default NullFormatter
        ax.yaxis.set_minor_locator(minorLocator)
        ax.set_ylabel("Longitudinal strain")
        yt =  [0.00, 0.02, 0.04, 0.06]
        ax.set_yticks(yt)
        ax.set_yticklabels(["${}$".format(yti) for yti in yt])
        
        
        path = "/".join([outdir, "noisy_strain_region_1_ground.eps"])
        fig.savefig(path)

        s_sim = np.array([all_results["simulated"][n]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["strain"]["longitudinal"][region] \
                          for n in realizations])

        fig = plt.figure(figsize=(10.8, 8.8))
        ax = fig.gca()
        
        ax.fill_between(x, s_sim.max(0), s_sim.min(0), facecolor="gray", alpha = 0.5)
        ax.plot(x,s, "k-", linewidth = 2.0, label = "Original")    
        ax.legend(loc="best")
        
        ax.set_xlabel("Point")
        ax.set_xticks(x)
        ax.set_xticklabels(x)
        ax.set_yticklabels([])

      
        path = "/".join([outdir, "noisy_strain_region_1_repr.eps"])
        fig.savefig(path)


    if plot_snapshots:

        patient = load_patient_data()
        mesh = Mesh(patient.mesh)
        V_cg1 = FunctionSpace(mesh, "CG", 1)
        VV_cg2 = VectorFunctionSpace(mesh, "CG", 2)
        VV_cg1 = VectorFunctionSpace(mesh, "CG", 1)
    
        gamma = Function(V_cg1)
       
        u = Function(VV_cg2)
        u_prev = Function(VV_cg2)
        u_diff = Function(VV_cg2)
    
        snap_outdir = "/".join([outdir, "snapshots/alpha{}_lmbda{}".format(fixed_alpha, snap_lambda)])
        if not os.path.exists(snap_outdir):
            os.makedirs(snap_outdir)

        us = all_results["simulated"]["0"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["displacement"]
        gammas = all_results["simulated"]["0"]["alpha_{}".format(fixed_alpha)]["lambda_{}".format(snap_lambda)]["gamma"]
        times = sorted(gammas.keys(), key=asint)

        rnge = (0,0.15)
        path = "/".join([snap_outdir, "gamma_{}"])
        for t in times:
            u.vector()[:] = us[t]
            u_diff.vector()[:] = u.vector() - u_prev.vector()
            d = interpolate(u_diff, VV_cg1)
            ALE.move(mesh, d)
            
            # Simulated
            gamma.vector()[:] = gammas[t]
            snap_shot(gamma, path.format(t), "gamma", "simulated", rnge = rnge, colorbar = False)
            u_prev.assign(u)
        


        # Put together a canvas
        keys = times[1:]
        labels = [r"Point {}".format(int(i)) for i in keys]

        heatmappath = "/".join([snap_outdir, "heatmap"])
        snap_shot(gamma,  heatmappath, "gamma",
                  rnge = rnge, colorbar = True,
                  colorbar_align = "horizontal", colorbar_only=True)
        
        make_canvas_snap_shot(keys,labels,path[:-3], heatmap_name = heatmappath+"_front")
        make_video("/".join([snap_outdir, "gamma_%d_side.png"]), times,
                   "/".join([snap_outdir, "gamma_side.mp4"]))
        make_video("/".join([snap_outdir, "gamma_%d_front.png"]), times,
                   "/".join([snap_outdir, "gamma_front.mp4"]))

    if plot_synthetic_snapshots:

        patient = load_patient_data(synth_data=True)
        mesh = Mesh(patient.mesh)
        V_cg1 = FunctionSpace(mesh, "CG", 1)
        VV_cg2 = VectorFunctionSpace(mesh, "CG", 2)
        VV_cg1 = VectorFunctionSpace(mesh, "CG", 1)
        
        gamma = Function(V_cg1)
            
        u = Function(VV_cg2)
        u_prev = Function(VV_cg2)
        u_diff = Function(VV_cg2)
        
        snap_outdir = "/".join([outdir, "snapshots"])
        if not os.path.exists(snap_outdir):
            os.makedirs(snap_outdir)
            
        path = "/".join([snap_outdir, "syntheic_gamma_{}"])
        times = sorted(all_results["synthetic"]["displacement"].keys(), key=asint)

        for t in times:
            u.vector()[:] = all_results["synthetic"]["displacement"][t]
            u_diff.vector()[:] = u.vector() - u_prev.vector()
            d = interpolate(u_diff, VV_cg1)
            ALE.move(mesh, d)
            
                
            gamma.vector()[:] = all_results["synthetic"]["gamma"][t]
            snap_shot(gamma, path.format(t), "gamma", "synthetic", rnge = (0,0.15), colorbar = False)
            u_prev.assign(u)

        keys = times[1:]
        labels = [r"Point {}".format(int(i)) for i in keys]
        
        make_canvas_snap_shot(keys,labels,path[:-3])
        make_video("/".join([snap_outdir, "syntheic_gamma_%d_side.png"]), times,
                   "/".join([snap_outdir, "syntheic_gamma_side.mp4"]))
        make_video("/".join([snap_outdir, "syntheic_gamma_%d_front.png"]), times,
                   "/".join([snap_outdir, "syntheic_gamma_front.mp4"]))

        



def plot_strains(simulated_strains, measured_strains, outdir):

    
    
   
    ## Put the strain dictionaries in arrays
    colors = [cmap(i) for i in [0.0, 0.75]]
    # Basal, Mid, Apical
    regions_sep = [[1,3,4,6,5,2], [7,9,10,12,11,8], range(13, 18)]
    dirs = ['circumferential','radial', 'longitudinal']
    big_labels = ['Circumferential','Radial', 'Longitudinal']
    small_labels = ["Basal", "Mid", "Apical"]
    labels = ["Measured", "Simulated"]
  
    paths = []
    
    for d in range(3):
        direction = dirs[d]
        strains = []
        smaxs = []
        smins = []
        for i in range(3):

            smax = -np.inf 
            smin = np.inf
            for region in regions_sep[i]:
                cur_strain = [measured_strains[direction][str(region)],
                              simulated_strains[direction][str(region)]]

                strains.append(cur_strain)

                min_strain = np.min(cur_strain)
                max_strain = np.max(cur_strain)

                smax = max_strain if max_strain > smax else smax
                smin = min_strain if min_strain < smin else smin

            smaxs.append(smax)
            smins.append(smin)

        fig, big_ax = plt.subplots(figsize=(150.0, 60.0))

        # Labels for the big figure

        # Turn off axis lines and ticks of the big subplot 
        # obs alpha is 0 in RGBA string!
        
        big_ax.set_xticks([])
        big_ax.set_yticks([])
        # removes the white frame
        big_ax.set_frame_on(False)

        # Set the labels 
        big_ax.set_ylabel(big_labels[d], fontsize = 280)
        big_ax.yaxis.set_label_position("right")
        big_ax.yaxis.labelpad = 0


        regions = {1:"Anterior",
                   2:"Septum", 
                   3:"Inferior", 
                   4:"Lateral",
                   5:"Posterior",
                   6:"Anteroseptal",
                   7:"Apex"}

        # % of cardiac cycle
        x = np.linspace(0,100, len(strains[0][0]))

    
        t = 0
        # Add subplots with strain plots
        for i in range(1,22):

            if 1:
                ax = fig.add_subplot(3,7,i)

                # Put titles on the top ones at each level
          
                if d == 0:
                    if i in regions.keys():
                        ax.set_title(r"{}".format(regions[i]) , fontsize = 220, y = 1.1)

                if i in [7, 14, 19, 20]:
                    ax.set_axis_off()
                    continue

                # Put ticks on every one of them
                ax.set_xlim(0,100)
                ax.set_xticks([0,50,100])
              
                minorLocator_y = MultipleLocator(0.01)
                ax.yaxis.set_minor_locator(minorLocator_y)
                minorLocator_x = MultipleLocator(25)
                ax.xaxis.set_minor_locator(minorLocator_x)
                
                if i <= 7:
                    ax.set_ylim(smins[0], smaxs[0])
                    ax.set_yticks([smins[0], 0, smaxs[0]])
                    ax.set_yticklabels([r"${:.2f}$".format(yti) for yti in [smins[0], 0, smaxs[0]]], fontsize = 220)
                    
                elif i > 14:
                    ax.set_ylim(smins[2], smaxs[2])
                    ax.set_yticks([smins[2], 0, smaxs[2]])
                    ax.set_yticklabels([r"${:.2f}$".format(yti) for yti in [smins[2], 0, smaxs[2]]], fontsize = 220)

                else:
                    ax.set_ylim(smins[1], smaxs[1])
                    ax.set_yticks([smins[1], 0, smaxs[1]])
                    ax.set_yticklabels([r"${:.2f}$".format(yti) for yti in [smins[1], 0, smaxs[1]]], fontsize = 220)
                    

                # Put xlabels only on the bottom ones
                if i in [12,13,15, 16, 17, 18, 21] and d == 2: 
                    ax.set_xticklabels([r"${}$".format(xti) for xti in [0,50,100]], fontsize = 220)
                    
                else:
                    ax.set_xlabel("")
                    ax.set_xticklabels([])

                # Put y labels only on the left most ones
                if i not in [1,8,15]:
                    ax.set_ylabel("")
                    ax.set_yticklabels([])
                else:
                    
                    if i == 1:
                        ax.set_ylabel(small_labels[0], fontsize = 220) # Basal
                    elif i == 8:
                        ax.set_ylabel(small_labels[1], fontsize = 220) # Mid
                    else:
                        ax.set_ylabel(small_labels[2], fontsize = 220) # Apical

                    

                
                strain = strains[t]
                t+= 1
                
                l1 = ax.plot(x, strain[0], color = colors[0], linestyle = "-", label = labels[0], linewidth = 18.0)
                l2 = ax.plot(x, strain[1], color = colors[1],  linestyle = "-", label = labels[1], linewidth = 18.0)
                ax.axhline(y=0, color = "k", ls = ":",linewidth = 15.0)
                lines = [l1[0], l2[0]]
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.yaxis.set_ticks_position('left')
                ax.xaxis.set_ticks_position('bottom')
                ax.yaxis.set_tick_params(length=70, pad = 50)
                ax.xaxis.set_tick_params(length=70, pad = 50)

                ax.tick_params(axis="y", which = "minor", length=40, pad = 50)
                ax.tick_params(axis="x", which = "minor", length=40, pad = 50)

            # Plot the legend
            if i == 7:
                ax = fig.add_subplot(3,7,21)
                ax.set_axis_off()
                ax.legend(lines, labels, "center", prop={'size':170})

        # Adjust size
        
        fig.tight_layout(w_pad = 0.0)
     
        # Remove top and right axis
        path = "{}/simulated_strain_{}.pdf".format(outdir, direction)
        fig.savefig(path, bbox_inches='tight')
        plt.close()
        paths.append(path)

    make_canvas_strain(paths)

def get_opt_timings():

    spaces = [("CG1", 0.01), ("regional", 0.0), ("scalar", 0.01)]
    resdir = "results/test_times/{}/alpha_0.95/regpar_{}/result.h5"

    forward_times= {"CG1": [], "regional":[], "scalar":[]}
    backward_times = {"CG1": [], "regional":[], "scalar":[]}
    maxiter_reached = {"CG1": [], "regional":[], "scalar":[]}
    

    h5group = "alpha_0.95/active_contraction/contract_point_{}"
    forgroup = "/".join([h5group, "forward_times"])
    backgroup = "/".join([h5group, "backward_times"])
    contgroup  = "/".join([h5group, "controls"])

    
    
    for s in spaces:

        path = resdir.format(s[0], s[1])
        with h5py.File(path, "r") as h5file:

            p = 0
            while h5group.format(p) in h5file:
                forward_times[s[0]].append(np.array(h5file[forgroup.format(p)]))
                backward_times[s[0]].append(np.array(h5file[backgroup.format(p)]))

                maxiter_reached[s[0]].append(len(np.array(h5file[contgroup.format(p)])) == 102)
                
                p += 1
                
   
    n = len(maxiter_reached["CG1"])
    
    print " "*10+"\t{:<19}\t{:<19}\t{:<19}".format("Forward times", "Bacward times", "Opt Criteria Met")
    for k in forward_times.keys():
        for_mean = np.mean([np.mean(a) for a in forward_times[k]])
        for_std = np.mean([np.std(a) for a in forward_times[k]])

        back_mean = np.mean([np.mean(a) for a in backward_times[k]])
        back_std = np.mean([np.std(a) for a in backward_times[k]])
        n_maxit = sum(maxiter_reached[k])
       
        print "{:<10}\t{:<8.3f} +/- {:<7.3f}\t{:<8.3f} +/- {:<7.3f}\t{:<}/{:<}".format(k, for_mean, for_std,
                                                                                   back_mean, back_std,
                                                                                   (n-n_maxit), n)
    
                  

def basal_spring_sensitivity(plot_gamma = False, outdir = ""):

    print_misfit = False
    compute_endo_diameter = False

    alpha = 0.9
    outdir = "figures/sensitivity_analysis/spring_constant_alpha{}".format(alpha) \
             if outdir == "" else outdir
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    fname = "results/spring_constant_sensitivity_alpha{}.h5".format(alpha)
    
    setup_general_parameters()
    patient = load_patient_data()
    params = setup_application_parameters()
    params["alpha"] = alpha

    # "0.0" failed
    ks = ["1e-08", "1e-07", "1e-06", "1e-05",
          "0.0001", "0.001", "0.01", "0.1",
          "1.0", "10.0", "100.0", "inf"]


    results = load_dict(fname)


    if print_misfit:
        # Material parameters
        caption = "Optimal material parameters using different spring " + \
                  "constants $k$, and initial material parameters: " + \
                  "$a = {}$".format(results['0.1']["material_parameters_initial"][0])
        header = ["$k$", "$a$"] 
        table = [[k,results[k]["material_parameters_optimal"][0]] for k in ks]
        label_mat = "tab:spring_test_matparams"
    
        T = tabalize(caption, header, table, label_mat, floatfmt="0.3g")
        print T
            
        # Passive misfit
        
        caption = "Misfit during passive optimization for different spring constants $k$"
        header = ["$k$", "Initial $I_{\\text{strain}}$",
                  "Optimal $I_{\\text{strain}}$",
                  "Initial $I_{\\text{volume}}$",
                  "Optimal $I_{\\text{volume}}$"]
        table = [[k,
                  results[k]["I_strain_initial_passive"],
                  results[k]["I_strain_optimal_passive"],
                  results[k]["I_volume_initial_passive"],
                  results[k]["I_volume_optimal_passive"]] for k in ks]
        label = "tab:spring_test_passive"
        
        T = tabalize(caption, header, table, label)
        print T
        
        # Active mistfit
        caption = "Misfit during active optimization for different spring " + \
                  "constants $k$, using the corresponding material parameters " + \
                  "in Table \\ref{{{}}}".format(label_mat)
        header = ["$k$", "Initial $I_{\\text{strain}}$",
                  "Optimal $I_{\\text{strain}}$",
                  "Initial $I_{\\text{volume}}$",
                  "Optimal $I_{\\text{volume}}$"]
        table = [[k,
                  results[k]["I_strain_initial_active"],
                  results[k]["I_strain_optimal_active"],
                  results[k]["I_volume_initial_active"],
                  results[k]["I_volume_optimal_active"]] for k in ks]
        label = "tab:spring_test_active"
    
        T = tabalize(caption, header, table, label)
        print T



    # Mean gamma
    if plot_gamma:
        
        colors = [cmap(i) for i in np.linspace(0, 1, len(ks[4:]) +1)]

        n = patient.num_points
        gamma_space = FunctionSpace(patient.mesh, "CG", 1)
        gamma = Function(gamma_space)
        dX = Measure("dx",subdomain_data = patient.strain_markers, domain = patient.mesh)
        fig = plt.figure()
        ax = fig.gca()
        for i, k in enumerate(ks[4:]):

            
            gamma_lst = [results[k]["gammas"][t] for t in sorted(results[k]["gammas"], key=asint)]
            gamma_mean = get_global(dX, gamma, gamma_lst)
           
            if k == "inf":
                ax.plot(gamma_mean, label = r"$k = \infty$", color = colors[i], linestyle = next(linestyles))
            else:
                ax.plot(gamma_mean, label = r"$k = {}$".format(k), color = colors[i], linestyle = next(linestyles))

        lgd = ax.legend(loc = "center left", bbox_to_anchor=(1, 0.5))
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\overline{\gamma}$")
        fig.tight_layout()
        path = "/".join([outdir, "gamma_mean.eps"])
        fig.savefig(path, bbox_extra_artists=(lgd,), bbox_inches='tight')

    if compute_endo_diameter:
        mesh = Mesh(patient.mesh)


def plot_regional_synthetic_noisefree():
    plot_l_curve = False
    plot_gamma_error = False
    plot_displacement_error = False
    plot_strains = False
    plot_snapshots = False
    print_gamma_error = True
    print_disp_error = False

    outdir = "figures/regional/synthetic_noisefree"
    
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    fname = "results/synthetic_noisefree_regional.h5"

    setup_general_parameters()
    patient = load_patient_data(synth_data = True)
    params = setup_application_parameters()

    

    
    params["noise"] = False
    
    params["reg_par"] = 0.0
    params["synth_data"] = True
    params["gamma_space"] = "regional"
    
    alphas = [str(i/10.) for i in range(11)] + ["0.95", "0.99", "0.999", "0.9999"]
  
 

    V_cg1 = FunctionSpace(patient.mesh, "CG", 1)
    VV_cg2 = VectorFunctionSpace(patient.mesh, "CG", 2)


    u_sim = Function(VV_cg2)
    u_synth = Function(VV_cg2)
    u_err = Function(VV_cg2)


    all_results = load_dict(fname)


    alphas.sort()

    if print_gamma_error:

        dX = Measure("dx", subdomain_data = patient.strain_markers, domain = patient.mesh)
        synth_gamma = all_results["synthetic"]["gamma"]
        times = sorted(synth_gamma.keys(), key=asint)
      
        gamma_synth_regional = []
        for t in times:

            gamma_synth_regional.append(synth_gamma[t])

        max_g_reg = np.array(gamma_synth_regional).max(0)

        # g_err_l2 = []
        g_err_regional = []
        sim_gamma = all_results["simulated"]["gamma"]      
        for i, t in enumerate(times):

            g_err_regional.append(np.mean(np.divide(np.abs(np.subtract(sim_gamma[t],
                                                                       synth_gamma[t])),
                                                    max_g_reg)))

        print "max gamma error (regional) = {}".format(np.max(g_err_regional))
        print "mean gamma error (regional) = {}".format(np.mean(g_err_regional))
        
    if print_disp_error:

        synth_disp = all_results["synthetic"]["displacement"]
        sim_disp = all_results["simulated"]["displacement"]
        times = sorted(synth_disp.keys(), key=asint)
            
        u_diff = [np.abs(np.subtract(sim_disp[t], synth_disp[t])) for t in times]
        print "max u diff (mm) = {}".format(np.max(u_diff)*10.)
        print "mean u diff (mm) = {}".format(np.mean(u_diff)*10.)
        

    if plot_gamma_error:
        errkey = "err_linf"
        synth_gamma = all_results["synthetic"]["gamma"]
      
        gamma_error = {}
        # Maximum value on the dofs
        max_synth_gamma = np.max([np.max(s) for s in synth_gamma.values()])
        times = synth_gamma.keys()
       
        colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
        
        fig = plt.figure()
        ax = fig.gca()
        # Loop over alphas
        gamma_error = {}
        # Loop over time steps
        for t in times:
            gamma_error[t] = {}
            
            # Inf error
            err_linf = np.linalg.norm(np.subtract(all_results["simulated"]["gamma"][t], synth_gamma[t]), np.inf)
                
            gamma_error[t]["err_linf"]  = np.mean(err_linf)
               

        
        ax.plot(times, [gamma_error[t][errkey] for t in times],
                "o", color = colors[0])
                     
        ax.legend(loc="best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\frac{ \| \gamma - \gamma^{ \mathrm{synth}} \|_{\infty} } { \| \gamma^{\mathrm{synth}} \|_{ \infty} }$")
        
        path = "/".join([outdir, "gamma_{}_new.pdf".format(errkey)])
        fig.savefig(path)

    if plot_displacement_error:
        # Just do the max over the dofs

        synth_disp = all_results["synthetic"]["displacement"]
    
        times = synth_disp.keys()
        disp_error = {}
        
        colors = [cmap(i) for i in np.linspace(0, 1, len(alphas))]
        
        fig = plt.figure()
        ax = fig.gca()
        # Loop over alphas
        disp_error= {}
          
        # Loop over time steps
        for t in times:
            disp_error[t] = {}
            
            err = np.linalg.norm(np.subtract(all_results["simulated"]["displacement"][t],
                                             synth_disp[t]), np.inf)
            
            disp_error[t]["err"] = np.mean(err)

        ax.plot(times, [disp_error[t]["err"] for t in times], "o", color = colors[0])
        ax.legend(loc="best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\| \mathbf{u} - \mathbf{u}^{\mathrm{synth}} \|_{ \infty}$")
        
        path = "/".join([outdir, "disp_err_new.pdf"])
        fig.savefig(path)


    if plot_strains:
        region = "1"
        # Simulated
        s = all_results["synthetic"]["strain"]["longitudinal"][region]
        x = range(len(s))            
        
        for a in alphas:
            fig = plt.figure()
            ax = fig.gca()
            ax.plot(all_results["simulated"]["alpha_{}".format(a)]["strain"]["longitudinal"][region], label = "alpha = {}".format(a))

            ax.plot(x,s, "k-", linewidth = 2.0, label = "Original")
            ax.legend(loc="best")
            path = "/".join([outdir, "strain_simulated_region_{}_alpha_{}.pdf".format(region, a)])
            fig.savefig(path)

    if plot_snapshots:

        snap_outdir = "/".join([outdir, "snapshots"])
        if not os.path.exists(snap_outdir):
            os.makedirs(snap_outdir)

        synth_gamma = all_results["synthetic"]["gamma"]
        times = synth_gamma.keys()

        path_sim = "/".join([snap_outdir, "gamma_simulated_{}"])
        path_synth = "/".join([snap_outdir, "gamma_synthetic_{}"])
                
        for t in times:
            # Simulated
            gamma_sim.vector()[:] = \
                all_results["simulated"]["gamma"][t]
            
            snap_shot(gamma_sim, path_sim.format(t), "gamma", "simulated")

            gamma_synth.vector()[:] = synth_gamma[t]
            
            snap_shot(gamma_synth, path_synth.format(t), "gamma", "synthetic")

        
        # Put together a canvas
        make_canvas_snap_shot(sorted(times, key=asint)[1:],
                              ["Point {}".format(t) for t \
                               in sorted(times, key=asint)][1:],
                              path_sim[:-3])
        make_canvas_snap_shot(sorted(times, key=asint)[1:],
                              ["Point {}".format(t) for t \
                               in sorted(times, key=asint)][1:],
                              path_synth[:-3])


def mesh_resoltion_convergence(plot_mean_gamma =False, outdir = ""):


    
    print_misfit = False
    
    alpha = "0.95"
    lmbda = "0.01"

    fname = "results/mesh_res_conv.h5"
    
    outdir = "figures/sensitivity_analysis/mesh_res_conv" if outdir == "" else outdir

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    patient_low = load_patient_data(res = "low")
    patient_med = load_patient_data()
    patient_high = load_patient_data(res = "high")

    res = load_dict(fname)

    if plot_mean_gamma:

        fig = plt.figure()
        ax = fig.gca()

        
        colors = [cmap(i) for i in [0.0, 0.4, 0.8]]
        # Low
        dX = Measure("dx",subdomain_data = patient_low.strain_markers,
                     domain = patient_low.mesh)
        gamma_space = FunctionSpace(patient_low.mesh, "CG", 1)
        gamma = Function(gamma_space)
        gamma_lst = [res["low"]["gamma"][k] for k in sorted(res["low"]["gamma"], key=asint)]
        gamma_mean = get_global(dX, gamma, gamma_lst)
        ax.plot(gamma_mean, color = colors[0], linestyle = next(linestyles), label = r"{} vertices".format(patient_low.mesh.num_vertices()))

        # Med res
        dX = Measure("dx",subdomain_data = patient_med.strain_markers,
                     domain = patient_med.mesh)
        gamma_space = FunctionSpace(patient_med.mesh, "CG", 1)
        gamma = Function(gamma_space)
        gamma_lst = [res["med"]["gamma"][k] for k in sorted(res["med"]["gamma"], key=asint)]
        gamma_mean = get_global(dX, gamma, gamma_lst)
        ax.plot(gamma_mean, color = colors[1], linestyle = next(linestyles), label = r"{} vertices".format(patient_med.mesh.num_vertices()))

        # High res
        dX = Measure("dx",subdomain_data = patient_high.strain_markers,
                     domain = patient_high.mesh)
        gamma_space = FunctionSpace(patient_high.mesh, "CG", 1)
        gamma = Function(gamma_space)
        gamma_lst = [res["high"]["gamma"][k] for k in sorted(res["high"]["gamma"], key=asint)]
        gamma_mean = get_global(dX, gamma, gamma_lst)
        ax.plot(gamma_mean, color = colors[2], linestyle = next(linestyles), label = r"{} vertices".format(patient_high.mesh.num_vertices()))

        
        ax.legend(loc = "best")
        ax.set_xlabel("Point")
        ax.set_ylabel(r"$\overline{\gamma}$")
        path = "/".join([outdir, "mesh_conv_gamma.eps"])
        fig.savefig(path, format="eps")
        
    if print_misfit:

        
        lst = []
        for r in ["low", "med", "high"]:
            meas_data = res[r]["measured"]

            sim_vol = res[r]["volume"]
            n = len(sim_vol)
            meas_vol = meas_data["volume"][-n:]
           
            I_vol = np.sum(np.abs(np.subtract(sim_vol,meas_vol)))/np.sum(meas_vol)
            print I_vol
            I_strain_rel, I_strain_max = get_Istrain(meas_data, res[r], n)

            if r == "low":
                nverts = patient_low.mesh.num_vertices()
            elif r == "high":
                nverts = patient_high.mesh.num_vertices()
            else:
                nverts = patient_med.mesh.num_vertices()

            lst.append([nverts, I_vol, I_strain_rel, I_strain_max])

        caption = "Relative misfit for different mesh resolutions"
        header = ["$\#$ vertices",
                  "$\overline{I}_{\mathrm{vol}}$",
                  "$\overline{I}_{\mathrm{strain}}^{\mathrm{rel}}$",
                  "$\overline{I}_{\mathrm{strain}}^{\mathrm{max}}$"]
        table = lst
        label = "tab:mesh_conv_opt_misfit"
        T = tabalize(caption, header, table, label, floatfmt=".2g")
        print T


def get_Istrain(measured, simulated, n):
    I_strain_tot_rel = 0
    I_strain_tot_max = 0
    for d in measured["strain"].keys():
        
        I_strain_region_rel = []
        I_strain_region_max = []

        s_max = np.max([np.max(np.abs(s)) for s in measured["strain"][d].values()])
        for region in measured["strain"][d].keys():
            
            s_meas = measured["strain"][d][region][-n:]
            s_sim =  simulated["strain"][d][region][-n:]
                        

            err_max =  np.divide(np.mean(np.abs(np.subtract(s_sim,s_meas))),
                                 s_max)
            err_rel = np.divide(np.sum(np.abs(np.subtract(s_sim,s_meas))),
                                np.sum(np.abs(s_meas)))
            I_strain_region_max.append(err_max)
            I_strain_region_rel.append(err_rel)
  
        I_strain_tot_rel += np.mean(I_strain_region_rel)
        I_strain_tot_max += np.mean(I_strain_region_max)
                
    I_strain_rel = I_strain_tot_rel/3.
    I_strain_max = I_strain_tot_max/3.

    return I_strain_rel, I_strain_max

def get_Istrain_noisy(measured, simulated, n, realization):

    
    I_strain_tot_rel = 0
    I_strain_tot_max = 0


    for d in simulated["strain"].keys():

        I_strain_region_rel = []
        I_strain_region_max = []

        s_max = np.max([np.max(np.abs(s)) for s in measured["strain_corrected"][realization][d].values()])
        for r in simulated["strain"][d].keys():
            # Take out only the active part
            s_meas = measured["strain_corrected"][realization][d][r][-n:]
            s_sim =  simulated["strain"][d][r][-n:]

            err_max =  np.divide(np.mean(np.abs(np.subtract(s_sim,s_meas))[SYNTH_PASSIVE_FILLING:-1]),
                                 s_max)
            err_rel = np.divide(np.sum(np.abs(np.subtract(s_sim,s_meas))[SYNTH_PASSIVE_FILLING:-1]),
                                np.sum(np.abs(s_meas)[SYNTH_PASSIVE_FILLING:-1]))
            I_strain_region_max.append(err_max)
            I_strain_region_rel.append(err_rel)
  
        I_strain_tot_rel += np.mean(I_strain_region_rel)
        I_strain_tot_max += np.mean(I_strain_region_max)
                
    I_strain_rel = I_strain_tot_rel/3.
    I_strain_max = I_strain_tot_max/3.
                    

    return I_strain_rel, I_strain_max


def plot_publication_figures():

    outdir = "figures/publication"
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    
    mesh_resoltion_convergence(True, outdir = outdir)
    basal_spring_sensitivity(True, outdir = outdir)
    material_parameter_sensitivity_fixed(outdir = outdir)
    plot_patient_pv_loop(outdir = outdir)

    
    # # # Synth w noise
    plot_synthetic_results_CG1_w_noise(outdir = outdir,
                                       plot_mean_gamma = True,
                                       plot_strains = True,
                                       plot_l_curve_fixed_alpha = True,
                                       plot_l_curve_fixed_lambda = True)


    # # # # Patient
    plot_CG_1_results(case_id = 0,
                      plot_all_strains = True,
                      plot_pv_loop = True, 
                      plot_l_curve_fixed_alpha = True,
                      plot_l_curve_fixed_lambda = True,
                      plot_mean_gamma_varying_alpha =True,
                      plot_mean_gamma_varying_lambda =True,
                      outdir = outdir)
                    
    
    
if __name__ == "__main__":
    plot_publication_figures()
