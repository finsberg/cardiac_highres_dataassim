#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of PULSE-ADJOINT.
#
# PULSE-ADJOINT is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE-ADJOINT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE-ADJOINT. If not, see <http://www.gnu.org/licenses/>.
import os
from numpy import logspace, multiply
from itertools import product
import yaml


filepath= os.path.dirname(os.path.abspath(__file__))
OUTPATH_REAL = filepath+"/results/real{}_final_low_res/alpha_{}/regpar_{}"
OUTPATH_SYNTH = filepath+"/results/{}synthetic_noise_{}{}_final/alpha_{}/regpar_{}/{}"

def main():

    # Synthetic data or not
    synth_data = False
    noise = False # if synthetic data is chosen
    use_deintegrated_strains = False
    passive_synth = False
    ### Combinations ###

    # Patient parameters

    # Which patients
    # patients = ["Impact_p8_i56", 
#                 "Impact_p9_i49", 
#                 "Impact_p10_i45", 
#                 "Impact_p12_i45", 
#                 "Impact_p14_i43", 
#                 "Impact_p15_i38"]
# #   
    patients = ["Impact_p16_i43"]
    #patients = ["CRID-pas_ESC"]
    # Optimization parameters
    res = "low"
    # Weighting of strain and volume and regularization
    if synth_data:
        #alphas = [0.0, 0.1, 0.5, 0.9, 1.0] + [0.95, 0.99, 0.999, 0.9999] + [0.2, 0.3, 0.4, 0.6, 0.7, 0.8]
        #alphas = [0.2, 0.3, 0.4, 0.6, 0.7, 0.8]
        alphas = [0.95]
        #reg_pars = [0.0,  0.0001, 0.001, 0.01]
        reg_pars = [0.0]
        #reg_pars = [0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0]
        #reg_pars = [0.0, 0.0001, 0.001, 0.01]
    else:
        alphas = [0.95]
        #alphas = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 1.0]
        #alphas = [0.0, 0.1, 0.5, 0.9, 1.0] + [0.95, 0.99, 0.999, 0.9999]
        #alphas = [0.2, 0.3, 0.4, 0.6, 0.7, 0.8]
        #reg_pars = [0.0, 0.0001, 0.001, 0.01]
        #reg_pars = [0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0]
        reg_pars = [0.0]
    # Weighting of strain and volume for passive phase
    alpha_matparams = [1.0]

    active_maxiter = 100
    ### Fixed for all runs ###
    
    # Compressibility
    # Possibilities: ["incompressible", "stabalized_incompressible", "penalty", "hu_washizu"]
    compressibility = "incompressible"
    incompressibility_penalty = 0.0

    # Space for contraction parameter
    gamma_space = "CG_1"
    # Use gamma from previous iteration as intial guess
    nonzero_initial_guess = True
    # Optimize material parameters or use initial ones
    optimize_matparams = True
    
    # Spring constant at base
    base_spring_k = 1.0
    # Initial material parameters
    #material_parameters = {"a":0.795, "b":6.855, "a_f":21.207, "b_f":40.545}
    if synth_data:
        if passive_synth:
            material_parameters = {"a":2.280, "a_f":1.685, "b":9.726, "b_f":15.779}
        else:
            material_parameters = {"a":0.435, "a_f":1.685, "b":9.726, "b_f":15.779}
        
    else:
        material_parameters = {"a":2.280, "a_f":1.685, "b":9.726, "b_f":15.779}

    
    #{"a":0.291, "a_f":2.582, "b":5.0, "b_f":5.0}
    patient_type = "full"

    ### Run combinations ###
    # How to apply the weights on the strain (direction, rule, custom weights)
    # Not custom weights not implemented yet
    weights = [("all", "equal", None)]
    # Resolution of mesh
    resolutions = ["low_res"]
    # Fiber angles (endo, epi)
    fiber_angles = [(40,50)]

    if synth_data and noise:
        realizations = range(30) if passive_synth else range(10)
    else:
        realizations = [0]

    # Find all the combinations
    comb = list(product(alphas, reg_pars, alpha_matparams, realizations))

    # Directory where we dump the paramaters
    input_directory = "input"
    if not os.path.exists(input_directory):
        os.makedirs(input_directory)

    fname = input_directory + "/file_{}.yml"
    
    # Find which number we 
    t = 1
    while os.path.exists(fname.format(t)):
        t += 1
    t0 = t


    for c in comb:

        #params = setup_adjoint_contraction_parameters()
        params = {}
        params["alpha"] = c[0]
        params["reg_par"] = c[1]
        params["alpha_matparams"] = c[2]
        params["noise_realization"] = c[3]
        params["passive_synth"] = passive_synth
        params["res"] = res
        params["base_spring_k"] = base_spring_k
        params["optimize_matparams"] = optimize_matparams
        params["use_deintegrated_strains"] = use_deintegrated_strains
        
        params["gamma_space"] = gamma_space
        params["Material_parameters"] = material_parameters
        params["synth_data"] = synth_data
        params["noise"] = noise
        
        params["Optimization_parmeteres"] = {}
        params["Optimization_parmeteres"]["active_maxiter"] = active_maxiter
        # params["Patient_parameters"]["patient_type"] = "full"
        if gamma_space == "R_0":
            scalar_str = "_scalar"
        elif gamma_space == "regional":
            scalar_str = "_regional"
        else:
            scalar_str = ""


        if synth_data:
            pstr = "passive_const_" if passive_synth else ""
            outdir = OUTPATH_SYNTH.format(pstr, noise, scalar_str, c[0], c[1], c[3])

        else:

            outdir = OUTPATH_REAL.format(scalar_str, c[0], c[1])

        # Make directory if it does not allready exist
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        params["outdir"] = outdir
        params["sim_file"] = "/".join([outdir, "result.h5"])

        # Dump paramters to yaml
        with open(fname.format(t), 'wb') as parfile:
            yaml.dump(params, parfile, default_flow_style=False)
        t += 1

    #os.system("sbatch save_patient_data.slurm {} {}".format(t0, t-1))
    os.system("sbatch run_submit.slurm {} {}".format(t0, t-1))


if __name__ == "__main__":
    main()
