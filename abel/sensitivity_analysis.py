#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of PULSE-ADJOINT.
#
# PULSE-ADJOINT is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE-ADJOINT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE-ADJOINT. If not, see <http://www.gnu.org/licenses/>.
import os
from numpy import logspace, multiply
from itertools import product
import yaml


filepath= os.path.dirname(os.path.abspath(__file__))
OUTPATH = "/".join([filepath, "results/sesitivity_analysis/{}"])


def main():

    matparmams_sens = True
    base_spring_sens = False
    
    space_sens = False
    
    # Space for contraction parameter
    if space_sens:
        gamma_spaces = ["CG_1", "regional", "R_0"]
    else:
        gamma_spaces = ["CG_1"]

    # Spring constant at base
    if base_spring_sens:
        base_springs = [0] + [10**i/1000. for i in range(6)] + ['inf']
    else:
        base_springs = [1.0]
        
    # Initial material parameters
    # Holzapfel and Ogden : a = 2.280
    if matparmams_sens:
        material_parameters = [{"a":0.561, "a_f":1.685, "b":9.726, "b_f":15.779},
                               {"a":4.835, "a_f":1.685, "b":9.726, "b_f":15.779},
                               {"a":24.985, "a_f":1.685, "b":9.726, "b_f":15.779},
                               {"a":19.643, "a_f":1.685, "b":9.726, "b_f":15.779},
                               {"a":31.635, "a_f":1.685, "b":9.726, "b_f":15.779},
                               {"a":44.894, "a_f":1.685, "b":9.726, "b_f":15.779},
                               {"a":12.210, "a_f":1.685, "b":9.726, "b_f":15.779},
                               {"a":37.386, "a_f":1.685, "b":9.726, "b_f":15.779}]
    else:
        material_parameters = [{"a":0.435, "a_f":1.685, "b":9.726, "b_f":15.779}]
        
        
    # Weighting of strain and volume
    alpha_matparams = 1.0
    alpha = 0.5
    reg_par = 0.0


    ### Fixed for all runs ###
    
    # Compressibility
    # Possibilities: ["incompressible", "stabalized_incompressible", "penalty", "hu_washizu"]
    compressibility = "incompressible"
    incompressibility_penalty = 0.0

    
    # Use gamma from previous iteration as intial guess
    nonzero_initial_guess = True
    # Optimize material parameters or use initial ones
    optimize_matparams = False


    ### Run combinations ###
    # How to apply the weights on the strain (direction, rule, custom weights)
    # Not custom weights not implemented yet
    weights = [("all", "equal", None)]

    # Fiber angles (endo, epi)
    fiber_angles = [(40,50)]


    # Find all the combinations
    comb = list(product(gamma_spaces, base_springs, material_parameters))

    # Directory where we dump the paramaters
    input_directory = "input"
    if not os.path.exists(input_directory):
        os.makedirs(input_directory)

    fname = input_directory + "/file_{}.yml"
    
    # Find which number we 
    t = 1
    while os.path.exists(fname.format(t)):
        t += 1
    t0 = t


    for c in comb:

        #params = setup_adjoint_contraction_parameters()
        params = {}
        params["alpha"] = alpha
        params["reg_par"] = reg_par
        params["alpha_matparams"] = alpha_matparams

        if c[1] == 'inf':
            params["base_bc"] = "fixed"
        else:
            params["base_bc"] = "dirichlet_bcs_fix_base_x"
            params["base_spring_k"] = c[1]
       
        
        params["gamma_space"] = c[0]
        params["Material_parameters"] = c[2]
     

        if matparmams_sens:
            s = "material_parameters/a_{}".format(c[2]["a"])
            outdir = OUTPATH.format(s)
        elif base_spring_sens:
            s = "spring_constat/k_{}".format(c[1])
            outdir = OUTPATH.format(s)
            
                
        # Make directory if it does not allready exist
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        params["outdir"] = outdir
        params["sim_file"] = "/".join([outdir, "result.h5"])

        # Dump paramters to yaml
        with open(fname.format(t), 'wb') as parfile:
            yaml.dump(params, parfile, default_flow_style=False)
        t += 1

    #os.system("sbatch save_patient_data.slurm {} {}".format(t0, t-1))
    os.system("sbatch run_submit.slurm {} {}".format(t0, t-1))


if __name__ == "__main__":
    main()
