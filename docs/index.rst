.. Cardiac Highres Data Assimilation documentation master file, created by
   sphinx-quickstart on Wed Jan  4 10:25:47 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cardiac Highres Data Assimilation's documentation!
=============================================================

cardiac_highres_dataassim contains the code to reproduce the results
given in the paper `<link to paper>`

A cardiac computational model is constrained using clinical measurements such as pressure, volume and regional strain. The problem is formulated as a PDE-constrained optimisation problem where the objective functional represents the misfit between measured and simulated data. The control parameter for the active phase is a spatially varying contraction parameter defined at every vertex in the mesh. The control parameters for the passive phase is the linear isotropic material parameter from a Holzapfel and Ogden transversally isotropic material. The problem is solved using a gradient based optimization algorithm where the gradient is provided by solving the adjoint system

pulse_adjoint originates from the `Center for Cardiological Innovation
<http://heart-sfi.no>`__, a Norwegian Centre of Research Based Innovation, hosted by
`Simula Research Laboratory <http://www.simula.no>`__, Oslo, Norway.


Installation and dependencies:
==============================

The cardiac_highres_dataassim source code is hosted on Bitbucket:

  https://bitbucket.org/finsberg/cardiac_highres_dataassim

cardiac_highres_dataassim is based on

* The FEniCS Project software (http://www.fenicsproject.org)
* dolfin-adjoint (http://www.dolfin-adjoint.org)


Install the software using

.. code::

   python setup.py install


Contents:
=========

.. toctree::
   :maxdepth: 2

   demo
   postprocess
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Licence
=======
 
cardiac_highres_dataassim is an open source project that can be freely used under the `GNU GPL version 3 licence`_.
 
.. _GNU GPL version 3 licence: http://www.gnu.org/licenses/gpl.html


Citing
======
If you plan to use the code in your research we would be grateful if you could cite the following publication:



Main authors:
=============

* Henrik Finsberg    (henriknf@simula.no)
