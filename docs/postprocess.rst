Postprocess
===========


In the repository there is also a script for postprocessing the results called `postprocess.py`
If you are interested to reproduce the results from the paper, you can simply run the script
`reproduce_figures.sh` which will download the results from a dropbox link, and generate the
figures in a folder figures.

If you want to use the script `postprocess.py` to postprocess your own results please consult
the code itself. 
