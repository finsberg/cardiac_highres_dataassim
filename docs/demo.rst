Demo
====

This demo shows how to run the code using the patient data and using synthetic data.
The script `demo.py` implements this functionality.


Parameters
**********

Before running the code you should choose the parameters. Different parameters are listed in the table below.
These are the same parameters as in `setup_application_parameters()` in `demo.py`


    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | key                                 | Default Value                                        | Description                         |
    +=====================================+======================================================+=====================================+
    | base_bc                             |  'dirichlet_bcs_fix_base_x'                          | Boundary condition at the base.     |
    |                                     |                                                      | ['dirichlet_bcs_fix_base_x','fixed']|
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | alpha                               | 0.9                                                  | Weight between strain and volume    |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | reg_par                             | 0.01                                                 | Regularization parameter            |         
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | noise                               | False                                                | If synthetic data, add noise        |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | noise_path                          | './data'                                             | Path to noise data                  |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | noise_realization                   | 0                                                    | Which realization of noise          |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | use_deintegrated_strains            | False                                                | Use full strain field               |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | alpha_matparams                     | 1.0                                                  | Weight between strain and volume    |
    |                                     |                                                      | for the passive phase               |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | base_spring_k                       | 1.0                                                  | Basal spring constant               |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | synth_data                          | False                                                | Synthetic data                      |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | sim_file                            | 'result.h5'                                          | Path to result file                 |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | res                                 | 'normal'                                             | Mesh resolution                     |
    |                                     |                                                      | ['low', 'normal', high']            |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | Material_parameters                 | {'a': 2.28, 'a_f': 1.685, 'b': 9.726, 'b_f': 15.779} | Material parameters                 |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | phase                               | passive_inflation                                    | 'passive_inflation'                 |
    |                                     |                                                      | 'active_contraction' or 'all'       |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | passive_synth                       | False                                                | Synthetic data for passive phase    |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | gamma_space                         | 'CG_1'                                               | Space for gammma.                   |
    |                                     |                                                      | 'R_0', 'regional' or 'CG_1'         |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | active_contraction_iteration_number | 0                                                    | Iteration in the active phase       |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+
    | outdir                              |                                                      | Directory for the result            |
    +-------------------------------------+------------------------------------------------------+-------------------------------------+

You can also change bounds on the control parameters, tolerance, maximum number iterations and optimization
method in the function `setup_optimization_parameters()` in `demo.py`.


Patient data
************

The function `load_patient_data()` in `demo.py` stores the patient data as attributes to a generic object.
The object generated by this function contains the minimum required attributes to run the code.
You can also choose the resolution of the mesh, and there is three available resolutions available in this repository,
all stored in a HDF5 file in the folder `data`.


Running the code
****************

There are two ways of running the code; using patient data or using synthetic data.
In you want to run the code using patient data simply do:

.. code::

   params = setup_adjoint_contraction_parameters()
   
   # Perform any changes to the parameters of change them directly in the function

   main_patient(params)



This will load the measurements from the file `data/measurements.yml` and run through the whole cycle.

If you wan to run the synthetic case you do:

.. code::

   params = setup_adjoint_contraction_parameters()
   
   # Perform any changes to the parameters of change them directly in the function
   # You may want to add noisy (or not), and you may want to run the synthetic test
   # on the active or the passive phase

   main_synthetic(params)


If you want to add noise in the synthetic case, there are already generated yaml files in the folder
`data` with realizations of the noise. These are the same realizations as used in the paper.


The results are stored in the file specified by the `sim_file` in the parameters.
