#!/usr/bin/env python
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of CARDIAC_HIGHRES_DATAASSIM.
#
# CARDIAC_HIGHRES_DATAASSIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CARDIAC_HIGHRES_DATAASSIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with CARDIAC_HIGHRES_DATAASSIM. If not, see <http://www.gnu.org/licenses/>.
import numpy as np
from dolfin import *
from dolfin_adjoint import adj_reset
from cardiac_highres_dataassim.run_optimization import run_passive_optimization, run_active_optimization
from cardiac_highres_dataassim.adjoint_contraction_args import *
from cardiac_highres_dataassim.utils import passive_inflation_exists, Text, pformat, Object
from cardiac_highres_dataassim.synthetic_data import generate_active_synth_data


path = os.path.dirname(os.path.abspath(__file__))

def setup_adjoint_contraction_parameters():
    """Setup all the parameters used for 
    the optimization
    
    Returns:
      apllication parameters (:py:class`dolfin.parameters`)

    """
    

    params = setup_application_parameters()

    # Optimization parameters
    opt_parameters = setup_optimization_parameters()
    params.add(opt_parameters)
    
    return params
    

def setup_general_parameters():
    import dolfin

    # Parameter for the compiler
    flags = ["-O3", "-ffast-math", "-march=native"]
    dolfin.parameters["form_compiler"]["quadrature_degree"] = 4
    dolfin.parameters["form_compiler"]["representation"] = "uflacs"
    dolfin.parameters["form_compiler"]["cpp_optimize"] = True
    dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = " ".join(flags)
    dolfin.set_log_active(True)
    dolfin.set_log_level(ERROR)


def setup_application_parameters():
    """
    Setup the main parameters for the pipeline

    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | key                                 | Default Value                                        | Description                        |
    +=====================================+======================================================+====================================+
    | base_bc                             |  'dirichlet_bcs_fix_base_x'                          | Boudary condition at the base.     |
    |                                     |                                                      |['dirichlet_bcs_fix_base_x','fixed']|
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | alpha                               | 0.9                                                  | Weigth between strain and volum    |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | reg_par                             | 0.01                                                 | Regularization parameter           |         
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | noise                               | False                                                | If synthetic data, add noise       |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | noise_path                          | './data'                                             | Path to noise data                 |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | noise_realization                   | 0                                                    | Which realization of noise         |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | use_deintegrated_strains            | False                                                | Use full strain field              |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | alpha_matparams                     | 1.0                                                  | Weight between strain and volume   |
    |                                     |                                                      | for the passive phase              |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | base_spring_k                       | 1.0                                                  | Basal spring constant              |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | synth_data                          | False                                                | Synthetic data                     |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | sim_file                            | 'result.h5'                                          | Path to result file                |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | res                                 | 'normal'                                             | Mesh resolution                    |
    |                                     |                                                      | ['low', 'normal', high']           |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | Material_parameters                 | {'a': 2.28, 'a_f': 1.685, 'b': 9.726, 'b_f': 15.779} | Material parameters                |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | phase                               | passive_inflation                                    | 'passive_inflation'                |
    |                                     |                                                      | 'active_contraction' or 'all'      |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | passive_synth                       | False                                                | Synthetic data for passive phase   |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | gamma_space                         | 'CG_1'                                               | Space for gammma.                  |
    |                                     |                                                      | 'R_0', 'regional' or 'CG_1'        |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | active_contraction_iteration_number | 0                                                    | Iteration in the active phase      |
    +-------------------------------------+------------------------------------------------------+------------------------------------+
    | outdir                              |                                                      | Direction for the result           |
    +-------------------------------------+------------------------------------------------------+------------------------------------+

    """

    params = Parameters("Application_parmeteres")
    params.add("sim_file", "results/result.h5")
    
    params.add("outdir", "results")
    params.add("alpha", 0.9)
    params.add("base_spring_k", 1.0)
    params.add("reg_par", 0.01)
    params.add("res", "normal")
    
    params.add("gamma_space", "CG_1", ["CG_1", "R_0", "regional"])
    params.add("optimize_matparams", True)
    params.add("synth_data", False)
    params.add("passive_synth", False)
    params.add("noise", False)
    params.add("noise_path", "/".join([path, "data"]))
    params.add("noise_realization", 0)
    params.add("use_deintegrated_strains", False)
    params.add("optimize_displacements", False)
    params.add("base_bc", "dirichlet_bcs_fix_base_x", ["dirichlet_bcs_fix_base_x",
                                                       "fixed"])
    
    
    # Set material parameter estimation as default
    params.add("phase", 'passive_inflation', ['passive_inflation', 'active_contraction', 'all'])
    params.add("alpha_matparams", 1.0)
    params.add("active_contraction_iteration_number", 0)

    material_parameters = Parameters("Material_parameters")
    material_parameters.add("a", 0.291)
    material_parameters.add("a_f", 2.582)
    material_parameters.add("b", 5.0)
    material_parameters.add("b_f", 5.0)
    params.add(material_parameters)

    return params

def setup_optimization_parameters():
    # Parameters for the Scipy Optimization
    params = Parameters("Optimization_parmeteres")
    params.add("method", "SLSQP")
    params.add("active_opt_tol", 1e-6)
    params.add("active_maxiter", 100)
    params.add("passive_opt_tol", 1e-9)
    params.add("passive_maxiter", 30)
    params.add("scale", 1.0)
    params.add("gamma_max", 0.9)
    params.add("matparams_min", 0.1)
    params.add("matparams_max", 50.0)
    params.add("disp", False)

    return params


def load_patient_data(synth_data = False, res="normal"):
    """
    Load patient data into an object used for optimization

    :param bool synth_data: Use synthetic data or not
    :param str res: Reolution of mesh `(default = "normal")`
    :returns: An object with patient data
    :rtype: object

    """
    
    import yaml
    import numpy as np
    
    
    patient = Object()

    h5group = "26"
    ggroup = '{}/geometry'.format(h5group)
    mgroup = '{}/mesh'.format(ggroup)
    lgroup = "{}/local basis functions".format(h5group)
    fgroup = "{}/microstructure/".format(h5group)

    if res == "high":
        h5name = "/".join([path, "data/mesh_highres.h5"])
    elif res == "low":
        h5name = "/".join([path, "data/mesh_low_res.h5"])
    else:
        h5name = "/".join([path, "data/mesh.h5"])
        
    with HDF5File(mpi_comm_world(), h5name, "r") as h5file:

        # Load mesh
        mesh = Mesh(mpi_comm_world())
        h5file.read(mesh, mgroup, False)
        patient.mesh = mesh

        # Get facet function
        ffun = MeshFunction("size_t", mesh, 2, mesh.domains())
        ffun.array()[ffun.array() == max(ffun.array())] = 0
        patient.facets_markers = ffun

        # Get cell function
        sfun = MeshFunction("size_t", mesh, 3, mesh.domains())
        patient.strain_markers = sfun

        # Get local bais functions
        local_basis_attrs = h5file.attributes(lgroup)
        lspace = local_basis_attrs["space"]
        family, order = lspace.split('_')

        namesstr = local_basis_attrs["names"]
        names = namesstr.split(":")
        
        lb_names = {"circumferential":"e_circ", "radial":"e_rad", "longitudinal":"e_long"}
        V = VectorFunctionSpace(mesh, family, int(order))
        for name in names:
            l = Function(V, name = name)
            h5file.read(l, lgroup+"/{}".format(name))
            setattr(patient, lb_names[name], l)

        # Get fibers
        if DOLFIN_VERSION_MAJOR > 1.6:
            elm = VectorElement(family = "Quadrature",
                                cell = mesh.ufl_cell(),
                                degree = 4,
                                quad_scheme="default")
            V = FunctionSpace(mesh, elm)
        else:
            V = VectorFunctionSpace(mesh, "Quadrature", 4)
                
        name = "fiber"
        l = Function(V, name = name)
        fsubgroup = fgroup+"/fiber_epi50_endo40"
        h5file.read(l, fsubgroup)
        fsub_attrs = h5file.attributes(fsubgroup)
        setattr(patient, "e_f", l)

    measurements = "/".join([path, "data/measurements.yml"])
    with open(measurements, "rb" ) as data:
        d = yaml.load(data)
        
    patient.pressure = np.array(d["pressure"])
    patient.volume = np.array(d["volume"])
    patient.strain = d["strain"]
    patient.original_strain = d["original_strain"]

    if synth_data:
        patient.passive_filling_duration = SYNTH_PASSIVE_FILLING
        patient.num_contract_points =  NSYNTH_POINTS + 1
        patient.num_points = SYNTH_PASSIVE_FILLING + NSYNTH_POINTS + 1
    else:
        patient.passive_filling_duration = 3
        patient.num_contract_points = 31
        patient.num_points = 34
    

    return patient

def main_patient(params):

    setup_general_parameters()
    logger.info(Text.blue("Start Adjoint Contraction"))
    logger.info(pformat(params.to_dict()))
    

    ############# GET PATIENT DATA ##################
    patient = load_patient_data(res = params["res"])

    ############# RUN MATPARAMS OPTIMIZATION ##################
    
    # Make sure that we choose passive inflation phase
    params["phase"] =  PHASES[0]
    if not passive_inflation_exists(params):
        run_passive_optimization(params, patient)
        adj_reset()
    
    ################## RUN GAMMA OPTIMIZATION ###################

    # Make sure that we choose active contraction phase
    params["phase"] = PHASES[1]
    run_active_optimization(params, patient)

def main_synthetic(params):
    setup_general_parameters()

    params["phase"] = "all"
    params["synth_data"] = True
    patient = load_patient_data(synth_data = True)
    logger.info(pformat(params.to_dict()))

    # Define gamma as a gaussian function
    gamma_expr = '0.1*exp(-((x[0]-x0)*(x[0]-x0))/10)*(1- exp( -10*x[0]*x[0]))'
    
    # Move the concentration of gaussian function from apex to base
    # in order to get an apex-to-base contraction pattern
    X0 = np.linspace(10,-2, NSYNTH_POINTS)

        
    # if not passive_inflation_exists(args_synth):
    logger.info('\nGenerate synthetic data:')
    parameters["adjoint"]["stop_annotating"] = True
    generate_active_synth_data(params, gamma_expr, X0, patient)
    parameters["adjoint"]["stop_annotating"] = False
    logger.info('\nDone generating synthetic data... run optimization')

    if params["passive_synth"]:
        params["phase"] = PHASES[0]  
        run_passive_optimization(params, patient)
    else:
        params["phase"] = PHASES[1]  
        run_active_optimization(params, patient)
   
        
if __name__ == '__main__':
    
    params = setup_adjoint_contraction_parameters()
    # params["gamma_space"] = "regional"
    params["optimize_matparams"] = False
    params["Optimization_parmeteres"]["active_maxiter"] = 2
    # main_patient(params)
    main_synthetic(params)
    
